module.exports = {
  apps: [
    {
      name: 'maintCenter',
      exec_mode: 'cluster',
      instances: 3, // Or a number of instances
      script: './server/index.js',
      cwd: '/app',
      log_file: './logs/pm2Log.log',
      time: true,
      merge_logs: true,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
}
