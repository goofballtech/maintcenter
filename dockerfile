# building base image with minimal dependancies require for future steps
FROM node:14.21.3-alpine AS base

RUN apk add --no-cache bash git openssh && echo -e "\n\n\nBase built, grabbing site\n\n\n"

# adding all dependencies and building out a fully buildable and testable image

FROM base AS full

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY . /app

# copy the changelog file into the docs build folder
RUN cp -v /app/CHANGELOG.md /app/docs/changelog/index.md

# when added jest testing will be put in here
RUN npm run buildDocs && mkdir /app/static/docs && cp -rv docs/.vuepress/dist/* /app/static/docs 

RUN npm run build && echo -e "\n\n\nApp and Docs Built, moving to deploy\n\n\n"

# move built files over to deploy
FROM base AS producion

WORKDIR /app

# copy all the needed files to make the app run from the full instal into a minimal image for deployment
COPY --from=full /app/.nuxt ./.nuxt
# front end stuff
COPY --from=full /app/dist ./dist
COPY --from=full /app/static ./static
COPY --from=full /app/assets ./assets
# back end stuff
COPY --from=full /app/api ./api
COPY --from=full /app/plugins ./plugins
COPY --from=full /app/server ./server
# dependencies and configs
COPY --from=full /app/package*.json /app/nuxt.config.js /app/vuetify.options.js /app/ecosystem.config.js /app/CHANGELOG.md ./

# do an install for only the dependecies required for production
RUN npm install --production
# Install PM2 so we can have parallel processes and ensure 100% uptime
RUN npm install pm2 -g && pm2 install pm2-logrotate

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000
ENV NODE_ENV=production

CMD ["pm2-runtime", "start", "./ecosystem.config.js"]