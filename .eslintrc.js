module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    'eslint:recommended',
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/recommended',
    'plugin:prettier/recommended',
  ],
  // required to lint *.vue files
  plugins: ['vue', 'prettier'],
  // add your custom rules here
  rules: {
    semi: [2, 'never'],
    'no-console': 'warn',
    'no-async-promise-executor': 'warn',
    'vue/max-attributes-per-line': 'off',
    'vue/attribute-hyphenation': 'off',
    'vue/valid-v-bind-sync': 'off',
    'vue/valid-v-slot': ['error', { 'allowModifiers': true }],
    'prettier/prettier': [
      'error',
      {
        semi: false,
        singleQuote: true,
        jsxSingleQuote: true,
        quoteProps: 'preserve',
        trailingComma: 'es5',
        printWidth: 120,
        tabWidth: 2,
        endOfLine: 'auto',
        bracketSpacing: true,
        jsxBracketSameLine: false,
        arrowParens: 'always',
        singleAttributePerLine: true,
      },
    ],
  },
}
