import Vue from 'vue'
import VeeValidate from 'vee-validate'

const veeConfig = {
  // events: 'blur',
  mode: 'passive',
  fieldsBagName: 'formFields', // was conflicting as 'fields' with the b-table element
}

Vue.use(VeeValidate, veeConfig)
