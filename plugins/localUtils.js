// components/index.js, all common component to load site-side
// all components need a top level name: element for registering the component
import Vue from 'vue'
import LoaderButton from '~/components/Utils/LoaderButton.vue'
import BaseFormDialog from '~/components/Utils/BaseFormDialog.vue'
import TechName from '~/components/Utils/TechName.vue'
import BaseFormatDate from '~/components/Utils/BaseFormatDate.vue'

Vue.config.productionTip = false

const commonComponents = {
  LoaderButton,
  BaseFormDialog,
  TechName,
  BaseFormatDate,
}

Object.entries(commonComponents).forEach(([name, component]) => {
  Vue.component(name, component)
})
