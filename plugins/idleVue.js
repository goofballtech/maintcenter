import Vue from 'vue'

const EventBus = new Vue()

import IdleVue from 'idle-vue' // idle time library to check for active user

const options = {
  eventEmitter: EventBus,
  // store,
  idleTime: 60000 * 5, // 5 minutes to idle time out
}

Vue.use(IdleVue, options)

//! TODO: move this to vuex to remove the evenBus requirement
