# Task Modes

## Custom vs Classic Mode

In Custom mode there are potentially 4 modes that can be named and assigned colors. These mode do not change tasking in an way. They are simply to denote the status of the hardware. The current status can be view via a flag that can be optionally shown on the task page or by mousing over the Due Tasks button on the tasks page. Modes can be hidden in addition to named and colored in status. If only one status is active or all are hidden the menu options and mode flags will be hidden as there are no mode changes that require notification. 

Classic mode is intended for but not limited to use on a system that has changing needs depending on the equipment state. These are described more below. 

## Changing Modes

In order to change task modes open the menu ![using the menu button](../assets/images/menuButton.jpg),
select the Maintenance group, select [change mode](/navigation/#change-mode), choose the mode you would like to transition too.

## Available Modes

The below are the options available in classic mode and their function. 

### Operations

The Operations mode is intended to be used when the system is being operated actively.
While in this mode the tasks will become due based on the Ops Frequency frequency selection.

### Transit

The Transit mode is intended to be used when the system is in route to or from the op area but there are people available for doing maintenance.
While in this mode the tasks will become due based on the Transit Frequency frequency selection.

### Layup

There are two sub modes to Layup:

- In Layup
- Out Of Layup.

In Layup is intended for use when the system is being prepared for a period of time when no one will be available for maintenance or care of the system.
Out of Layup is intended to contain tasks that get the system back from a layup state to prepare everything to be able to work once again.

### Mobilization

There are two sub modes to Mobilization:

- De-Mobilization
- Mobilization

De-Mobilization is the tasks that are required in order to remove the system from the host vessel
Mobilization is the tasks required to install the system onto a host vessel

## Theory of operation of Task Modes

The intended purpose of the task modes is to have options for crews that better fit the real world op-tempo changes we face.

Operation and Transit are actively working statuses that will be used to go about the normal PM tasks that need to be done.
Layup and Mobilization will be used less but have specific tasks that are only relevant to themselves.

When going into layup all of the tasks are removed form the list of tasks with the exception of those whose category list includes "In Layup".
The task list will now only contain those tasks which are set for "In Layup". As these tasks are completed and the system is prepared to be in layup mode those tasks will be signed off.
Once all tasks are completed the task list will become a small message that says the system is in layup and you have a button there for coming out of layup.
When this button is pressed then the tasks that contain the category "Out of Layup" are shown in the list.
When these tasks are completed then the system automatically transitions into transit mode as the assumption at this point is that that system is now manned and will be going to work before long.

The mobilization modes is much the same but instead of "In Layup" and "Out of Layup" there will be "De-Mobilization" and "Mobilization".

If a task contains more than one of the above categories it will become due in each of the relevant modes.