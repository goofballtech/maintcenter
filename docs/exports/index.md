# Exports

## Task Export Types

### Tasks or Completions

The first selection oon the exports page is Tasks or completions.

- [Tasks](/exports/#task-exports): You will be presented with options for exporting whole tasks, descriptions, frequencies, etc.
- [Completions](/exports/#task-completion-exports): You will be presented with options to search in existing task completions in the database.

## Task Exports

### Select excel columns

This selection allows the choice of which data points will be exported to he spreadsheet. The order the columns are selected will change their order within the sheet, A preview of the selected column order will appear just below this selection as data points are clicked.

### Query

Now you have to set the criteria for the tasks you wish to have displayed in the list. Keep in mind you are adding to the list not taking away. Each time to add criteria the task list will grow.

#### Tasks Search

- Only Active Tasks: This selection is on by default and limits the search to only tasks that currently appear in the all tasks page. All tasks currently in the deactivated tasks page of settings will be excluded.
- All Active Tasks: This ignore the search criteria below and does a complete task dump of all active tasks currently in the database.
- Formatted for input: this will ignore the selected columns and instead for the columns to match the input spreadsheet that can be used in [settings](/settings/#task-import).

You may search for tasks by adding their relevant categories, doing a query for tasks whose name includes a key word, or description includes a key word.

### Download Export

Once you have some results in the table you are shown the Download Export button. Clicking this will create the spreadsheet then send it to the browser for download.

## Task Completion Exports

### Start and End Dates

You can select a date range for the results that are shown in the list. You may select a start date, end date, both, or none. If not selected the date will be wide open. So an empty start date will allow all entries going back to the creation of the database. An empty end date will allow all entries up to today.

### Task ID Filter

Entering a number into this block will filter the export down to show only results from that specific task number.

### Task Name Filter

Entering text in here will limit results to only those task that contain that exact string somewhere in the task name. This value is ignored if a number is entered into the Task ID Filter.

### Category Filter

Select existing task categories from the drop down to filter selections by category. Only tasks including the selected categories will be included in the results.

:::tip
This selection is done based on the categories of each task at the time of the search, not the time of task completion. Categories can be added to a task after completion and they will still be part of the result pool.
:::

### Tech Name Filter

Entering text here will limit results to only entries that have matching text in the Tech Name field of the completion.

### Download Previous Completion Export

Once the table reflects the desired results then click this button to have the server build the proper spreadsheet and send it to the browser for download.

### Download Task Notes

Enabling this option will export an addition excel sheet that contains all notes for tasks included in this completion export. This is not limited by the dates of the completions but will instead export all notes for the included tasks.

## Export Inventory

### Select excel columns

This selection allows the choice of which data points will be exported to he spreadsheet. The order the columns are selected will change their order within the sheet, A preview of the selected column order will appear just below this selection as data points are clicked.

### Query

Now you have to set the criteria for the tasks you wish to have displayed in the list. Keep in mind you are adding to the list not taking away. Each time to add criteria the task list will grow.

#### Item Search

- All Inventory Items: This will ignore all further search criteria and fill the export list with all active inventory items.

#### Category

Add items to export list based on the categories to which they contain.

#### Vendors

Add items to list based on applied vendors. This will only find vendors names applied by the user not ones that are available via cross reference in the vendor database.

#### Name includes

Add all items whose name includes the provided text

#### Description Includes

Add all items whose description includes the provided text

### Download Export

Download the requested excel spread based on the existing search selections.

## Order Exports

### Select Vendor

Select the vendor you would like to export your order for. If Oceaneering is selected then the Export To Encompass template will be selected by default. If any other vendor is selected the the export type will be set to PR Template.

### Request Dates

Select the request date that you would like to be filled into your spread sheet.

### Job Number

Job number and activity code to be filled into the template as required.

### Mark Pending

Mark Pending defaults to active. This option will mark the exported lines as pending while the excel sheet is being generated. This allows things to not be exported twice if at all possible as **_the export search ignores items in the list that are already marked as pending_**.

### Download Excel Sheet

Trigger the excel sheet download. If exporting to Encompass then the entire list will pull to a single excel file. See below tip for use of this sheet. If exporting to PR then the order will will be one a single sheet if less than 27 items or split among multiple sheets and zipped together if more than 27.

When the sheet opens there is an error about named ranges. It is expected and has something to do with the formula's in the sheet and the way the app opens it to fill in the boxes. There may be a way to fix it but it has not yet been discovered. Acknowledge it and move on with completing your sheet as required.

### Order Line Table

The table will show the name of the To Order item as provided by the user as well as a list of the part numbers included in the order. This is made to be a quick reference of how many items are being returned by the vendor query.

:::tip
Use of Encompass formatted excel sheet.

Generate a new Encompass Requisition as per normal process. Once the Encompass req is opened select the import button and navigate to your excel sheet.
![Import Button](../assets/images/importButton.jpg)

This should import each line in the excel sheet to it's own row within the requisition. Once these items are filled in properly there is a function within Encompass that will fill in the remaining data. To auto fill the units, descriptions, and prices click the gear icon. Once that is done click "Validate and Save" to make sure it's all locked in.
![Fill, Validate, and Save](../assets/images/fillAndSave.jpg)

Once everything is saved scroll through the descriptions to make sure you don't see any of concern such as "OBSOLETE" in anything. Because part numbers change it's possible things become outdated before we submit orders.
:::

## To Do Exports

### Priority

Select the priority to narrow down the results by or All to include all options.

### Group(s)

Select one or more group specifications to include in the results to export.

### Download Excel Sheet

Generate and download the export.

## Export Packages

### System

Select All systems or narrow those to be exported by a specific system.

### categories

Select on or more categories to be included in the results to be exported.

### Package Number Pre-Text

This text will be added to the Package # data in the table and on the export sheet is amplifying data is required in the table.

### Open Status

Narrow data by All packages, only those remaining open or those that have yet to be set to open.

### Download Excel Sheet

Generate and download the export.
