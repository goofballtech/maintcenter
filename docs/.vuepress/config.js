module.exports = {
  title: 'Maintenance Documentation',
  description:
    'This should contain the required information to assist in navigating and using the maintenance software.',
  base: '/docs/',
  theme: 'default-prefers-color-scheme',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Change Log', link: '/changelog/' },
      { text: 'Site Navigation', link: '/navigation/' },
      { text: 'Task Modes', link: '/modes/' },
      { text: 'Upgrading', link: '/upgrade/' },
      { text: 'Feedback', link: '/feedback/'},
      { text: 'Deployment Strategies', link: '/setup/' }
    ],
    smoothScroll: true,
    sidebar: 'auto'
  },
  head: [
    ['meta', { name: 'theme-color', content: '#ffffff' }],
    ['meta', { name: 'msapplication-TileColor', content: '#ffffff' }],
    [
      'meta',
      { name: 'msapplication-TileImage', content: '/icons/ms-icon-144x144.png' }
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/icons/favicon-16x16.png'
      }
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/icons/favicon-32x32.png'
      }
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: '/icons/favicon-96x96.png'
      }
    ],
    [
      'link',
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: '/icons/android-icon-192x192.png'
      }
    ],
    ['link', { rel: 'manifest', href: '/manifest.json' }]
  ],
  plugins: ['@vuepress/pwa', { serviceWorker: true, updatePopup: true }]
}
