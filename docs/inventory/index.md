# Inventory

## Inventory Page

### Searchable Table

The table contains all items currently in the inventory system and set as active. Inactive items are displayed on the [settings page](/settings/#inactive-items). You may use the search table to find needed items. The search with look through Name, Description, Category, Part Numbers, and Keywords and display only those items with matching information. 

Clicking a row will open the Item Details popup

### By Location Tree

The table contains all items in the selected location and all it's sub locations. If the primary location is selected the entire inventory will be displayed as it is on the searchable table. If the tree is navigated then the item listing will be narrows by only things contained in that location. 

Clicking a row will open the Item Details popup

## Item Details Popup

### Edit Mode Button (Admin or Inventory Mode)

The edit button is only visible when logged in as admin or when inventory mode is set by an admin in the settings page. 

### Name

A very brief description of the item. 

### Total on hand

The total number of the current item on hand in all locations. This field is not editable as it is calculated when this popup is open based on the existing quantities. 

### Customer ID and PO Number

Location to record Customer and PO for tracking purposes.

### Unit of Measure

How thew items is ordered or packaged normally. Pack, box, each, etc. 

### Critical Spare

Select if the item is considered a critical spare. Critical spares are generally something that could potentially cause significant downtime if they are not present in the event of a failure. This selection will automatically add a "CS" tag to the categories list on the Inventory Page display tables. 

### Compliance

Adds a Compliance tag to the item details window. 

### Attachments Window

Clicking the paper clip will open the attachments window. The number on the icon indicates the number of existing attachments on the current item.

Within the attachments window a list of the current attachments are shown along with options to upload new attachments to the inventory item. 

### Detail Needed / Date Needed

Turn on Detail Needed and/or Date needed as required. Details can be something like Batch or Serial number that can be used to identify items. Date can be used can be a manufacture date, expiration date, certification date, etc. The name used for the Detail or Date can be set in the Label box so future users will know what this information references. 

When either of these is activated a button will appear in edit mode below the location table to add details. Ensure the proper quantity of each item is filled into the proper locations first then click the "Add Details" button. You will then be able to fill the proper details into the resulting popup. 

### Description

A more detailed description of the item. 

***This will auto fill if block is left empty and an OI number is selected in the part number search block***

### Minimum Quantity on Hand

The number of these items that are considered to be the least amount on hand before a reorder is required.

### Maximum to Order To

Once the quantity of this items reaches or goes lower than the minimum number the ordered needed will be what is required to get the order back up to this value. 

### Categories

A list of the categories to which this item is related. 

### Drawings

Add drawing numbers that contain this item for reference. The line number in the BOM for is also sometimes helpful. 

### Locations

A table of locations where this items is stored. 

#### Path

The path by which to get to the location this item is stored. 

#### Locations

The name of the location where this part is actually stored. Mousing over the location name will display any notes that may also be applied to this location.

#### Qty Here

The number of this items stored at this location. Mousing over this value will display the date this number was last updated. 

If item Details or Date is in use this number can be hovered over to display the specifics for the items in this location. 

#### Use One

Clicking this icon will remove 1 from the quantity at this location

When item Details or Date is set the Use One button will show a pop up so the proper item being used can be chosen and that one will be removed from inventory.

#### Fix QOH

Show a form which allows the changing of the on hand quantity of the item. 

When item Details or Date is set to needed the Fix QOH button is hidden as details or dates would need to be added for additional items. 

### Database Vendor Numbers Table

This table is auto generated based on the vendor numbers or OI numbers entered by the user for this item. All matching part numbers in the vendor database will be added to this table for additional references to users. 

### *Entered* Part Numbers

A table of the part numbers and their relevant vendors for this item as entered by a local user.  

If Oceaneering or OII is the vendor for a pending part number entry a button will appear that allows a (search of known OI part numbers)[/to-order/#oi-part-number-search-2]. If the description of the item if left blank and an OI part number is selected fro the OI part number search box the description of the item from the database will be filled into the description box automatically. 

#### Vendor

The vendor name that uses the part number specified

#### Part Number

The part number used to identify the given part for the specified vendor. If Vendor name is OII or Oceaneering and more information is available for the part it will pop up on mouse over of the part number. 

### Keywords

Key words assigned to this part for ease of future searches. When in edit mode words can be added or removed as normal. When no in edit mode keywords can only be added. These words are always searched when looking for parts so enter words here that will assist you in looking for this part in the future. 