# Basic Site Navigation

## Nav Menu

To navigate to other pages within the site click the ![menu button](../assets/images/menuButton.jpg) at any time if the [menu is set to hidden](/settings/#menu-style)).
If the [menu is set to persistent](/settings/#menu-style) the menu will always be visible on the left side of the screen and expand when the mouse moves over it.

### Hardware Selection

The top of the nav bar displays the [Site Name](/settings/#site-name) of the currently selected hardware. Click the name in order to display the other hardware currently set up on this server. Click on a new piece of new hardware in order to reload the page with that data.

### Maintenance

If enabled in [site settings](/settings/#site-settings), the number of tasks [currently due](/settings/menu-due-values) will show here. 

#### Maintenance Page

Click here to open the [tasks](/maintenance-tasks/) page.

#### Assets Page

Click here to open the [assets](/asset-details/) page.

#### Change Mode

These options may change slightly depending on the current [task mode](/maintenance-tasks/#available-modes).

- Operation Mode
  - Tasks become due at the Ops interval defined in the task.
  - Once clicked all tasks will be checked to make sure the proper due date is set for this mode.
- Transit Mode
  - Tasks become due at the Transit interval defined in the task.
  - Once clicked all tasks will be checked to make sure the proper due date is set for this mode.
- Layup
  - Go Into Layup
    - Remove all currently due tasks form the task list and replace them with only tasks that have "Into Layup" category assigned to them.
    - Once all Into Layup tasks have been completed a button for coming out of layup will appear.
  - Come out of Layup
    - Remove all currently due tasks (including in "Into Layup" tasks that were uncompleted) and show all tasks with the category "Out of Layup" assign to them.
    - Once all "Out of Layup" categories have been completed the system will automatically shift into Layup mode assuming the crew is now in place and the system is headed to work.
- Demobe
  - Demobe system
    - Remove all currently due tasks and replace them with only tasks that have "De-Mobilization" category assigned to them.
    - Once all tasks are complete the system will be paused and a Mobilization button will appear.
  - Mobilize system
    - Remove all currently due tasks (including "De-Mobilization" tasks that were uncompleted) and replace them with tasks with the category "Mobilization" assigned to them.
    - Once all "Out of Layup" categories have been completed the system will automatically shift into Layup mode assuming the crew is now in place and the system is headed to work.

### To Do list

Opens the [To Do List](/to-do/) page.

If enabled in [site settings](/settings/#site-settings), the number of To Do's [currently past their set due date](/settings/menu-due-values) will show here. 

### Inventory Page

Opens the [Inventory](/inventory/) Page. 

### Hose Registry Page

Opens the [Hose Registry](/hose-registry/)

If enabled in [site settings](/settings/#site-settings), the number of Hoses [currently due for change and/or inspection](/settings/menu-due-values) will show here. 

### To Order List

Opens the [To Order List](/to-order/)

If enabled in [site settings](/settings/#site-settings), the number of To Order items [currently past their needed by date](/settings/menu-due-values) will show here. 

### Exports

#### Task Exports

Opens the [task exports page](/exports/)

### Settings

Open the [Settings page](/settings/)

### Admin Log In

Allows an admin user to log into the site and reveal additional functionality for settings and task modification. If you leave the system idle you will be logged out. 

### Requests/Feedback

Opens a separate browser tab with to [Maintenance Center Issues](https://gitlab.com/goofballtech/maintcenter/-/issues). (internet required)
It is here where you would fill in a bug report, feature request, etc. Please be as detailed as possible so as to better communicate what you need.
You can also view existing open report and requests, feel free to upvote existing feature requests so they get implemented faster if you are so inclined.
I won't know what everyone needs and will essentially be working off a one boat data set without feedback.

If you do not yet have an account one can be created using your Oceaneering email address to sign up. Any other email address will not work by default.

### Help/Wiki

Opens this page

