# Markdown Cheatsheet

## Headings

# Heading 1 
    
    # Heading 1

## Heading 2 ##

    ## Heading 2

### Heading 3

    ### Heading 3

#### Heading 4

    #### Heading 4

## Text

Common text

    Common text

_Emphasized text_

    _Emphasized text_ or *Emphasized text*

~~Strikethrough text~~

    ~~Strikethrough text~~

__Strong text__

    __Strong text__ or **Strong text**

___Strong emphasized text___

    ___Strong emphasized text___ or ***Strong emphasized text***

## Links

[Named Link](http://www.google.com/ "Named link title") and http://www.google.com/ or <http://example.com/>

    [Named Link](http://www.google.com/ "Named link title") and http://www.google.com/ or <http://example.com/>

[heading-1](#heading-1 "Goto heading-1")
    
    [heading-1](#heading-1 "Goto heading-1")

## Lists

* Bullet list
    * Nested bullet
        * Sub-nested bullet etc
* Bullet list item 2

~~~
* Bullet list
    * Nested bullet
        * Sub-nested bullet etc
* Bullet list item 2
~~~

1. A numbered list
    1. A nested numbered list
    2. Which is numbered
2. Which is numbered

~~~
1. A numbered list
  1. A nested numbered list
  2. Which is numbered
2. Which is numbered
~~~

- [ ] An uncompleted task
- [x] A completed task

~~~
- [ ] An uncompleted task
- [x] A completed task
~~~

Foldable text:

<details>
  <summary>Title 1</summary>
  <p>Content 1 Content 1 Content 1 Content 1 Content 1</p>
</details>
<details>
  <summary>Title 2</summary>
  <p>Content 2 Content 2 Content 2 Content 2 Content 2</p>
</details>

    <details>
        <summary>Title 1</summary>
        <p>Content 1 Content 1 Content 1 Content 1 Content 1</p>
    </details>

## Lines and Images

Horizontal line :
- - - -

    - - - -

Image with alt text:

![picture alt](http://via.placeholder.com/200x150 "Title is optional")

    ![picture alt](http://via.placeholder.com/200x150 "Title is optional")

## Blocks and code

The below code is wrapped in one or 3 back ticks, not apostrophes. Backtick is the key near escape. Usually has the tilde (~) as a roommate. 

`code() all on a single line`

    `code() all on a single line`

Multi-line generic block of text

```
<h3>HTML</h3>
<p> Some HTML code other code here that is on multiple lines</p>
```
    ```
    <h3>HTML</h3>
    <p> Some HTML code other code here that is on multiple lines</p>
    ```

Multi-line language-specific code

```javascript
var specificLanguage_code = 
{
    "data": {
        "lookedUpPlatform": 1,
        "query": "Kasabian+Test+Transmission",
        "lookedUpItem": {
            "name": "Test Transmission",
            "artist": "Kasabian",
            "album": "Kasabian",
            "picture": null,
            "link": "http://open.spotify.com/track/5jhJur5n4fasblLSCOcrTp"
        }
    }
}
```

    ```javascript
        if(wantHighlighting === 'yes'){
            var specify = 'your language after the opening back ticks'
        }
    ```


## Other/Misc

Emoji:

:exclamation: Use emoji icons to enhance text. :+1:  Look up emoji codes at [emoji-cheat-sheet.com](http://emoji-cheat-sheet.com/)

    Code appears between colons :EMOJICODE: