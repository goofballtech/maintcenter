# Packages

## Packes Table

### New Package Button

Clicking on this opens the [New Package Entry](/packages/#new-package-dialog) button dialog.

### Search

Filter packages to ones that contained the entered search text. This searches the items visible in the table but not the notes entered into the package.

## New Package Dialog

### Package Number

Manually entered package number. Due to this needing to match documentation generated elsewhere there is no auto numbering scheme here. The only thing this will do is to make sure that the chosen package number does not already exist in the system to very no duplicate entries.

### Package Task

Specify the related package task.

### System

Specify the system the package will be applied to. The dropdown will be populated automatically with previous systems used in other packages. 

### Category

Add one or more categories to the package entry. 

### Description

Any additional information that expand the details provided in the above boxes.

This box allows use of [Markdown](https://en.wikipedia.org/wiki/Markdown) which is a very easy way to do formatting of web based text.
[See here](/markdown/) for a decent cheat sheet that shows what can be done and how to do it.

### Package Attachments

Add any files that would be useful later when referencing the package entry. 

## Edit package Dialog

The below are only the additional options available to edit. The edit options are only available when logged in as an admin. 

### Date Opened

Date the package was opened. 

### Date Closed

Date the package was closed. 

## Package Details Window

The below are only the visible options not available in the above dialogs. 

### Notes

Add any additional information in the form of notes applied to the package. Enter the required note and press enter or click the plus to apply the note to the package. There is a limit of 5 notes allowed to be applied to any one package and more than that should require a new revision of the package instead of additional notes. A logged in admin account also has the ability to delete notes as required via a "remove" button that appears when logged in. 