# Feedback

The below is some basic information about how to report bug or request new features. 

This can be done at the Issues page on the gitlab website. It can be found here [https://gitlab.com/goofballtech/maintcenter/-/issues](https://gitlab.com/goofballtech/maintcenter/-/issues).

Once the "New Issue" button is pressed there will be a template selection option that wil assist you in making sure you have some of these items covered in the issue context as well. 

## Bug Reporting

When reporting a bug more details are better. If possible please specify the steps to recreate the bug so it can be duplicated during troubleshooting.

Each browser has development tools that can provide very useful information. In Chrome this can be accessed vis the menu's or by pressing Crtl + Shift + I. Select the console tab the cause the bug to happen if possible. Watch the console and see if any error appear. If so these can be an important clue as to the cause. You can sometime right click on the console and select save as... to generate a text file with the error. If this is not available then selecting the errors and pasting them into the bug report would work fine.

Navigate over to the Settings page and hit the logs tab. Download the spreadsheet available there and attach it to the bug report. This could contain useful error information as well as the version of the browser and operating system that could be useful in recreating the bug. 

If the bug is a visual problem then a screen shot or anything that shows the issue would be very helpful.

## Request Feature

When requesting a feature on the site try to think it through as much as you can. 
- What do you want this feature to accomplish?
- How could it be implemented in a clean way?
- What information would be required for an entry(if applicable)?

Try to lay these out as best you can in the feature request so that i might be able to see your vision. Photoshop or even a pen and paper drawing is better then nothing to try ad communicate your ideas. 