# Deployment Strategies

There a a couple different deployment strategies for the software. Once you get into it there may be more than this, these are just the ones at the surface. 

 There are some gains and losses for each, whether they are pro's and cons would depend on the end user. I will highlight some here just to have a baseline when doing initial setup of the site. 

## Multiple Hardware Setup

 - Set up an individual [hardware](/settings/#adding-a-new-site) page for each major asset and use these to [switch back and forth](/navigation/#hardware-selection) between equipment. Things like ROV/Cage, ROV Winch, etc would each have an isolated page. 

    - Each hardware page maintains a seperate task list, order list, to do list, etc. 
        - If this is the cage and you did not want to manage maintain seperate lists you could use the settings page and turn off the un-needed pages listed in the other hardware pages. The one that's not show is the one you want users to utilize. 
        - Tasks attached to different hardware cannot be [related](/maintenance-tasks/#show-related-tasks-dialog) using the grouping functionality.  
        - Tasks and other items do not cross search between hardware pages. You would have to switch hardware pages to search for a task that may be atttached to another item. 

## Single Hardware Setup

 - Set up a single hardware site and use the [categories](/maintenance-tasks/#category-edit) in each task to identify which hardware the task applies to.

    - With a single set of hardware then it all changes [modes](/modes/) together. So transit, layup, demobe, all move as one, there is no way to put individual piece of hardware in layup or demobe status. 
    - If different teams of people work on each hardware set then they would have to search for their own hardware on each page load as all tasks will be intermingled. 


## Long term vision

The second option was the basis of the design in the inital vision of the site. As with all things though requirements evolve. I will be trying to merge the pros of the functionality of each deplyment strategy together in the future. This should assist in having much more versitility of use without the drawbacks of having to choose A or B options in the future. This is a bit lower on the [issues list](https://gitlab.com/goofballtech/maintcenter/-/issues/57) than thing like logs books and inventory control though so it has been back burnered until other base functionality is in place.
