# Hose Registry

## Hose Registry Table

### Order Info

The Order Info button will become active when hoses in the table are selected.

When this button is pressed all the selected hoses will be compiled into a list to allow the user to order the materials to replace these hoses. If the hose is small enough to be made locally the part numbers and fittings for the hose will be totaled up. If there is an Oceaneering part number assigned to the hose these will be totaled. For [hoses larger than can be made](/settings/#max-hose-size) on site each hose will be listed so they can be ordered pre-made.

The [largest hose](/settings/#max-hose-size) that can be created on site can be set and is adjustable in the [Hose Registry](/settings/#hose-registry-settings-admin-only) page of the settings menu.

Once the dialog is open and the data appears to be what is needed you can download the information in excel form by pressing the "Download Order Info" button.

### Create New Hose

Click the New Hose button to open the [New Hose](/hose-registry/#new-hose-dialog) dialog.

### Search

The search box will allow the user to filter out hoses shown in the table.
3 or more characters have to be entered into the box before the search is triggered.
By default the search looks at the data visible with in the table.
If required the toggle switch near the search box allows the user to include the hose descriptions in the query as well.
This adds a bit of time for the search to complete but can certainly be useful if the required term is not included in the hose name.
In order to clear the search simply delete the search text form the search input box.

### Hose Table

The table displays the tag number, the hardware where the hose is installed, a more specific location within that hardware, then the next time that hose has an inspection/change due. The table only shows the most immediate date, the [details](/hose-registry/#hose-details) window will show more specific data.

The default sort on the table is by the next hose inspection coming due.

### Mark Inspected/Changed

Before clicking the Inspected/Changed button you may open the notes entry box by clicking the down arrow and entering the required comment into the field. The note will be attached to the completion and displayed with the [completion entries](/hose-registry/#show-inspectionchange-intervals).

The buttons on the Hose Table to Make a hose and having been inspected or changed can be clicked to mark the hose as needed and set the next inspection/change date according to the periodicity for that specific hose.

The buttons will turn red when due. When the hose change becomes overdue the inspection button is no longer available.

## New Hose Dialog

### Tag Number

The tag number will be pre-filled with the next tag number in the series.

If the user would like to specify a tag number the tag number will be checked and a message will display to tell the user if the requested tag number is already used.

If you would like to retrieve the next tag number again after testing availability of other tags click the refresh icon on the right side of the text field.

::: tip
The hose tag number can also be a customized string. Remove the number and type in your own string if you wish. The program will check to make sure that tag isn't already used.
:::

### Other new hose options

Please see the [Edit Hose](/hose-registry/#edit-hose) section for specifics on all other data entry fields.

## Hose Details

To open the details page click on the hose in the [Hose Table](/hose-registry/#hose-registry-table)
The details windows displays information about the selected hose.

### Edit Mode Toggle

The pen icon at the top left of the details window will switch over into Edit Mode where the hose can be changed as required.

### Previous Inspections/Changes

When there are previous inspections/changes on the displayed hose a timer icon will be displayed to the right side of the hardware display.

:::warning Completion Note
Due to a change in the way completions are recorded and adding an ability to change a hoses tag number after it's creation. Previous inspections/changes submitted before version 3.7.5 (released October 2021) will have been stored using only the Tag number of the hose. Submissions after this time will be using a hose ID other than it's tag number so the consistency will be greater over tag value changes if they become required. This change means that it is possible to have some completions before this date not show if a tag number is changed down the road and also means if someone were to re-use a previously used tag number there could be an inaccurate completion noted.
:::

### Show Hose Part Numbers

If hose part numbers/pressure ratings have been entered then a blue clipboard will appear to the left of the hose size display. Clicking this icon will toggle the display of the part numbers that have been entered for this hose.

### Show Inspection/Change Intervals

Between the Last Inspection date and Next Change date there is an Orange pen icon. Clicking this icon will toggle the display of the inspection/change intervals.

## Edit Hose

### Tag Number

The tag number assigned to this hose is displayed here. This is not an editable field once the hose is created.

### Hardware

The hardware on which the hose is installed. This will he high level equipment and there is a dropdown of Hardware already entered to avoid doubling up with slight variations of the same thing.

### Location

This is a more specific location within the hardware. For example if the Hardware is ROV this might be port manipulator.

### Hose size

This dropdown displays the hose sizes currently entered as hose defaults in the [Hose Registry](/settings/#hose-registry-settings-admin-only) page of settings.
To add an option to this dropdown it can be done there as well.

If a hose size is entered above the hose size that can be made on site a pressure rating will also be required for the purposes of later spec and ordering hoses. If the pressure rating in the Hose Registry page of the settings is set for each size it will appear as the default value when that particular hose size is selected.

#### Part Numbers

The blue clipboard icon to the left of the hose size selection box will display the part number for hose and fittings for the current hose. These boxes are pre-filled with the part numbers according to the defaults set in the [Hose Registry](/settings/hose-registry) settings page for the hose of the specified size.
These numbers are able to be changed but will be automatically put back to the default number is the selection is changed via the hose size drop down.

### OI Number

Turn on the OI number switch and fill in the resulting field if the hose is a pre-made hose provided by Oceaneering via inventory.

### Hose Length

Please enter the hose length in inches or meters per selected units in settings page. The hose is measured by the base of the fitting. Click the help icon for a reference to how to get your measurement. This allows the person making the replacement hose to cut the material to length and end up with the proper overall measurement.

If the measurement was taken in Feet and Inches and you don't feel like doing math then click on the icon to the right of the form for a helper dialog to convert to inches (hidden if meters is the chosen unit).

### Last Inspection

Please select the date the hose was last inspected if other than the one currently filled in.

### Last Change

Please the the date the hose was last changed if other than the one currently filled in.

### Intervals

Click the orange pen icon to display the intervals for this specific hose.

#### Inspection Interval

Select the interval between inspections. This will be auto filled on new hose creation with the default set in [Hose Registry settings](/settings/#hose-registry-settings-admin-only) and will not allow the user to select a setting higher than the Max set in [Hose Registry settings](/settings/hose-registry).

#### Change Interval

Select the interval between changes. This will be auto filled on new hose creation with the default set in [Hose Registry settings](/settings/#hose-registry-settings-admin-only) and will not allow the user to select a setting higher than the Max set in [Hose Registry settings](/settings/hose-registry).

### Description

Hose description should contain any additional information that will help the user identify the appropriate hose.

### Delete Button (Admin Only)

Deletes hose from database
