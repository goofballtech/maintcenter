# To Do

## To Do Table

### New To Do Button

Clicking this button opens the [New To Do](/to-do/#new-todo-dialog) dialog.

### Show Previous To Do's

This button will display all previously entered To Do's items that have either been marked previously as completed or removed.

#### Show Current To Do's

Once the previous button is clicked the button changes and will now toggle back tot he current list of To Do's

### Search

The search box will allow the user to filter out to do's shown in the table.
3 or more characters have to be entered into the box before the search is triggered.
By default the search looks at the data visible with in the table.
If required the toggle switch near the search box allows the user to include the to do descriptions in the query as well.
This adds a bit of time for the search to complete but can certainly be useful if the required term is not included in the to do name.
In order to clear the search simply delete the search text form the search input box.

### To Do Table

The table displays the name, date due (if set), group (if set), priority, and Tech name who entered the to do.

The default sort is by priority, high to low.

### Mark Done/Removed

You can mark a To Do as done by clicking the Done button. You can mark a To Do removed by clicking on the red removal button.

In order to attach notes to the To Do click on the down arrow and add the note in the resulting field.

## New To Do Dialog

Please see [Edit To Do](/to-do/#edit-mode) for details on each relevant form field.

## Details To Do

The details popup will display all relevant data for the selected To Do.

### Edit Mode Toggle

The pen icon at the top left of the details window will switch over into Edit Mode where the hose can be changed as required.

## Edit Mode

### To Do Name

A short but descriptive name of the To Do.

### Priority

Th priority of the To Do. This will default to Low if not changed.

### Due Date

If a due date is relevant enter it here. This is an optional field.

### Group

Enter or select an existing task group.

### Description

Provide additional details required for the person performing the task to complete it appropriately.
