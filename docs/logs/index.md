# System Logs

## Log Entry Table

### New Log Entry Button

Clicking on this opens the [New Log Entry](/logs/#new-log-dialog) button dialog.

### Log Selection

Select the type of logs you wish to view in the table. Logs that have multiple categories selected will be shown in each of the relevant tables. 

### Search

Filter logs to ones that contained the entered search text. This search includes the note/documentation/passdown body but not the attachment names or contents. 

## New Log Dialog

The initial presentation of the new log dialog give a selection between a Log/Passdown entry and a Documentation entry.

### Log Type Selection

Once Log/Passdown is selected 3 checkboxes become available (ET Log, MT Log, Passdown). These are the categories that are available to attach to the log. The associated categories selected here will determine under which table selections the log will be displayed. 

### Date Range

Date From and Date To that the passdown covers. 

### Title

Add a title to the new log entry that's short but descriptive. This will be what's visible in the table so select something that will allow the log to be easily found later. 

### Category

Add one or more categories to the log entry. 

### Note/Description

The body of the log entry. 

This box allows use of [Markdown](https://en.wikipedia.org/wiki/Markdown) which is a very easy way to do formatting of web based text.
[See here](/markdown/) for a decent cheat sheet that shows what can be done and how to do it.

### Log Attachments

Add any files that would be useful later when referencing the log entry. 

