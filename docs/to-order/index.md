# To Order

### OI Part Number Search
Opens the [OI Part Number Search](/to-order/#oi-part-number-search) window. 

### New Item Needed Button
Press this button in order to open up the [new order dialog](/to-order/#new-order-dialog)

### Show Previous Orders
Click this button to change the table form the orders that need to be place now to the ordered that have already been placed or removed in the past

When the [current orders](/to-order/#current-orders-table) are show this button will be blue and read "Show Previous Orders", when previous orders are shown the button will be orange and read "Show Current Orders"

### Search
Enable to toggle to also search the descriptions contained within the orders.

Enter 3 or more characters into the search box to trigger a search and filter down the table to only items that contain the searched term.

## Current orders table

### Selection boxes
When the selection boxes on the left side of the table is used a text input field and button will appear. This is intended for use when there is an order number or reference of some type that can be entered along with orders. 
For example an encompass order number would be noted here, when the "Submit Selected" button is press the specified order number is attached to each selected order then that order is submitted. 

### Current Table Body
Things are pretty self explanatory here. The order name, category(ies), priority, and person who entered it onto the list. 

### Row Expansion
If the down arrow is clicked then you have an addition row where notes can be entered for the order. If [ordering](/to-order/#ordered-button) or [removing](/to-order/#remove-button) the item then the notes will be added along with the task for display when the order is looked up in the [previous orders table](/to-order/#previous-orders-table). 

### Ordered button
Pressing the ordered button will mark the item as being ordered on the day of the button press and send the item to the [previous orders table](/to-order/#previous-orders-table).

### Item Pending Status
Displays if some or all of the items within a To Order entry is already set to pending. 

### Remove button
Pressing the remove button will mark the item as removed on the day of the button press. A note is recommended to explain why the item was removed instead of ordered but is not required. This item will then be moved to the [previous orders table](/to-order/#previous-orders-table).

## Previous Orders Table

### Previous Table Body
Things are pretty self explanatory here. The order name, category(ies), priority, and person who entered it onto the list. The final column will state is the item was [ordered](/to-order/#ordered-button) or [removed](/to-order/#remove-button) and the date. If the order had an order number attached that will also be displayed. Removed items will have the text highlighted in red. 

## New Order Dialog
Details about each individual field on this page is provided in the [Edit Order Page](/to-order/#edit-order)

## Order Details
This page will display the entered information for the selected order. 

If the vendor data for a given order has *Oceaneering* or *OII* set as the vendor mousing over the part number will attempt a search to gather data for that specific part and show it in a popup. 

## Edit Mode
To enter edit mode click on the orange edit button on the left hand side of the window. 

## Edit Order

### To Order Name
Name the item, this should be descriptive enough to know what you are talking about but lengthy descriptions and links should be added to the [description](/to-order/#description) instead of the name

### Priority
Setting a basic priority to the need for this item. The default sort order for the [order table](/to-order/#current-orders-table) is high priority to low priority.

### Category
The dropdown will display all existing categories that have been used in orders past and present. You can select one of the existing ones or you can create a new category by typing into the box and pressing enter. More than one category can be selected. 

### Quantity Needed
*depreciated* How many should we order?

::: tip
This box is removed on newly added orders as of version 3.7.13. The quantity has been moved down to the vendor item row so multiple items on the order can have separate quantities. 
Old orders will still show a previously entered quantity value but new orders or edits will now allow this value to be entered in favor of line item quantities. 
:::

### Needed by
Is there a drop dead date that this part is required by? Useful for order related to upcoming tasks that will be due at a certain time. 

### Add Vendor
The dropdown will show existing vendors that have been used on previous orders. Feel free to select one or type in a new one. Once the vendor is entered type the relevant part number that corresponds to that vendor. Type the quantity required for this specific part in the request. Then press the blue add button to save that vendor/number/qty combination to the order.

Once an item is put onto an order use the Set Row Pending button to mark it as having been ordered. Once all the items are ordered then the entire To Order item can be set as ordered, preferably with a note attached for later reference if needed. 

In the event of error you may remove a vendor using the red button to the right of the row on the vendor table, re-add the vendor with the correct information after removal. 

::: tip
If the entered part number is some or all of an existing part number existing in inventory or is already on an existing order the part number box will turn red once you click out of it. You can then click the ! icon to the right of the field to see the one or more similar part numbers. Use this for comparison and to decide if this part should be ordered or possibly modified within the existing inventory. 

The number of months for previous order checks can be set with the [to order month setting](/settings/#to-order) in the Site Settings page. 
:::

### OI Part Number Search
If the entered vendor is Oceaneering or OII then an additional button will appear. This button allows the user to search for OI part numbers based on part number or description of the part. 
The search will find part numbers or descriptions that contain any of the individual terms provided. If you search for *fitting tube* you will receive results that contain the word *fitting* and/or the word *tube*. 
If you would like to search for an exact phrase wrap it in quotes. *"fitting tube"* would return only results that have those two words, in that order, with a single space between them. 

The switch between the search phrase and the refresh button allows the user to search vendor specific part numbers and find their OI part numbers. All vendor numbers are not there but many of the common vendors in use should have most of their part numbers loaded properly. 

You may click on the image or the info button to open a pop-up that contains more information on the item.
Information contained within could include the below as the information is available:
 - OI Number :  The Oceaneering part number of the part
 - Description : The Oceaneering description of the part
 - BU : The associated business unit that generated the part number
 - Status : Within the listed business unit the current status of the part
 - Cost : The last reported cost of the item as of the last part number information scrape
 - UOM :  The default UOM of measure the part is ordered as
 - "# on Hand" : The number of this item on hand at the listed business unit as of the last part number information scrape
 - 12 month usage : The usage of this component at the listed business unit in the last 12 months
 - Manufacturer : Manufacturer Name
 - Manufacturer Part Number : Part number used by item manufacturer
 - Oceaneering Vendors and their associated Part Numbers

::: tip
The database with part numbers and descriptions will be updated along with program updates and should not need end user intervention. 

The pictures will need to be added by a system admin, if no pictures are added the search will still work without them. The picture archive is more than 800 MB so it will not be bundled with the app. 
The pictures will be available for download once a suitable long term location is found. Once that occurs it will be described here for access. 

The pictures should be placed in supportDocs/ExternalUpdate/Pictures. Within this folder each picture should have a name that matches it's relevant OI part number (all .jpg format). When the search is done the program looks here and matches file names to OI number in order to present the appropriate pictures. 
:::

### Tasks related to order
Select the tasks that are related to this order from the dropdown. 

### To Dos related to order
Select the To Do's related to this order from the drop dropdown

## Description
Type a detailed description about this item, add links to possible sources if required/available. 
Links will be auto-detected and clickable by default.

This box allows use of [Markdown](https://en.wikipedia.org/wiki/Markdown) which is a very easy way to do formatting of web based text.
[See here](/markdown/) for a decent cheat sheet that shows what can be done and how to do it.
