# Settings Available

## Site Settings

### Site Name (Admin Only)

The Site Name set in this box will be the name displayed at the top of the navigation menu. This name will also be the site name as it appears when sending email summaries for safety, tasks, todo's, etc.

### Currently Installed Version

Displays the current version of the site running on the server. If the internet is available then a release notes link will appear which will allow you to go directly to the notes committed with that version of the software. These notes will show the changes of each progressive version of software. Bugs, Additions, etc.

### Inventory

Change this setting to allow all users to modify inventory items as though they are an admin. This can be limited time access for initial inventory or left accessible longer term.

### To Order

When entering a new item in the [new order dialog](/to-order/#new-order-dialog) page a check will be performed to see if there is a similar part number in inventory, currently pending orders, or recently submitted orders. This value is the number of months to look back at previous orders and consider them relevant for results. This should be set to the approximate number of months it usually takes from order submission to part retrieval.

### Default Theme

This is the default theme that will be applied when the page is loaded if the browser/OS does not support [native app themes](/#quick-theme-toggle). The theme can still be changed by clicking on the Maintenance logo but when the page is reloaded it will default back to the selection made here.

### Menu Style

The menu can be toggled back and forth between a hidden menu that can be opened via the menu button near the logo or a persistent menu that shrinks and grows based on the mouse position.

#### Menu Due Values

This option toggles the display of due items counts under each page header in the menu bar.

### Visible Menu Items (Admin Only)

The items selected here will be visible in the menu. Items that will not be used at this site can be disabled in order to have a menu that is more relevant to this specific location/hardware set.

### Task Mode Type (Admin Only)

Classic task mode will allow user to select a predetermined [set of modes](/modes/#available-modes) with some functional differences in what is visible/comes due based on these modes.

Custom mode allows an admin to custom name and color a set of modes that a user than then select to indicate hardware status when required.

When custom mode is selected some additional boxes will be shown. These are the available modes for a user to select. The visibility icon on the left can be toggled to enable or disable a mode, the name can be set to whatever is required, and the pallette on the right can be clicked to cycle through available colors for highlighting of the mode.

### Task Settings (Admin Only)

The "Task mode shown/Task mode not shown" option toggles where or not the current task mode is shown above the task table on the Maintenance Tasks page. This is used as a visual indication of the mode for users who may be limited in available tasking based on hardware status.

The "Users can delay tasks/Users cannot delay tasks" option toggles the ability of general users to [delay tasks](/maintenance-tasks/#add-task-delay) based on general need or external\forced (things like missing parts, etc) items.

The "Show freq column on task table/Freq column not shown on task table" option toggles if the task frequency column is display on the maintenance tasks table.

### Add attachment names to completions (Admin Only)

Slugs of text may be entered here and upon submission of a task any attachments that contain one or more of these slugs will have the attachment name added to the task completion for later reference.

This can be used for thing like documenting the revision of a procedure attached to a task that was active at the time of task completion.
Assuming good record keeping on the procedure then it will be known later if the task was completed with an out dated version of the document at the time.

::: warning
The slugs are case insensitive and will find any instance of themselves in the attachment name.
So Rov, rov, and RoV are the same as far as matching goes.
A slug of ROV will match ROV, rov, RoV, rover, disapprove, unapproved, and anything else that contains "r" and "o" and "v" in that order.

This may cause you to get extra file names in the completion list that were unintended.
There is not a limit to how many file names can be added so this is not an issue from the program's end but it's good to be aware it could happen.
:::

### Task Import (Admin Only)

Here you can download an excel template that can be used to fill out multiple tasks and do batch imports into the site. As described below:

1. Download the excel template by clicking the Download Import Template button

- Within the excel sheet fill in:
  - Task ID, This can be a number or string, number will become the task ID and a string will become a Custom Task ID and a Task ID number will be auto generated. (this field is optional, the program will auto-assign a value if left blank)
  - Task Name
  - Description
  - Last Completion Date (in YYYY-MM-DD format)
  - Operation Mode Frequency (this should be entered as a number in days. Words like "Monthly", will not parse themselves into a days value)
  - Transit Mode Frequency (in Days)
  - Categories (as a comma separated list)
  - Task Hours

2. The name of the file should remain as "MaintCenter task import template.xslx" in order to have a successful import. The import process checks the name before it will import.
3. Once you have verified the file contains the required tasks and is still name properly, select the file for upload or drag and drop it onto the open browser window. The tasks should import and a notification will be display letting you know how many tasks have been imported.

**Do not upload the same sheet or separate sheets with the same tasks multiple times unless you want multiples of the same tasks. There is no check on import to verify that a certain task does not already exist in the database.**

### Edit Selection Dropdowns (Admin Only)

This dropdown allows the editing of the common dropdown selections in the site such as meter check intervals and task frequency options.

1. Select the menu you wish to edit.
2. Select the option you wish to edit within that menu, or create a new selection option.
3. Fill in the desired menu display text and value.
   - "Text to display" will be the option visible to the user ex: Weekly
   - "Numerical value to be associated" will be the number associated to that text option ex: 7
   - There will be a note with information as to how the number is used for that particular menu, for example Frequency options numerical value is the number of days between when a task comes due.

## Admin Settings (Admin Only)

This page gives access to basic admin functionality such as:

- Adding/Deleting users. Resetting user passwords.
- Existing user can change their own password and email address.
- Existing sites can be added or removed.

The initial user will be created as below if an admin user does not exist in the database.

User: admin
Password: Ocean123

Upon first login the site will prompt for a new admin password.

### Existing Users

The existing users table will show a list of the other users established on the site.

The table displays the existing user's username and email address.

There are three options on each user's row.

- Remove Existing user
- Edit user's email address
- Reset user's password, this will reset the password to Ocean123. The user will be required to change it on next log in.

The button at the bottom of the table allows addition of another user. Click the button and fill out the resulting fields, then click Save New User.
The new user will have an initial password set of Ocean123.

### Current User

This table display's information for the currently logged in user. Click the appropriate button to change the current user's email or password.

### Existing Sites/Hardware

This table will display the existing hardware sites currently established on the server as well as their site name and relevant path.

Click the button on the right site of the site rwo to remove this site form the server. The resulting popup will explain what "deletion" consist of.

The basic's are that the task and other items are not removed at all. the only thing leaving is the settings for that site specifically. You can revive that site at any time later by visiting the site it used to live at or by adding it via the below form/link.

### Adding a new site/hardware

Fill in the form with the path you would like the site to live at. A link will appear below the form with an example of how that site would look in the browser.
Please try to avoid special characters or spaces in this name. Short and sweet is better as this will only be shown in the site path. The Site Name can then be longer and more complex for display purposes.
When you are happy with it click save. That page will now be available at the shown link as well at in the hardware menu on the nav bar and landing page.

## Meters

The meters page is a place where the meters used for tasks can be modified or which tasks use a meter can be found.

New meters are created from within the [task dialogs](/maintenance-tasks/#meter-used)

### List

The list button will pop up a windows listing all tasks that currently use this particular meter.

### Edit

The pen icon will toggle edit mode for the appropriate meter.
**This is not where you do a normal value update!**
This is only used when a meter is changed. When a value is update here is will adjust the due time for all associated tasks.

For Example:
If you have a meter which currently has a reading of 750, and a task that will come due at 1000. You meter fails and needs to be replaced.
You change the meter with a brand new meter whose reading is now 0, you would come to this location and set the new meter reading to 0.
The app will adjust the task to now be due at 250. (New meter reading plus the interval until the task was to come due)

In order to update the meter as a normal task search for the meter name in the task list and find the task whose name is shown as updating the reading for the needed meter.

## Deactivated tasks (Admin Only)

The deactivated tasks page is exactly what it sounds like. It lists tasks that were [deactivated](/maintenance-tasks/#deactivate-task) so they no longer show up in the tasks page. Click Re-Activate in order to push the task back to the maintenance tasks page.

## Inventory (Admin or Inventory Mode)

### Locations

#### Location Tree

Selecting a location on the tree will reveal options involved with that location.

##### Rename Location

Rename the currently selected location

##### Add Child Location

Add a child location to the currently selected location.

##### Move Location

Move the selected location. Once the move process is initiated a second location will be able to be selected that will be destination for the move.

##### Remove Location

This option is only available for locations that do not have any inventory items stored in themselves or any of their child locations. Once all items are clear of the appropriate locations the button will be available. A confirmation window will appear and once the confirmation is clicked the selected location and all it's child locations will be permanently removed.

##### Add item to (Location)

To create an inventory item, select it's immediate parent location then fill in the add item box as required.

Part number adding will not allow a duplicate of an already existing part number. An error will occur and the red information icon that appears can be clicked for a display of where the existing part number is already stored.

If Oceaneering or OII is the vendor for a pending part number entry a button will appear that allows a (search of known OI part numbers)[/to-order/#oi-part-number-search-2]. If the description of the item if left blank and an OI part number is selected fro the OI part number search box the description of the item from the database will be filled into the description box automatically.

### Inactive Items

Items shown here are previously created inventory items that have since been deactivated. Clicking the reactivate button will put them item back in inventory. Upon reactivation the item needs to have locations assigned/reassigned.

## Hose Registry Settings (Admin Only)

Here you set the Hose Size options, default part numbers, max hose size that can be made on site, and the default/max intervals for inspection and change of hoses.

### Default Inspection Period

Set the default Inspection period for hoses here. This number will auto fill into new hoses that are created as the inspection interval. This number can still be changed on an individual basis but cannot exceed the maximum allow inspection time.

### Max Inspection Time Allowed

Set the maximum allowed inspection time for new hoses and hose edits. Changing this number will not retroactively go back and bring previously created hoses down to this if their interval exceed this number.

### Default Change Period

Set the default change period for hoses here. This number will auto fill into new hoses that are created as the change interval. This number can still be changed on an individual basis but cannot exceed the maximum allow change time.

### Max Change Time Allowed

Set the maximum allowed change time for new hoses and hose edits. Changing this number will not retroactively go back and bring previously created hoses down to this if their interval exceed this number.

### Max Hose Size

This number is the maximum hose size that can be made on site.

- Hoses this size and smaller will populate the order based on raw materials filled into the hose part numbers of that hose size.
- The hoses that exceed this size will populate the order as individual hoses lengths/sizes.
- The exception to these is hoses that have an Oceaneering part number will show up that way instead of as raw hose or individual lengths.

### Hose Size options

The hose size drop down can be edited here.

#### Add Hose Size options

Click the add icon to the right of the dropdown in order to add a hose size option to the dropdown. Fill in the [hose data](/settings/#add-hose-size-options) and click save once completed.

#### Edit existing hose option

Select an option from the dropdown that you would like to edit and it's existing data will populate the resulting form. Click save once completed.

#### Hose Options Data Points

- Dash Value of Hose - This is the [dash value](https://www.hoseandfittingsetc.com/technical-info/tips/dash-size) that is appropriate for the hose size.
- Text Label - This is the label that will actually appear in the dropdown. Make it obvious what size it reflects as it's the only visible component to the user when making the selection.
- Hose Part Number - This is the part number used for the raw hose in the [order list](/hose-registry/#order-info).
- Hose Pressure Rating - The pressure rating of the hose if required.
- Fitting Part Number - The part number of the fitting on either end of the hose and counted up for the [order list](/hose-registry/#order-info). This will be filled into Fitting A and Fitting B but each can be change individually on a hose if required.

## Auto Emails

This table shows the dates and days related to automatically generated emails. The checks for these emails will run once when the server is start/rebooted and then again once a day thereafter. These emails will send regardless of having the web interface open.

### Auto Email Settings Table

#### Auto Email Frequency

The table displays the surface data and number of days between sends. Setting the "Auto Email Frequency" to 0 days will disable that specific email from sending.
When turning on an email that was previously disabled the next send date will be sent relative to today and the [next send date](/settings/#manual-next-send) will be shown as the [next send date](/settings/#manual-next-send) in the following column.

#### Date Last Sent/Next Send Date

The date of the last AUTO email send is displayed here above the horizontal line. This does not count the test emails send from within the ["Configure/Test"](/settings/#configure-test-dialog) dialog.
The date of the next intended send is shown below the horizontal line. If no [manual send date](/settings/#manual-next-send) is set this will show the last send date plus the number of days specified in the [auto email frequency](/settings/#auto-email-frequency). If a [manual send date](/settings/#manual-send-date) is set then that will be shown instead.

#### Configure/Test

This button opens a dialog to allow configuration of this specific email.

#### Set Search Criteria (Task Export Only)

Opens the dialog to [set the task search criteria](/settings/#set-task-search-criteria-dialog-task-export-only) for the task export email.

### Configure/Test Dialog

#### Email Subject

Sets the subject of the email to be sent. Can be used to allow search/sort criteria in the receiving inboxes. The hardware identifier will also be added to the email when sent in order to allow differentiation for people who may get similar emails from different systems.

#### To CC BCC

These are just as they appear. They will specify the To:, CC:, and BCC: fields of the email. Emails can be put in as two different formats.

- Directly typing the email address: someone@email.com
- Specifying the users name alongside the email address: John Doe <JDoe@email.com>

Either choice will work, the only difference is the way the name is displayed in the receiving party's inbox.

#### Manual Next Send

Clicking the "Set manual date for next send" button will reveal a date selection dialog. This dialog allows the user to specify the next date this email will sent. The last send date and interval will big ignored in favor of this date. Once this send date is reached the normal send interval will continued the emails form that date as specified.

#### Test Email

There are two test options:

- You may click the "Test by sending to all addresses" button in order to send an email to every address specified above. Email be marked as (TEST) in the subject line.
- You may specify a single email address to send a test email to if you want to do a limited test to yourself or someone else. Email be marked as (TEST) in the subject line.

### Set Search Criteria Dialog (Task Export Only)

The set search criteria dialog is a modified version of the [task completions export](/exports/#task-completion-exports) options. There are a few exceptions as noted below.

- The settings display a sample se of results based on the set criteria. These are just for example purposes so the user may see what would be export if the email were sent today.
- Because exports will be dynamically generated based on the date the email is sent the "End Date" option is not selectable. It will always be the date of the email send and therefore it will be the day you are doing the setting selection when this window is open.
- The download export button has become a "Save Export Criteria" button. This button does exactly what it sounds like. Saves the selected criteria for later use when sending auto emails.

::: warning
These setting are also used to generate a local version of completions export in parallel to [backups](/settings/#backups-admin-only). When an automatic or manual back is generated an export of these is also generates to a folder name autoTaskExport within the tenant directory. This file will be there for 90 days before it automatically deletes in order for an admin to have the ability to copy it out to another folder if a local export is required.
:::

## Backups (Admin Only)

The table shows the current database backups.

### Create Manual Backup

The create Manual Backup button will create a backup right now and overwrite any backup that may have already been made for today.

### Quantity/Frequency options

These settings here allow the user to choose how often the system creates automatic backups and how many backups are on file before the oldest one gets removed.

### Backup table

- Download Button: This button will create a zip file of the requested backup and download it to the computer.
- Restore Button: Will open a window allowing the user to select which portions of the backup they wish to Restore.
- Delete Button: Delete the appropriate backup.

### Download Entire Archive

This button will create a zip file of the entire folder structure of supporting file ont he server. This includes all backups, files attachments for all pages, etc.

### Upload local backup

You can drag a backup zip archive onto the screen or select it using the select files button. Once uploaded the archive will be tested to make sure it was taken from the curently active hardware and if so the database will be restored to the state it was in when the backup was made. You may select only certain portion of the backup to restore in the resulting popup.

## Logs

This page allows the viewing or downloading of the maintenance log.

Logs will automatically remove themselves 2 years after creation to avoid the log database from growing to an unmanageable size.
