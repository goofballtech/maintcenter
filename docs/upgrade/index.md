# Upgrade Processes

## Automatic Upgrades

If your instance of the maintenance software can see the internet it will self upgrade features and bug fixes. The system is set to look for updates once a day by default.
If that is too frequent due to bandwidth issues please put in a request on the [issues page](https://gitlab.com/goofballtech/maintcenter/-/issues) (internet required) and we can move it out to whatever interval works better for your site. 

If for any reason you need to try and force a manual upgrade, you can go and run the [update script](/upgrade/#update-script) to do so. The download a bit more than 100 megs so it may take a bit depending on connectivity. 

## Manual Upgrade

If your site does not have internet access you can download the update manually and install the new image onto the server. 

In order to download the image you must first head over to the [pipelines page](https://gitlab.com/goofballtech/maintcenter/-/pipelines) (internet required) where the builds are posted. If you are navigating to it manually it's below the CI/CD page in the navigation menu. 

Locate the most recent version of the master brach available. It's should be marked with a green "latest" flag. 

* If the button on the far left says anything other than passed a download may not be available and the latest tag will not be marked on that release. 
You may have to scroll down a bit to find the latest flag on an earlier build. 

![Latest Master](../assets/images/master-latest.jpg "Latest Master")

On the right-hand side of the appropriate build there is a small cloud icon, when that icon is selected an option to download the artifact is visible. 

![Download File](../assets/images/download-master-artifacts.jpg "Download File")

The downloaded file will have a .zip file extension. Within that file will be a folder named DockerFileSave. 
In the DockerFileSave folder will be a file with the extension .tar. That is the file needed for the manual docker update. 

On the Synology where the maintenance program is hosted:

1. Log into the Synology admin interface. Open the docker app from the main menu. 
2. Once the docker app is open click the image category on the left hand side. 
3. Within the image category there will be an add button near the top of the window. 
4. Once you click the add button you will have two selections, from URL and from file. Select from File. 
![Add Image](../assets/images/dockerAddFromFile.jpg "Add Image")
5. If you have already transferred the file to the synology you may locate and select it there. 
6. Once the master.tar file is selected press the blue bottom in the bottom right corner to finalize your selection. An upload window will appear and it may not always change visibly to monitor progress, this alone is not a sign of a failed attempt. 

If not select the Upload option which will allow you to pick it from the local computer. 
You can monitor the progress of the upload if you are uploading the file from a local computer on the bottom right corner of the browser. 

This will install this image in place of the currently installed maintcenter:latest docker image on the server.

You will then have to go an run the [update script](/upgrade/#update-script) on the synology. 

## Update Script

In the Synology admin interface go to Control Panel -> Task Scheduler -> Right click on the "update" script and select run. 
If connected to the internet then the program will force a check for a new version of the container. If one is found it will download that version.
If no new version is found or if no internet is accessible then the containers should shut down, update, then restart. 
If you would like to monitor this you can look at the containers tab within the docker window on the synology. The container will disappear then come back up if the script is executed properly. 

You may then verify the update had completed successfully by looking at the installed software version number on the maintenance site settings page if needed. 
