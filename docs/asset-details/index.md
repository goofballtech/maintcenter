# Assets

## Asset Table

### New Asset

Click this button to open the New Asset dialog.

### Search

The search box will allow the user to filter out assets shown in the table to those that contain the query.
3 or more characters have to be entered into the box before the search is triggered.
By default the search looks at the data visible within the table.
If required, the toggle switch near the search box allows the user to include the asset descriptions in the query as well.
This adds a bit of time for the search to complete but can certainly be useful if the required term is not included in the visible data.
In order to clear the search simply delete the search text form the search input box.

### Asset Table

Click on the appropriate asset int he table to open the [Asset Details Dialog](/asset-details/#asset-details-dialog)

# New Asset Dialog

## Location

Create an initial location for the asset. The drop down allows selection of previously used locations or a new location may be entered. 

### Other new asset details

Please refer to the [Edit Hose Dialog](/asset-details/#edit-asset-dialog) for details on every other field available in the New Asset. 

# Asset Details Dialog

## Asset Edit Mode toggle (Admin Only)

Toggles edit mode for the below detailed information

## Asset ID

The ID of the selected asset. 

## Asset Name

The name of the selected asset. 

## Date Acquired

The date the asset was acquired. If this date was marked as estimated this is also noted between the title and the date. 

## Date In Service

The date the asset was placed into service. If this date was marked as estimated this is also noted between the title and the date. 

## Category

The category or categories that are applied to this asset.

## Current Location

The description of the current asset location is shown here.

## Manufacturer

The manufacturer of the current asset.

## Model

The model number of the current asset (if applicable).

## Serial

The serial number of the current asset (if applicable).  

## Posting Reference

The posting reference used for the asset acquisition. (More details will be provided once I figure out what this is, as of now I have no idea what this data relates to. It was put here at request.)

## Posting Date

The posting date for the asset acquisition. 

## Asset Cost

The currently applicable cost payed or replacement cost for the asset as a whole. 

## Contract number

The contract number related to this asset that outlines details for the asset requirements and use. 

## Related Items

The other items within the database that are related to this asset and would be exported along with this asset. 

## Disposition

The current disposition of the selected asset. 

## Description

Any additional information about the currently selected asset that was not captured in the above selections but needs to be captured. 

# Edit Asset Dialog

Only the details which have something special about them are noted. If editing a detail is simply changing the text provided to something else they are not specified below.

## Date Acquired

Select the date the asset was acquired. If this date is an estimated date select the associated check box to note it as such. 

## Date in service

Select the date the asset was placed in service. If this date is an estimated date select the associated check box to note it as such. 

## Category

Select all applicable categories that relate to this asset or type new ones if required. The list is populated by the categories that currently exist among all assets int he database. 

## Current Location

In order to change the location of this asset select the arrow icon near the current location display. The resulting popup will ask for information relevant to a location transfer including who requested the transfer, the date of transfer, and the description of the new location. The description field used previously on assets, a new one can be typed in if the required location is not in the list. 

## Manufacturer

The manufacturer dropdown shows all manufacturers currently applied to assets. If the required selection is not available you may type a new entry. 

## Related Items

Enabled the switch for the proper type of item you wish to related to the current asset. 

### Related Assets
Details will be filled in here as these features are enabled.
### Related Tasks
Details will be filled in here as these features are enabled.
### Related To Dos
Details will be filled in here as these features are enabled.
### Related To Order
Details will be filled in here as these features are enabled.
### Related Hoses
Details will be filled in here as these features are enabled.