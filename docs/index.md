# Maint Center Help

Welcome to the help/info page! Hopefully the information you seek is contained within. 

The search box will hopefully allow you to get straight to what you need but if you are in an exploring mood pick a page below and wander around to see what you can find. 

If there is an issue found in the documentation or application, if there is a feature you would like to request, if there is something that is not documented but you think should be, you can give [feedback on the issues page](/feedback/).  It can be found here [https://gitlab.com/goofballtech/maintcenter/-/issues](https://gitlab.com/goofballtech/maintcenter/-/issues). 

You can easily also create an issue with minimal effort by sending an email to **maintcenter+goofballtech-maintcenter-2-issue-@gmail.com**. This will create an issue in the system and start an email chain with the sending email address so all notes will come back as replies to the sending email. 

:::tip
If there is already a request opened for what you need please give it a thumbs up to show you also require that feature, comment on it if you want to add to the discussion. This lets me know what features are most in demand and need additional time put into them.
:::

Another path to the same place is to visit [https://gitlab.com](https://gitlab.com), sign up if you don't already have an account. Search for user "goofballtech" and select the maintcenter archive. The archive is publically viewable. 

For systems where the server has access to the internet the generic loading messages will be replace with a random fact. 
Also on such systems there will be a small button available at the top right of the screen. 
If you mouse over this dot it will display the current random fact in memory in the even you wanted to read it and did not have enough time while the page was loading. 

---

## Main pages on the site

- [Maintenance Tasks](/maintenance-tasks/)
- [Assets](/asset-details/)
- [To Do](/to-do/)
- [Hose Registry](/hose-registry/)
- [Inventory](/inventory/)
- [To Order](/to-order/)
- [System Logs](/logs/)
- [Exports](/exports/)
- [Settings](/settings/)

## Additional data available in docs

- [Change log](/changelog/) - View changes in the site organized by date/version number
- [Feedback](/feedback/) - Addition information on how best to provide feedback for changes
- [Markdown](/markdown/) - A reference for how to do formatting within certain dialogs that allow this syntax
- [Upgrade](/upgrade/) - When you need to upgrade your local installation, these are some way to go about it
- [Setup](/setup/) - If you were to wish to assist in programming the site, this helps you get all set up

---

## Landing Page

On the landing page at the site's root you will be greeted with buttons for each set of hardware currently established on the server. The name presented on the button is the [site name](/settings/#site-name).

## Quick Theme Toggle

The theme of the main site and documentation page are both based on your preference setting in Windows/MacOS. 
If you have the theme for apps set to dark then this page will appear in dark mode on load, conversely if that is set to light then this page will appear in light theme mode on load.  

In the main site, at any point in time, you may click on the ![Maintenance Logo](./assets/images/logo.jpg) in order to toggle dark/light theme. 
The [default theme](/settings/#default-theme) can be changed on the settings page. This is the fall back setting if the browser or OS does not properly support app themes. 

The documentation page cannot be toggled manually due to it being a generated page that is not as dynamic as the main site. To change it's theme you must change the OS app mode setting.

::: tip
In order to change the theme in Windows you can search Windows (window button + S or type directly into the search bar on the start menu/task bar) for the word "dark" and you will see a setting called "Dark Mode for Apps". When this is set to dark then the maintenance site will also be dark themed by default. 
:::
