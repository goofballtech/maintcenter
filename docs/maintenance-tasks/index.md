# Maintenance Tasks

## Task Page Selection Buttons

### Due Tasks

Due tasks is the default table that shows up when opening the Maintenance Tasks page.  
 The Due Tasks page shows the tasks that are due currently.

![Due Tasks](../assets/images/dueTasks.jpg)

The due tasks button will activate this page if it's not already active.
The color of the button indicates the current task mode. Green is ops, orange is transit, red is layup/mobe/demobe.
When your mouse is over the button the shown popup will list the current status so you are not required to memorize the colors.

The default state of the page is showing the tasks that are due within the next 1 day.  
 When the Due Tasks page is displayed a + sign is visible to the right of the Due Tasks button,
this button allows the user to adjust the tasks shown by increasing the look ahead from 1 day to an ever increasing number of days. For the first 7 days the time increments by 1 day each click then it jumps to increase a week at a time.

Once the number of days is increased a - symbol will also appear to allow the number of days shown to be decreased as well.

![Days in future for due](../assets/images/duePlus.jpg)

### All Tasks

The All Tasks button shows all active tasks in the table.

When the All Tasks page is selected a + sign can be seen to the right of the All Tasks button.
This button is used to create a [new task](/maintenance-tasks/#new-task). (Admin Only)

### Previous Completions

The previous completions button will display the last (up to) 200 previous task completions. Even though this table is limited to 200 results the completions before that are not lost.
They are stored within each task itself. To find [specific completions](/maintenance-tasks/#previous-completions) open the task in question and the information is available there.

### Tech Name/Date

In order for most tasks to be performed a name is required. Whether a name or initials is used is up to the user.
Consistency is preferred in any case.

The calender icon near the tech name boxes allows the user to submit a task on a previous day if it was missed when completed.
The default submission date is today, if a task submission was missed and you wish to change the date to submit a task
on a previous day click the icon then select the appropriate day in the resulting date selection box. Any task submitted will now reflect the modified date shown.
Click again to close the date selection to return to submission for today's date.

## Task table

### Task Print

The print button allows a task list to be made that can easily be printed in order to make a work list.
To add items to the print list select the check boxes on the left hand side of each items row then click the print button.
This will open a print window with the selected tasks shown.
In the resulting popup there is an option to "Toggle Tools Display". This button allows the user to show the tools save
in each task (if any exist) so the proper tools can be gathered for work.
The printed list will also show a form (if a required info form has been created) so when the task is being performed the
technician has a template of required information to gather for the task.
This information will need to later be entered when the task is entered for sign off.

![Print List](../assets/images/printList.jpg)

### Task Search

The search box will allow the user to filter out tasks shown in the table.
3 or more characters have to be entered into the box before the search is triggered.
By default the search looks at the data visible with in the table, ID, Name, and Categories.
If required the toggle switch near the search box allows the user to include the items descriptions in the query as well.
This adds a bit of time for the search to complete but can certainly be useful if the required term is not included in the task name.
In order to clear the search simply delete the search text form the search input box.

### Task display row

Each row of the table is displaying the data from a single task.
Task ID, Name, and relevant categories are all displayed.
The frequency column of the tasks table is not on by default but is able to be turned on or off on the [task settings](/settings/#task-settings) card of the [site settings page](/settings/#site-settings).
This column display the frequency the task will become due when in Operations or Transit mode.

If too many categories are associated to a single task the list will be truncated and a "+ 2 categories" will be displayed (with the relevant number), the hidden categories are still part of the search results though.

For the categories displayed, if you mouse over the category a popup will appear that tells you how many tasks share that specific category.
This number is information is pulled from all active tasks, so the number shown may not match what you see if you are currently in the [Due Tasks](/maintenance-tasks/#due-tasks) table.
If the category is clicked a search will be triggered with that category name. To remove this search clear the [search](/maintenance-tasks/#task-search) field as normal.

If All Tasks is the selected table the the submit button will always be green and simply submit the task when pressed.
If Due Tasks is the selected table the there are a few variants of the Submit button as listed below:

- Green - Task is currently due
- Red - Task is currently overdue (Due date has been past by the due interval ex: A task normally due at a 7 day interval came due 14 or more days ago)
- Blue and reads "Acknowledge" - The task has had a reminder set and that is what is currently displaying.
  The task name is also modified to better explain the need for a reminder. This is typically used to notify techs that a
  task is coming due and stock needs to be check, parts ordered, preparations made, etc.

::: tip
Once a task is submitted the submit button for that task is disabled until the next page refresh. A button will appear at this time that will count down to table refresh. If you are done submitting and want to refresh immediately for any reason you can press the button to do so.
:::

The down arrow on the right hand side of the task row reveals some additional options:

- Notes to be added to a task upon submission, simply type the note into the box before pressing the submit button and the note will be added
- The ability to quickly add a needed order item in line with the submission while the information is fresh. If you used the last spare to do this maintenance,
  add some to the order list here before submitting to make sure you don't forget. (Only shown if the To Order page is not hidden in settings)
- If related tasks have already been assigned then a second box will be shown that allows the user to pull the related tasks forward and make them due immediately.
  This allows related tasks to be completed together even if they are technically due at different intervals without having to look up each individual task.

Finally you can click on any row in order to open a [more detailed view](/maintenance-tasks/#task-details) of the specific task selected.

## New Task

### Clone Existing Task

If you want to create a very similar task to another you have the option to clone an existing task into the new task creation form.

Clicking on the ![clone icon](../assets/images/clone.jpg) will bring up a prompt for the task ID of the task to be cloned.
You may start typing in the box to search task names and categories to assist you in finding the proper task to clone.
Once the appropriate id is entered the data will be cloned into the new task form fields.
You may then modify the data as required for the new task.

### New Task Completion

When creating a new task there is a switch available just above the [Last Completion](/maintenance-tasks/#last-completion) box. This will automatically insert a task completion ino the table when the new task is saved.
This is typically used when a task is input in the the database only after it has already been completed and that completion needs to be recorded.

### Additional Info form

The additional info form options are not available when creating a new task. The way the form is created requires the task to already exist.
If you need to [add a form](/maintenance-tasks/#additional-info-required) to a task please fill in the rest of the task as required, save the task, then go back and edit that task to add the form as required.

### Related Tasks

The related tasks options does not show in the new task window. Tasks are tied together with their Task ID and since a new task does not yet have an ID assigned it can't be grouped immediately. Once a task is created then you must edit it to add it to a [related task group](/maintenance-tasks/#show-related-tasks-dialog).

### New Task Form Fields

The rest of the new task form is the same as the [Edit Task](/maintenance-tasks/#edit-task-admin-only) form, please refer to there for details on each field

## Task Details Popup

### Edit Task Button

Clicking the orange task edit button will toggle the [edit task](/maintenance-tasks/#edit-task-admin-only) page. (Admin Only)

### ID

Task ID for the current task. If a custom ID is set then that is shown here instead.

:::tip
Regardless of the custom task designation the back-end file structure still uses the common task number. This file structure is based on the task ID shown at the top of the window rather than the custom task ID.
:::

If the current task has any [related](/maintenance-tasks/#show-related-tasks-dialog) tasks the background of the ID header will be colored
orange and the related tasks will be displayed if the user hovers their mouse over the colored section. This task list will contain tasks that have been assigned to this task as well as other tasks that have been related to this one in there own edit screen.

:::tip
Keep in mind that this related tasks list is only one layer removed.
For tasks A, B, and C. If A is set as related to B, and B is set as related to C, then this context dialog for A will only show B. This dialog for B will show both A and C. Finally, C will only show B.
:::

### Name

Task Name for the current task.

### Previous Form Data

Only shown when [Additional Info Required](/maintenance-tasks/#additional-info-required) is set.

If an icon is visible to the right of the task name that looks like this ![No Form](../assets/images/noFormInfo.jpg) then the shown task has an
additional information form created but there has not yet been any form data entered so nothing is available for display.

If that icon is blue ![Form Data Available](../assets/images/formInfoAvil.jpg) then form data has been entered.
If you click the icon the form data will be displayed.

### Category List

One or more catagories that are applicable to the current task.

### Last Completion

The last(most recent) completion date for the current task.

### Previous Completions

If more than one completion has been done on this task the Last Completion header will be colored orange.
Click on the orange header in order to open a popup that will display all completion dates and names for previous completions of this task.

## Time based tasks

### Ops Frequency

The frequency that this task will become due when the system is in the Ops [task mode](/maintenance-tasks/#task-modes)

### Transit Frequency

The frequency that this task will become due when the system is in the Transit [task mode](/maintenance-tasks/#task-modes)

### Force Due Date

Turning on the Force Due Date switch shows a date selection as well as a reason for a forced due date.
This date would typically be used for things like safety gear which has a hard expiration date from the manufacturer but is not limited to that purpose.

An example use case and the problem it solves:
An eye wash station comes with fluid that has a set expiration date. The fluid is good for one year from when it was manufactured.
You order it from the vendor and install it when you receive it. As expected the manufacture date and install date will not be the same.
So when you install it as an "annual" task now the fluid will expire before the task actually comes due unless you cheat the system and post date the install.
This option allows you to set the expiration date of the fluid without having to falsify the date of the actual work.
You would then note that the forced date is based on the expiry and update the forced date when the bag is changed in favor of the new expiration of the replacement fluid.

### Next Due

The date which this task will once again become due in the currently active [task mode](/maintenance-tasks/#task-modes)

### Add Task Delay

The ![add delay](../assets/images/addDelay.jpg) button will show a dialog where the user can select to apply either a fixed delay to the task or a forced delay based on an existing To Do or To Order item.
The visibility of this button is selectable by an admin on the [task settings](/settings/#task-settings) card of the [site settings page](/settings/#site-settings).

### Fixed Task Delay

A fixed delay will push the tasks due date by an certain number of days. This offset is added to the existing due date on the task, not today's date. Once this delay is applied the task will now become due once again as the adjusted date.

### Forced Task Delay

A forced delay is caused by an external factor. Either missing parts that need to be ordered or a To Do that needs completing before the scheduled task can be performed successfully.

- If the related To Do is completed and there is no related order item as well then the hold is removed and the task returns to it's normal due date.
- If the related To Order is ordered and there is not related To Do item then the task is pushed to it's normal due date but a note is added to the categories to note the required items are ordered. Since there is no way to distinguish ordered vs received yet the item will sit int he due list for a bit in this state until the items arrive and the task is signed off as usual.

### Remove Task Delay

To remove a task delay manually the user can hit the ![remove delay](../assets/images/removeDelay.jpg) button in the Task Details popup. A task submission will also reset the delay set on the task.

---

## Meter Based Task Fields

### Last Value Completed

The last meter reading value which this task was completed.

### Meter Interval

The number that will be added to the last value completed in order to set the next due meter reading.

### Next Due

The next meter value where this task will become due.

If you mouse over the next due you can see the meter related to this task as well as the most recent recorded value for that meter.

---

### Due Notification

Only shown when [Notify Before Due](/maintenance-tasks/#notify-before-due) is set.

Notifies the user that a reminder has been set and how long before the task being due it will insert a message.
This notification changes slightly for the time between the notification and due date to let the user know the notification has already fired and the user that acknowledged that notification.

### Highlighted Attachment

Admins have the ability to highlight a specific task in the attachment list. Once a task is highlighted it will appear here for easy access.

### Description

A detailed description for the task.
Links will be auto-detected and clickable by default.

## Task Notes

### Notes

The Notes Button will open the notes popup to display all previously entered notes for this task, if there are any to display.

### Add New Note

The + button on the right side of the note button will open an additional dialog that will allow a new note to be added to the task.

## Tools List

Display/Edit tools required for this task.

The intention of this list is to remind technician of tools required to complete this job. Whether that be standard wrenches needed and save trips back into the shop
or specialty tools made for this task that are stored somewhere special to prevent them being thrown away accidentally.
This is just things that need to be remembered between completions to make life easier.
When [printing a task list](/maintenance-tasks/#task-print) this can be shown and/or printed as well to allow tools to be collected before work begins.

## Attachments

Clicking the attachments button will open a popup that displays all attachments that have been added to this task.

You may search and filter attachments by name. If there is more than one attachment a button will be visible that allows a zipped version of all file attachments to be downloaded.

Admin's have the ability to highlight a single PDF attachment that will be shown on the task details page and can be downloaded directly form there. Users can see which attachment is highlight but do not have the ability to change the highlighted attachment.

Each attachment has a delete button to be able to remove the attachment. (only visible to logged in admins)
**This also deletes the file** so do it with care as they cannot be recovered once deleted.

The left hand column will show an icon for files that can be opened directly in the browser. For image files a preview thumbnail will be shown. When clicked
these images will be displayed in the browser. If a download is require you may click on the files name in the second column to initiate a download in the browser.

In order to upload new files you may either click the _select files_ button and locate the files required or drag the needed files onto the attachment window.
:::tip
You can also drag a folder over and the program will grab all files in that folder and attach them individually.
:::

Once the files needed are added to the pending upload list which will be shown between the attachment table and select files button. Click Start Upload and the progress for each file will be shown.

Today's date will be added to the end of any file that is uploaded to prevent overwrite of similarly named files over time.

:::tip
Server Admins: On the synology the attachments are located at _/volume1/docker/supportDocs_. The folders from there are sorted by the site short name, page name, then ID. For tasks this would be the task ID number. Adding files to this folder will cause them to show up as attachment when the task is clicked. Therefor this entire folder can be backed p and restore if need (for example if a hardware swap is required)
:::

# Edit Task (Admin Only)

### Created/Edited Date

Across the top of the edit page the date the task object was created and the date the object was last modified will be displayed.
Modification is any time the task object is changed which includes a submission of the task, note added, etc.

### ID

Read only display that shows the task ID

### Name

Task name that will be shown in [task table](/maintenance-tasks/#task-table) and on [printed task lists](/maintenance-tasks/#task-print).

## Toggle Options

### Meter required

Used to signal that the task needs to be set due based on a meter reading as well as or instead of the set frequencies that are time based.

This toggle is only visible if the [Additional Info Required](/maintenance-tasks/#additional-info-required) toggle is not active. Only one or the other can be active on a given task.

Once this toggle is active a new row of form inputs will be visible. See [Meter Based Task](/maintenance-tasks/#meter-based-task) options for details.

### Show Related Tasks Dialog

Toggling this box will display the Show Related Tasks selection form. This can be used to signal to related tasks together for any reason.
When signing off one task which has relations you also have the option of [pulling the other related tasks](/maintenance-tasks/#task-display-row) that may not yet be due forward to make them due immediately.

This pull forward will only last until the next [task mode](/maintenance-tasks/#task-modes) change, so if something is pulled forward in error it will move itself back into the normal flow in time if not signed off before then.

This could be useful when you have tasks that are more convenient to be done when you are already in an area, you have some tasks that are inherently included when completing others, etc.
EX: when opening the telemetry can you must remove all whips, so if you open the can to do ground fault checks you can also likely sign off checking whips, connectors, water alarms, etc.

An important note to remember is that the related tasks bind both ways. So if you have a group of 5 tasks that already exist and you assign a 6th task to be related to a single task that is already part of the original 5....
**Now all 6 tasks will be related to each other.**

### Notify Before due

This allows the user to inject a notification into the task list before the task actually becomes due.

This is used to notify the technician that a task is coming due and can be used for ordering parts required or preparing for the task if it requires scheduling.
EX: setting a reminder when an armored re-termination is up coming so the wirelock can be checked to be in date for use.

When using this notification 2 items need to be set.

- The number of days before the task becomes due to inject the notification
- The message to be displayed for the notification

This notification message will appear on the due tasks page in place of the task name.
In the above example and couple say something along the lines of "Armored reterm is coming due, please check for in-date wire lock compound"

The user may choose to fill in one or more email addresses into the email notification box.If valid email addresses are filled in an email will be
auto-generated and sent to the specified recipients at the same time on the same day the notification task is added to the task list.

If the server is down for any reason and the email is not sent before the tasks due date an entry will be added to the log but the email will not be sent.

### Additional Info Required

This allows the technician to build a form to allow additional information to be entered when a task is signed off.

This option is only visible when the Meter Required toggle is disabled.

Once enabled for the first time a button will become visible called "Create New Form" When this button is pressed a dialog will appear to assist the user in creating the appropriate form.

This form will first ask for the groups required then it will ask for the fields within each group. This allows the technician to build a form that is sectioned off by category.
EX: For High Voltage checks on the ROV. You would have 3 groups: ROV, CAGE, and Console. Within each group you could then create the required fields. You would end up with something like the below.

- ROV
  - Voltage
  - Current
- CAGE
  - Voltage
  - Current
- Console
  - Max Current

There are many way to structure each form. You could also do Current and Voltage groups and have ROV, Console, and CAGE be the fields. This is up to the user to decide how these will be arranged.

With each field you have an option for that field to be Text or a Number, if number is set there is also an option to set a min/max value allowed to be entered.

This form will end up in a few places once created. When a form is made and that task is included in a [print list](/maintenance-tasks/#task-print) the field will be shown as a template on the printed form.
When signing off a task the form will appear and must be filled out in order for the task to be submitted.

In order to view the previously entered data see the [task details popup](/maintenance-tasks/#previous-form-data)

---

### Category Edit

The category/categories applied to this task. All the selection options in the dropdown are the categories that are on all the rest of the active tasks in the database.
If you begin typing in the field the list will be limited to results that match what you are typing. If the category you wish to enter is not already there then type what you want and press enter.
This category will be applied to this task which means moving forward it will be available for selection in the dropdowns of other tasks.

### Task hours

The number of hours, on average, it take to complete this task.

### Last Completed

The existing value here is the last completion date of this task. Changing this value directly will adjust the next due date of the task but will not apply a task completion in the database.

---

## Frequency Based Task

### Ops Frequency

How often the task will come due when in the Ops [task mode](/maintenance-tasks/#task-modes)

To change the options available here see the [selections dropdowns](/settings/#edit-selection-dropdowns)

### Transit Frequency

How often the task will come due when in the Transit [task mode](/maintenance-tasks/#task-modes)

To change the options available here see the [selections dropdowns](/settings/#edit-selection-dropdowns)

### Transition

The transition interval is here to allow the technician to defer tasks when coming back from the layup [task mode](/maintenance-tasks/#task-modes).
What his option does is allow some tasks that came due while the system was in layup to be delay by a percentage of the normal interval.
If everything comes due at once things can be a bit overwhelming. You may set transitions for those tasks that are less important or that can be delayed slightly to allow the more time sensitive tasks to take priority.

EX: If you want to delay greasing the guide tubed on the levelwind because it does not have to be done as soon as the system is manned, just before the first dive.
If this task is normally set to be monthly then it would be done every 30 days, you would then set the transition to 25% of normal interval.
Then when the system comes from Layup to transit modes and this task would normally have become due.
Now it will be delayed by 8 days (30 days X .25 = 7.5 rounded up to 8 Days) so it will not come due officially until a week after the change from the layup [task mode](/maintenance-tasks/#task-modes).

This is an option and will default to everything becoming due at it's normal time. The intention is that technicians will go and delay tasks one by one as the system comes out of layup
and the crew realizes the items that aren't required to be done immediately and can be delayed in favor of others.

---

## Meter Based Task

### Last Meter Reading Completed

This should be the reading of the meter the last time this task was completed.
This will be added to the interval in order to calculate the next time the task is due.

### Meter Interval

This is the frequency that the task will come due.

The way due meter tasks are calculated:

- When the meter's last reading is > 75% and < 90% of the next due value of a given task a notification will appear in the task list. This notification will simply be an acknowledgment that the task will be due soon. Very similar to the [notification before due](/maintenance-tasks/#notify-before-due) option for time based tasks.
- When the meter's last reading is > 90% of the next due value of a given task the task will become due.

### Meter Used

This is the meter used to calculate when this task will again become due.

The meter selected int he drop down will be tied to this task in order to calculate when the task will be shown in the [Due Tasks](/maintenance-tasks/#due-tasks) table.

If the required meter is not shown then a new meter might need to be created. To do so press the + next to the meter selection dropdown.

The Create New Meter popup will then be shown. In this dialog you must:

- Name the new meter
- Assign the type of meter (hours, miles, cycles, etc)
- Record the current reading
- Set an interval by which you would like to have this meter prompt for a new reading

What this will do is both create a meter and create a new task for recording the new meter reading at the interval set.

To change a meter out or remove one see the [settings page](/settings/#meters)

---

## Description

Describe the task and any information that would be required to do the task appropriately.

This box allows use of [Markdown](https://en.wikipedia.org/wiki/Markdown) which is a very easy way to do formatting of web based text.
[See here](/markdown/) for a decent cheat sheet that shows what can be done and how to do it.

---

### Deactivate Task

A task can be deactivated by pressing the deactivation button. This will leave the entire task exactly the way is it as this moment but set it to inactive so it no longer appear in the maintenance tasks page.

To reactive a previously deactivated task visit the [settings page](/settings/#deactivated-tasks)
