import 'material-design-icons-iconfont'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader version "^2.1.1" ,

export default {
  breakpoint: {},
  icons: { iconFont: 'mdi' },
  lang: {},
  rtl: false,
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#2957CD',
        accent: '#00D499',
        secondary: '#2277C2',
        success: '#147318',
        info: '#31D8FF',
        warning: '#FA9700',
        error: '#C21C1C',
      },
      light: {
        primary: '#49AAFF',
        accent: '#00D499',
        secondary: '#7FD5FF',
        success: '#06cf0c',
        info: '#33EAFF',
        warning: '#FFA900',
        error: '#FB6C57',
      },
    },
  },
}
