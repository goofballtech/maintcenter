const { format, createLogger, transports } = require('winston')
const { MongoDB } = require('winston-mongodb')
const { combine, timestamp, json, errors } = format
const mongoose = require('mongoose')
const path = require('path')

const dev = !process.env.NODE_ENV === 'production'

const logger = createLogger({
  exitOnError: false,
  level: 'info',
  format: combine(timestamp(), errors({ stack: true }), json()),
  transports: [
    new transports.File({
      tailable: true,
      filename: path.join(process.cwd(), 'logs', 'maint.log'),
      maxsize: 5000000,
      maxFiles: 10,
    }),
    new MongoDB({
      db: mongoose.connection,
      collection: 'sitelogs',
      cappedMax: 100000000,
    }),
  ],
})

if (dev) {
  logger.add(
    new transports.Console({
      format: format.combine(format.colorize(), format.simple()),
    })
  )
}

module.exports = logger
