// import { set } from '~/plugins/vuex-reuseable-functions' // pull exported function from other file

// possible future location for siteWide vars and mutations
export const state = () => ({})

export const mutations = {}

export const strict = process.env.NODE_ENV !== 'production' // throw errors for improper mutations only when in dev mode
