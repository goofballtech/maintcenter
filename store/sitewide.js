import { set } from '~/plugins/vuex-reuseable-functions' // pull exported function from other file
import { differenceInHours } from 'date-fns/esm'

// initial state
export const state = () => ({
  // Static vars
  siteVersion: '',
  siteRefreshInterval: 3 * 60000, // refresh time numberOfMinutes*60000 milliseconds
  hiddenMenu: '',

  // dynamic vars for interface
  mongoUrl: '',
  darkTheme: 'dark',
  hoseLengthUnit: '',
  pageDisplayed: '',
  currentTaskMode: '',
  previousTaskMode: '',
  taskModeType: '',
  taskModeShown: true,
  modeOptions: {},

  tableTitle: 'Due Tasks',
  tenantId: '',
  friendlyName: '',
  activeMenuOption: '',
  menuActive: false,
  menuOptions: {},
  activePage: '',

  defaultInspection: 90,
  maxInspection: 90,
  defaultChange: 1095,
  maxChange: 1095,
  maxHoseSizeMadeOnSite: '16',

  showMenuValues: true,
  menuValues: {},
  autoEmailOptions: [],

  techName: '',
  promptTechName: false,

  loadingScreen: false,

  userLoggedIn: false,
  loggedInUser: {},

  showTaskDelayOption: false,
  showFreqColumn: false,
  inventoryMode: false,
  orderMonths: 3,

  // number of chips to show in table before truncation
  chipsShownInTable: 4,

  // slugs for attachment name adding to completion object
  attachmentSlugs: [],

  // goofy things in the site
  iLikeTurtles: false,
  toggleTurtle: false,
  randomFact: '',
  lastFactGrabbed: '',
})

// getters
export const getters = {}

// mutations
export const mutations = {
  setSiteVersion: set('siteVersion'),
  setHiddenMenu: set('hiddenMenu'),

  // dynamic vars for interface
  setMongoUrl: set('mongoUrl'),
  setDarkTheme: set('darkTheme'),
  setHoseLengthUnit: set('hoseLengthUnit'),
  setPageDisplayed: set('pageDisplayed'),
  setCurrentTaskMode: set('currentTaskMode'),
  setPreviousTaskMode: set('previousTaskMode'),
  setTaskModeType: set('taskModeType'),
  setTaskModeShown: set('taskModeShown'),
  setModeOptions: set('modeOptions'),

  setTableTitle: set('tableTitle'),
  setTenantId: set('tenantId'),
  setFriendlyName: set('friendlyName'),
  setActiveMenuOption: set('activeMenuOption'),
  setMenuActive: set('menuActive'),
  setMenuOptions: set('menuOptions'),
  setActivePage: set('activePage'),

  setDefaultInspection: set('defaultInspection'),
  setMaxInspection: set('maxInspection'),
  setDefaultChange: set('defaultChange'),
  setMaxChange: set('maxChange'),
  setMaxHoseSizeMadeOnSite: set('maxHoseSizeMadeOnSite'),

  setShowMenuValues: set('showMenuValues'),
  setMenuValues: set('menuValues'),
  setAutoEmailOptions: set('autoEmailOptions'),

  // dynamic vars user accessed via form
  setTechName: set('techName'),
  setPromptTechName: set('promptTechName'),

  setLoadingScreen: set('loadingScreen'),

  setUserLoggedIn: set('userLoggedIn'),
  setLoggedInUser: set('loggedInUser'),

  setShowTaskDelayOption: set('showTaskDelayOption'),
  setShowFreqColumn: set('showFreqColumn'),
  setInventoryMode: set('inventoryMode'),
  setOrderMonths: set('orderMonths'),

  setAttachmentSlugs: set('attachmentSlugs'),

  setILikeTurtles: set('iLikeTurtles'),
  setToggleTurtle: set('toggleTurtle'),
  setRandomFact: set('randomFact'),
  setLastFactGrabbed: set('lastFactGrabbed'),
}

// actions
export const actions = {
  async getRandomFact({ commit, state }) {
    try {
      if (!state.lastFactGrabbed || differenceInHours(new Date(), state.lastFactGrabbed) >= 1) {
        let factResponse = await this.$axios.$get('https://uselessfacts.jsph.pl/random.json?language=en')
        commit('setRandomFact', factResponse.text)
        commit('setLastFactGrabbed', new Date())
      }
    } catch {
      // console.log("Can't get random fact")
    }
  },
  setApiUrl({ commit }, db) {
    commit('setMongoUrl', `${window.location.origin}/api/${db}/`)
    commit('setTenantId', db)
  },
  async getSiteSettings({ commit, state, dispatch }) {
    try {
      const taskModeData = await this.$axios.$get(`${state.mongoUrl}settings`)
      if (taskModeData.length) {
        commit('setFriendlyName', taskModeData[0].friendlyName)
        commit('setMenuOptions', taskModeData[0].menuOptions)
        commit('setShowMenuValues', taskModeData[0].showMenuValues)
        commit('setAutoEmailOptions', taskModeData[0].autoEmailOptions)
        commit('setCurrentTaskMode', taskModeData[0].activeStatus)
        commit('setTaskModeType', taskModeData[0].taskModeType)
        commit('setTaskModeShown', taskModeData[0].taskModeShown)
        commit('setModeOptions', taskModeData[0].modeOptions)
        commit('setPreviousTaskMode', taskModeData[0].previousStatus)
        commit('setAttachmentSlugs', taskModeData[0].attachmentSlugs)
        commit('setDarkTheme', taskModeData[0].siteTheme)
        commit('setHoseLengthUnit', taskModeData[0].hoseLengthUnit)
        commit('setHiddenMenu', taskModeData[0].hiddenMenu)
        commit('setShowTaskDelayOption', taskModeData[0].showTaskDelayOption)
        commit('setShowFreqColumn', taskModeData[0].showFreqColumn)
        commit('setInventoryMode', taskModeData[0].inventoryMode)
        commit('setOrderMonths', taskModeData[0].orderMonths)
        commit('settings/setNumberOfBackupsToKeep', parseInt(taskModeData[0].numBackupsToKeep), {
          root: true,
        })
        commit('settings/setDaysPerBackup', parseInt(taskModeData[0].daysPerBackup), {
          root: true,
        })
        
        commit('setDefaultInspection', taskModeData[0].defaultInspection)
        commit('setMaxInspection', taskModeData[0].maxInspection)
        commit('setDefaultChange', taskModeData[0].defaultChange)
        commit('setMaxChange', taskModeData[0].maxChange)
        commit('setMaxHoseSizeMadeOnSite', taskModeData[0].maxHoseSizeMadeOnSite)

        dispatch('sitewide/getMenuValues', undefined, { root: true })
        dispatch('settings/getBackups', undefined, { root: true })
      }
    } catch (error) {
      dispatch('logWrite', {
        logType: 'error',
        logItem: `sitewide.js, Actions-getSiteSettings() - ${error}`,
      })
    }
  },
  async getMenuValues({ state, dispatch, commit }) {
    if (state.showMenuValues) {
      try {
        let tempValues = {}
        let promises = [
          this.$axios.$get(`${state.mongoUrl}hoses?count=true`),
          this.$axios.$get(`${state.mongoUrl}taskdata/due?count=true&timezone=${new Date().getTimezoneOffset() / 60}`),
          this.$axios.$get(`${state.mongoUrl}todo?count=true`),
          this.$axios.$get(`${state.mongoUrl}toorder?count=true`),
        ]

        promises = await Promise.all(promises)
        tempValues.hoses = promises[0]
        tempValues.tasks = promises[1]
        tempValues.todo = promises[2]
        tempValues.toorder = promises[3]

        commit('setMenuValues', tempValues)
      } catch (error) {
        dispatch('logWrite', {
          logType: 'error',
          logItem: `sitewide.js, Actions-getMenuValues() - ${error}`,
        })
      }
    }
  },
  async changeMode({ state, dispatch }, newMode) {
    let updateObject = {}
    if (newMode.newMode.includes('custom')){
      updateObject = {
        taskModeType: 'custom',
        modeOptions: newMode || undefined,
        activeStatus: newMode.newMode,
        previousStatus: state.currentTaskMode,
      }
    } else {
      updateObject = {
        taskModeType: 'classic',
        activeStatus: newMode.newMode || 'Transit',
        previousStatus: state.currentTaskMode || 'Ops',
      }
    }
    try {
      let updateMode = await this.$axios.$put(`${state.mongoUrl}settings/`, updateObject)
      dispatch('updateModeOnly')
      dispatch('toastMessage', updateMode)
    } catch (error) {
      dispatch('logWrite', {
        logType: 'error',
        logItem: `sitewide.js, Actions-changeMode() - ${error}`,
      })
    }
  },
  async updateModeOnly({ state, dispatch, commit }) {
    try {
      let updateMode = await this.$axios.$get(`${state.mongoUrl}settings/`)
      // this updates the local variables to match the database for instant changes
      if (updateMode.length) {
        commit('setModeOptions', updateMode[0].modeOptions)
        commit('setTaskModeType', updateMode[0].taskModeType)
        commit('setCurrentTaskMode', updateMode[0].activeStatus)
        commit('setPreviousTaskMode', updateMode[0].previousStatus)
      }
    } catch (error) {
      dispatch('logWrite', {
        logType: 'error',
        logItem: `sitewide.js, Actions-updateMode() - ${error}`,
      })
    }
  },
  menuSelection({ commit }, selection) {
    commit('setActiveMenuOption', selection)
    commit('setMenuActive', false)
  },
  async logWrite({ state }, log) {
    if (typeof log == 'string') {
      let temp = {}
      temp.logType = 'info'
      temp.logItem = log
      log = temp
    }
    if (log.logType == 'error') {
      // eslint-disable-next-line no-console
      console.error(`${log.logType} - ${log.logItem}`)
    }
    try {
      await this.$axios.$post(`${state.mongoUrl}log`, {
        level: log.logType,
        body: log.logItem,
      })
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(`There was an issue writing to the log: ${error}`)
    }
  },
  toastMessage({ dispatch }, message) {
    dispatch('logWrite', {
      logType: 'info',
      logItem: message,
    })
    this.$toast.show(message)
  },
}
