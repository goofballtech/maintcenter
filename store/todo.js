// import { set, pushTo } from '~/plugins/vuex-reuseable-functions' // pull exported function from other file

// initial state
export const state = () => ({
  // Static vars
  priorityOptions: [
    {
      text: 'Low',
      value: 1,
    },
    {
      text: 'Medium',
      value: 2,
    },
    {
      text: 'High',
      value: 3,
    },
  ],
})

// getters
export const getters = {}

// mutations
export const mutations = {}

// actions
export const actions = {}
