import { set } from '~/plugins/vuex-reuseable-functions' // pull exported function from other file

// initial state
export const state = () => ({
  // arrays grabbed from database
  safetyCategoryOptions: [],
})

// getters
export const getters = {}

// mutations
export const mutations = {
  setSafetyCategoryOptions: set('safetyCategoryOptions'),
}

// actions
export const actions = {
  async getSafetyCategoryOptions({ commit, rootState, dispatch }) {
    try {
      let getObs = await this.$axios.$get(rootState.sitewide.ajaxUrl, {
        params: {
          condition: 'sort',
          tableName: 'obsCategories',
        },
      })
      commit('setSafetyCategoryOptions', getObs)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `safety.js, Actions-getSafetyCategoryOptions()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
}
