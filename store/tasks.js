import { set } from '~/plugins/vuex-reuseable-functions' // pull exported function from other file
import { addDays, parseISO, isDate, format } from 'date-fns/esm'

// initial state
export const state = () => ({
  // Array's [pulled from tables]
  activeTasks: [],
  previousCompletions: [],
  categoryOptions: [],

  emptyDatabase: false,

  meters: [],
  meterPercentageDue: 90, // when the hour meter is more than 90 percent of the due hour then mark it due
  meterIntervals: [],
  meterList: [],
  meterTypes: [],
  frequencyOptions: [],
  transitionInterval: [],
  attachmentList: [],

  loadingAttachments: false,

  detailsPopupData: [],

  newTaskPopup: false,
  detailsPopup: false,
  editTaskPopup: false,
  newMeterPopup: false,
  notesPopup: false,
  attachmentPopup: false,
  printPopup: false,
  showRequired: false,

  // dynamic vars controlled via forms
  numDays: 1, // default days ahead for due query
  iconNum: 3, // number of category icons to display in task table before truncating
  tasksLoading: false,
  busySubmitting: false,
  loadingScreenText: '',
  tableSearch: '',
})

// mutations
export const mutations = {
  setActiveTasks: set('activeTasks'),
  setPreviousCompletions: set('previousCompletions'),
  setCategoryOptions: set('categoryOptions'),

  setEmptyDatabase: set('emptyDatabase'),

  setMeters: set('meters'),
  setMeterIntervals: set('meterIntervals'),
  setMeterList: set('meterList'),
  setMeterTypes: set('meterTypes'),
  setFrequencyOptions: set('frequencyOptions'),
  setTransitionInterval: set('transitionInterval'),
  setAttachmentList: set('attachmentList'),

  setLoadingAttachments: set('loadingAttachments'),
  setDetailsPopupData: set('detailsPopupData'),

  setNewTaskPopup: set('newTaskPopup'),
  setDetailsPopup: set('detailsPopup'),
  setEditTaskPopup: set('editTaskPopup'),
  setNewMeterPopup: set('newMeterPopup'),
  setNotesPopup: set('notesPopup'),
  setAttachmentPopup: set('attachmentPopup'),
  setPrintPopup: set('printPopup'),
  setShowRequired: set('showRequired'),

  addNumDays(state) {
    switch (true) {
      case state.numDays <7:
        state.numDays++
        break
      default:
        state.numDays += 7
        break
    }
  },
  
  lowerNumDays(state) {
    switch (true) {
      case state.numDays <= 7:
        state.numDays > 1 ? state.numDays-- : state.numDays = 1
        break
      default:
        state.numDays -= 7
        break
    }
  },

  setIconNum: set('iconNum'),

  setTasksLoading: set('tasksLoading'),
  setBusySubmitting: set('busySubmitting'),
  setLoadingScreenText: set('loadingScreenText'),
  setTableSearch: set('tableSearch'),
}

// getters
export const getters = {}

// actions
export const actions = {
  async saveTask({ commit, rootState, dispatch }, newTaskData) {
    if (newTaskData.meterRequired === true && !newTaskData.category.includes('Meter')) {
      newTaskData.nextMeterReadingDue =
        parseInt(newTaskData.lastMeterReadingCompleted) + parseInt(newTaskData.meterFrequency)
    } else {
      newTaskData.meterFrequency = 0
      newTaskData.nextMeterReadingDue = 0
    }

    if (rootState.sitewide.currentTaskMode == 'Operations' || newTaskData.category.includes('Meter')) {
      if (!isDate(parseISO(newTaskData.lastCompletion))) {
        newTaskData.nextDue = '1970-01-01'
      } else {
        newTaskData.nextDue = addDays(parseISO(newTaskData.lastCompletion), newTaskData.opsFrequency)
      }
    } else {
      if (!isDate(parseISO(newTaskData.lastCompletion))) {
        newTaskData.nextDue = '1970-01-01'
      } else {
        newTaskData.nextDue = addDays(parseISO(newTaskData.lastCompletion), newTaskData.transitFrequency)
      }
    }

    if (newTaskData.category.includes('Meter')) {
      newTaskData.transitFrequency = newTaskData.opsFrequency // copy them over so they stay in sync if changed by user
    }

    newTaskData.techName = rootState.sitewide.techName || 'auto'
    try {
      const saveTaskRes = await this.$axios.$post(`${rootState.sitewide.mongoUrl}taskdata/add`, newTaskData)
      dispatch('sitewide/toastMessage', saveTaskRes.reply, {
        root: true,
      })
      if (newTaskData.newTaskCompletion) {
        let addComp = await this.$axios.$post(
          `${rootState.sitewide.mongoUrl}taskdata/completions/${saveTaskRes.newTask.taskID}`,
          {
            taskID: saveTaskRes.newTask.taskID,
            customTaskID: saveTaskRes.customTaskID ? saveTaskRes.customTaskID : undefined,
            task_id: saveTaskRes.newTask._id,
            taskName: saveTaskRes.newTask.taskName,
            techName: rootState.sitewide.techName,
            dateCompleted: saveTaskRes.newTask.lastCompletion,
            hourCompleted: saveTaskRes.newTask.lastMeterReadingCompleted
              ? saveTaskRes.newTask.lastMeterReadingCompleted
              : undefined,
          }
        )
        dispatch('sitewide/toastMessage', addComp, {
          root: true,
        })
      }
      if (newTaskData.category.includes('Meter')) {
        commit('setNewMeterPopup', false)
      } else {
        commit('setNewTaskPopup', false)
      }
      commit('setDetailsPopup', false)
      dispatch('getActiveTasks')
      dispatch('getCategoryOptions')
      dispatch('sitewide/getMenuValues', undefined, { root: true })
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, saveTask() - ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async editTaskSave({ commit, rootState, dispatch }, editTaskData) {
    if (editTaskData.meterRequired === true && !editTaskData.category.includes('Meter')) {
      editTaskData.nextMeterReadingDue =
        parseInt(editTaskData.lastMeterReadingCompleted) + parseInt(editTaskData.meterFrequency)
    } else {
      editTaskData.meterFrequency = 0
      editTaskData.nextMeterReadingDue = 0
    }

    if (editTaskData.relatedTasks.includes(editTaskData.taskID)) {
      editTaskData.relatedTasks.splice(editTaskData.relatedTasks.indexOf(editTaskData.taskID), 1)
    }

    if (rootState.sitewide.currentTaskMode == 'Operations' || 
        editTaskData.category.includes('Meter') ||
        (typeof rootState.sitewide.currentTaskMode == 'string' && 
        rootState.sitewide.currentTaskMode.includes('custom'))) {
      if (!isDate(parseISO(editTaskData.lastCompletion))) {
        editTaskData.nextDue = '1970-01-01'
      } else if (!editTaskData.active) {
        editTaskData.nextDue = '1970-01-01'
        editTaskData.nextMeterReadingDue = 0
      } else {
        editTaskData.nextDue = addDays(parseISO(editTaskData.lastCompletion), editTaskData.opsFrequency)
      }
    } else {
      if (!isDate(parseISO(editTaskData.lastCompletion))) {
        editTaskData.nextDue = '1970-01-01'
      } else if (!editTaskData.active) {
        editTaskData.nextDue = '1970-01-01'
        editTaskData.nextMeterReadingDue = 0
      } else {
        editTaskData.nextDue = addDays(parseISO(editTaskData.lastCompletion), editTaskData.transitFrequency)
      }
    }

    if (editTaskData.category.includes('Meter')) {
      editTaskData.transitFrequency = editTaskData.opsFrequency // copy them over so they stay in sync if changed by user
    }

    editTaskData.techName = rootState.sitewide.techName || 'auto'
    try {
      const editTaskRes = await this.$axios.$put(
        `${rootState.sitewide.mongoUrl}taskdata/${editTaskData._id}`,
        editTaskData
      )
      dispatch('sitewide/toastMessage', editTaskRes, {
        root: true,
      })
      commit('setDetailsPopup', false)
      if (editTaskData.category.includes('Meter')) {
        commit('setNewMeterPopup', false)
      } else {
        commit('setNewTaskPopup', false)
      }
      dispatch('getActiveTasks')
      dispatch('getCategoryOptions')
      dispatch('sitewide/getMenuValues', undefined, { root: true })
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, editTask() - ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getActiveTasks({ commit, rootState, dispatch }, loadingMessage = 'Working on making the table..') {
    commit('setLoadingScreenText', loadingMessage)
    commit('sitewide/setLoadingScreen', true, { root: true })
    commit('setActiveTasks', [])
    try {
      const activeTasks = await this.$axios.$get(`${rootState.sitewide.mongoUrl}taskdata`)
      if (!activeTasks.length) {
        commit('setEmptyDatabase', true)
      } else {
        commit('setEmptyDatabase', false)
      }
      commit('setActiveTasks', activeTasks)
      commit('sitewide/setLoadingScreen', false, { root: true })
    } catch (err) {
      commit('sitewide/setLoadingScreen', false, { root: true })
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, getActiveTasks() - ${err}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getAttachments({ commit, rootState, dispatch }, taskID) {
    try {
      commit('setLoadingAttachments', true)
      const attachments = await this.$axios.$get(`${rootState.sitewide.mongoUrl}files/tasks/${taskID}`)
      commit('setAttachmentList', attachments)
      commit('setLoadingAttachments', false)
    } catch (error) {
      commit('setLoadingAttachments', false)
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `task.js, getAttachments() - ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getPreviousTaskCompletions({ commit, rootState, dispatch }) {
    try {
      const prevCompletions = await this.$axios.$get(
        `${rootState.sitewide.mongoUrl}taskdata/completions?prevTable=true`
      )
      commit('setPreviousCompletions', prevCompletions)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, previousCompletions()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getFreqOptions({ commit, rootState, dispatch }) {
    try {
      const freqOptions = await this.$axios.$get(`${rootState.sitewide.mongoUrl}settings/frequencyOptions`)
      commit('setFrequencyOptions', freqOptions)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, Actions-getFreqOptions()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getTransitionIntervals({ commit, rootState, dispatch }) {
    try {
      const transIntervals = await this.$axios.$get(`${rootState.sitewide.mongoUrl}settings/transitionIntervals`)
      commit('setTransitionInterval', transIntervals)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, Actions-getTransitionIntervals()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getMeterIntervals({ commit, rootState, dispatch }) {
    try {
      const intervals = await this.$axios.$get(`${rootState.sitewide.mongoUrl}settings/meterIntervals`)
      commit('setMeterIntervals', intervals)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, Actions-getMeterIntervals()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getCategoryOptions({ commit, rootState, dispatch }) {
    try {
      const catOptions = await this.$axios.$get(`${rootState.sitewide.mongoUrl}taskdata/categories`)
      commit('setCategoryOptions', catOptions)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, getCategoryOptions()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getMeters({ commit, rootState, dispatch }) {
    try {
      const meterObject = await this.$axios.$get(`${rootState.sitewide.mongoUrl}meters`)
      commit('setMeters', meterObject)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, getMeters()- meterObject- ${error}`,
        },
        {
          root: true,
        }
      )
    }
    try {
      const meterList = await this.$axios.$get(`${rootState.sitewide.mongoUrl}meters/list`)
      commit('setMeterList', meterList)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, getMeters()- meterList- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async getMeterTypes({ commit, rootState, dispatch }) {
    try {
      let meterTypeRes = await this.$axios.$get(`${rootState.sitewide.mongoUrl}meters/types`)
      if (!meterTypeRes.length) {
        meterTypeRes = ['Cycles', 'Hours', 'Miles']
      } else {
        commit('setMeterTypes', meterTypeRes)
      }
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, Actions-getMeterTypes() - ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async makeNewMeter({ commit, rootState, dispatch }, newMeter) {
    try {
      commit('setNewMeterPopup', false)
      const newMeterResponse = await this.$axios.$post(`${rootState.sitewide.mongoUrl}meters`, {
        techName: rootState.sitewide.techName,
        name: newMeter.name,
        type: newMeter.type,
        currentReading: newMeter.currentReading,
      })
      dispatch('sitewide/toastMessage', newMeterResponse, {
        root: true,
      })
      const newMeterTaskData = {
        taskName: 'Record meter reading for ' + newMeter.name,
        category: 'Meter',
        detailedDescription: 'Record updated reading for ' + newMeter.name,

        meterRequired: true,
        lastMeterReadingCompleted: newMeter.currentReading,
        nextMeterReadingDue: newMeter.currentReading,
        meterName: newMeter.name,

        lastCompletion: format(new Date(), 'yyyy-MM-dd'),
        opsFrequency: newMeter.readFreq,
        transitFrequency: newMeter.readFreq,
      }
      dispatch('saveTask', newMeterTaskData)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js, Actions-makeNewMeter() - ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  OpsStatusChange({ commit, rootState, dispatch }, newMode) {
    commit('setLoadingScreenText', `Working to update all the tasks to reflect the ${newMode} status`)
    commit('sitewide/setMenuActive', false, {
      root: true,
    })
    commit('sitewide/setLoadingScreen', true, {
      root: true,
    })
    if (newMode == 'Operations') {
      if (rootState.sitewide.currentTaskMode == 'Transit') {
        dispatch('sendUpdateRequest', 'Operations')
      } else {
        dispatch(
          'sitewide/logWrite',
          {
            logType: 'info',
            logItem: 'Attempted to go to ops from layup/demobe',
          },
          {
            root: true,
          }
        )
      }
    } else if (newMode == 'In Layup') {
      if (rootState.sitewide.currentTaskMode == 'Operations' || rootState.sitewide.currentTaskMode == 'Transit') {
        dispatch('sendUpdateRequest', 'In Layup')
      } else {
        dispatch(
          'sitewide/logWrite',
          {
            logType: 'info',
            logItem: 'Attempted to go to layup direct from mobe.',
          },
          {
            root: true,
          }
        )
      }
    } else {
      const possibleModes = ['Operations', 'Transit', 'In Layup', 'Out Of Layup', 'Mobilization', 'De-Mobilization']
      if (possibleModes.includes(newMode) || newMode.includes('custom.')) {
        dispatch('sendUpdateRequest', newMode)
      } else {
        dispatch(
          'sitewide/logWrite',
          {
            logType: 'info',
            logItem: `Something went wrong trying to go to ${newMode} mode`,
          },
          {
            root: true,
          }
        )
      }
    }
  },
  async sendUpdateRequest({ commit, rootState, dispatch }, newMode) {
    const mode = {
      prev: rootState.sitewide.currentTaskMode,
      new: newMode,
    }
    try {
      const updateTaskDates = await this.$axios.$put(
        `${rootState.sitewide.mongoUrl}taskdata/mode?updatedMode=${mode.new}&lastMode=${mode.prev ||
          'none'}&timezone=${new Date().getTimezoneOffset() / 60}`
      )
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'info',
          logItem: `updating task dates to ${mode.new} returned a message of: ${updateTaskDates}`,
        },
        {
          root: true,
        }
      )
      dispatch('sitewide/toastMessage', `Updated task mode to ${mode.new}`, {
        root: true,
      })
      dispatch('getActiveTasks', 'The database is updated, working on making the new table....')
      let newModeObject = rootState.sitewide.modeOptions
      newModeObject.newMode = mode.new
      dispatch('sitewide/changeMode', newModeObject, {
        root: true,
      })
    } catch (error) {
      commit('sitewide/setLoadingScreen', false, {
        root: true,
      })
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `tasks.js.vue, sendUpdateRequest() -  ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
}
