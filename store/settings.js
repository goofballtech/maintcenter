import { set, pushTo } from '~/plugins/vuex-reuseable-functions'
import { isAfter, parseISO, addDays } from 'date-fns/esm'

// initial state
export const state = () => ({
  // dynamic vars user accessed via form
  numberOfBackupsToKeep: '',
  daysPerBackup: '',

  activeMeter: '',
  taskList: [], //  taskList made for meter use display
  taskListPopup: false,

  // arrays grabbed from database
  existingBackups: [],
  newestBackup: [],
  backupCheckActive: false,
  obsCardExport: [],
  offSiteBackup: [],
  toOrderList: [],
  meters: [],
  meterTasks: [],
})

// getters
export const getters = {}

// mutations
export const mutations = {
  setActiveMeter: set('activeMeter'),
  setTaskListPopup: set('taskListPopup'),

  setExistingBackups: set('existingBackups'),
  setBackupCheckActive: set('backupCheckActive'),
  setObsCardExport: set('obsCardExport'),
  setOffSiteBackup: set('offSiteBackup'),
  setToOrderList: set('toOrderList'),
  setMeters: set('meters'),
  setMeterTasks: set('meterTasks'),
  setTaskList: set('taskList'),

  pushToTaskList: pushTo('taskList'),

  setNumberOfBackupsToKeep: set('numberOfBackupsToKeep'),
  setDaysPerBackup: set('daysPerBackup'),

  incBackups(state) {
    if (state.numberOfBackupsToKeep < 30) {
      state.numberOfBackupsToKeep++
    } else {
      state.numberOfBackupsToKeep = 30
    }
  },
  decBackups(state) {
    if (state.numberOfBackupsToKeep > 2) {
      state.numberOfBackupsToKeep--
    } else {
      state.numberOfBackupsToKeep = 2
    }
  },
  incBackupDays(state) {
    if (state.daysPerBackup < 30) {
      state.daysPerBackup++
    } else {
      state.daysPerBackup = 30
    }
  },
  decBackupDays(state) {
    if (state.daysPerBackup > 2) {
      state.daysPerBackup--
    } else {
      state.daysPerBackup = 2
    }
  },
}

// actions
export const actions = {
  async getBackups({ commit, dispatch, rootState }, created) {
    try {
      if (!rootState.settings.backupCheckActive) {
        commit('setBackupCheckActive', true)
        const backupDates = await this.$axios.$get(`${rootState.sitewide.mongoUrl}backup/`)
        if (!backupDates.length && !created) {
          dispatch('sitewide/toastMessage', 'No backups found, creating one', {
            root: true,
          })
          dispatch('createBackup')
        } else if (
          isAfter(
            new Date(),
            addDays(parseISO(backupDates[backupDates.length - 1].date), rootState.settings.daysPerBackup)
          ) &&
          !created
        ) {
          dispatch('sitewide/toastMessage', 'Most recent backup is too old, making a new one', {
            root: true,
          })
          dispatch('createBackup')
        }
        commit('setExistingBackups', backupDates)
        commit('setBackupCheckActive', false)
      }
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `Setting.js, Actions-getBackups()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
  async createBackup({ dispatch, rootState }) {
    try {
      const newBackup = await this.$axios.$post(
        `${rootState.sitewide.mongoUrl}backup?maxBackups=${rootState.settings.numberOfBackupsToKeep}&techName=${
          rootState.sitewide.techName
        }`
      )
      dispatch('sitewide/toastMessage', newBackup, {
        root: true,
      })
      dispatch('getBackups', false)
    } catch (error) {
      dispatch(
        'sitewide/logWrite',
        {
          logType: 'error',
          logItem: `Setting.js, Actions-createBackup()- ${error}`,
        },
        {
          root: true,
        }
      )
    }
  },
}
