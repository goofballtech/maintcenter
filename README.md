# Maintenance Center

> Maintenance app for use with deployed hardware

## Links

### Issues Page

[https://gitlab.com/goofballtech/maintcenter/-/issues](https://gitlab.com/goofballtech/maintcenter/-/issues)

### Change Log

[https://gitlab.com/goofballtech/maintcenter/-/blob/master/CHANGELOG.md](https://gitlab.com/goofballtech/maintcenter/-/blob/master/CHANGELOG.md?ref_type=heads)

### Download Upgraded Images

[https://gitlab.com/goofballtech/maintcenter/-/pipelines](https://gitlab.com/goofballtech/maintcenter/-/pipelines)
