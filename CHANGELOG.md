# Changelog

The below are changes made to the program with each numbered release.
The numbers on the right side of some of the lines relate to the [issue number](https://gitlab.com/goofballtech/maintcenter/-/issues).
If you want to see the context related to a change you may do so at the issues page by finding that number in the [closed section](https://gitlab.com/goofballtech/maintcenter/-/issues/?sort=created_date&state=closed&first_page_size=25) of issues.

### Version 3.7.24 - 2025-01-02

- REQUEST - Removed 75 char limit from package notes closes #624
- IMPROVEMENT - make a local export based on export settings when backing up related #632
- IMPROVEMENT - Allow task table search to persist through page refresh closes #628
- IMPROVEMENT - check for email and skip email if not available related #627
- IMPROVEMENT - Added ctrl-S hotkey for save on most dialogs closes #631
- IMPROVEMENT - Make dialog boxes not close/clear on external click closes #635
- IMPROVEMENT - make default sort of task table alphabetical closes #630
- IMPROVEMENT - add hour totals to table and printout closes #644
- IMPROVEMENT - Name export properly depedning on contents closes #640
- TWEAK - allow admin to change tech name for task submittals closes #637
- BUG - limit freq option field to numerical only closes #620
- BUG - Upload edit window squished with not a jpeg closes #634
- BUG - Single completion doesnt generate completion list for task closes #629
- BUG - pre setup date buttons in completions export not working closes #638
- BUG - Better handling of special chars in uploads closes #645
- BUG - Competion notes not showing custom ID closes #639

### Version 3.7.23 - 2024-11-22

- REQUEST - default to all on task table
- REQUEST - add notes option to export closes #607
- IMPROVEMENT - ability to clear task forms, formatting tweak, bug fix for numbers
- IMPROVEMENT - clone auto exports to a local folder for pulling via script
- IMPROVEMENT - change length values on hose order pages when meters selected
- IMPROVEMENT - Remove the highligh column from logs closes #612
- IMPROVEMENT - add ability to record hose lengths in meters
- IMPROVEMENT - remove order shotcut from task if order page is hidden closes #608
- DOCUMENTATION - updates on hose and task page for recent mods
- TWEAK - make sure folder exists for local excel exports
- BUG - Hose ordert able not populating closes #602
- BUG - Type for QOH for OI Part import

### Version 3.7.22 - 2024-11-12

- BUG - Bug in notes entry on tasks closes #618

### Version 3.7.21 - 2024-11-09

- IMPROVEMENT - Added ExternalUpdate folder to archive for backups
- IMPROVEMENT - ability to backup entire server rather than individual hardware sets

### Version 3.7.20 - 2024-11-08

- IMPROVEMENT - Update parts with peoplesoft export
- IMPROVEMENT - Allow searchability of custom task ID closes #617
- IMPROVEMENT - Allow task import to use Custom ID during import
- BUG - Found issue with CustomItemID closes #616

### Version 3.7.19 - 2024-09-23

- REQUEST - added ability to modify package number closes #613

### Version 3.7.18 - 2024-08-01

- REQUEST - Ability to assign custom task ID's closes #597
- BUG - modify calender object for zulu offset handling closes #606
- BUG - time zone offset for package entries related #606
- BUG - Additional task table tweaks
- BUG - empty completions throwing error
- BUG - prevent auto switching modes when not ready
- BUG - show frequency column for tasks in custom modes

### Version 3.7.17-1 - 2024-06-28

- REQUEST - added custom task mode options #604
- TWEAK - add days to jump by weeks after the first 7 days
- HOTFIX - bug in menu dropdown on custom modes

### Version 3.7.16 - 2024-04-26

- REQUEST - add highlighted attachment to task details related #596
- REQUEST - To Do export capability related #572
- UPDATE - Oi Part Numbers data

### Version 3.7.15 - 2024-03-21

- ADDED - Add packages page closes #592 #594
- REQUEST - added notes to package export
- REQUEST - Package Export Capability related #592 - notes not included

### Version 3.7.14 - 2024-03-10

- REQUEST - Add CWP items in the logs page

### Version 3.7.13 - 2022-08-29

- ADDED - ability to export PR and Encompass excel sheet from orders related #285
- REQUEST - added detail/date to inventory items closes #512
- IMPROVEMENT - Change item pending to be per item instead of per order related #285
- IMPROVEMENT - move order quantity from parent order to line items for better flexibility closes #567
- IMPROVEMENT - make order searches include item part numbers closes #566
- IMPROVEMENT - update OI parts and vendor cross numbers closes #563
- IMPROVEMENT - look up existing orders and parts when new 'to order' is entered closes #516 related #504
- IMPROVEMENT - change sort on logs so most recent is at the top closes #559
- IMPROVEMENT - add checks to order page to see if item already exists in inventory closes #553
- TWEAK - remove column that no longer gives results from peoplesoft in item details
- BUG - step modified category tasks from removing themselves from due list closes #565
- BUG - missed a spot of code in forced due fix related #564
- BUG - Force due date clearing bug closes #564
- BUG - typo in file name for part number updates
- BUG - log entry edit's not saving closes #558
- BUG - adapt to gmail changing rules for email proxy closes #551
- BUG - pull dev from hardware selection in production mode closes #554
- BUG - move inventory image popup to top on order page closes #556

### Version 3.7.12 - 2022-07-18

- IMPROVEMENT - rework format of pending order items email closes #540
- IMPROVEMENT - add 1 second delay to using item inventory closes #550
- IMPROVEMENT - add checked by name to inventory quantities closes #552
- DOCS - added inventory mods to documentation closes #532
- BUG - offset item dates to UTC for database storage
- BUG - Task imports file error closes #535
- BUG - nudge inventory image up to prevent drop off screen closes #543
- BUG - allow deletion of system log attachments closes #542
- BUG - Added width to new log entry closes #541
- BUG - date entry not clearing within component closes #536
- BUG - asset location required and move updates properly closes #537
- BUG - added part number check to inventory details page closes #548

### Version 3.7.11 - 2022-02-08

- BUG - auto functions weren't being called correctly closes #522
- BUG - Inventory item attachment list not properly loading related #531

### Version 3.7.10 - 2022-02-06

- REQUEST - CustID and PO added to inventory items closes #526
- IMPROVEMENT - allow enter button to create sub-locations closes #530
- BUG - tech name not logged on new item creation closes #529

### Version 3.7.9 - 2022-02-02

- REQUEST - add location to inventory export closes #525
- BUG - add some resilience to location export in case of legacy data closes #524
- BUG - hose inspection button spacing on mid-size screen caused overlap
- BUG - hose completions add showing unknown hose name closes #518
- BUG - adding new drawing to item didn't update table immediately closes #523

### Version 3.7.8 - 2022-02-01

- REQUEST - Inventory Export closes #517
- REQUEST - allow enter key to save inventory qty changes closes #519
- BUG - prevent min/max number from inverting on inventory items closes #521

### Version 3.7.7 - 2022-01-27

- IMPROVEMENT - make vendor name check case insensitive closes #514
- BUG - Vendor list not populating properly closes #515
- HOTFIX - breaking error on inventory items during push

### Version 3.7.6 - 2021-12-07

- REQUEST - add compliance button to inventory item related #512
- REQUEST - missed compliance box on new item form related #512
- REQUEST - add drawing/BOM references to part related to #512
- TWEAK - disallow moving of root inventory location to avoid issues
- BUG - backups not grabbing inventory table closes #513
- HOTFIX - type on hose page prevent some edits

### Version 3.7.5 - 2021-10-30

- IMPROVEMENT - Allow hose number tag changes closes #485
- IMPROVEMENT - add hose tags to excel export for labeling or receiving purposes closes #484
- IMPROVEMENT - add hose size and length to table display
- BUG - add pressure rating to large hoses closes #417

### Version 3.7.4 - 2021-10-27

- REQUEST - allow order searches by vendor and order number closes #496
- IMPROVEMENT - Set a 2 year expiry on software log entries closes #498
- DOCS - add information for vendor references to documentation closes #509
- BUG - error when part number present as vendor but not in OI database related #509
- BUG - issue when checking for conflicting item numbers related #509

### Version 3.7.3 - 2021-10-26

- IMPROVEMENT - added vendor and manufacturer part number references related #508
- DOCS - update docs for mods in item creation related #507
- DOCS - update that inventory description will auto fill on OI selection
- TWEAK - Sort lists in inventory location tree
- TWEAK - add a blank item to top of vendor list for easy clearing
- BUG - prevent location from moving into a child of itself closes #507
- BUG - clear duplicate number check on change related #507
- BUG - don't check duplicates against inactive items related #507
- BUG - item reactivation bug related #507
- BUG - clear OI part number in new order page when unused closes #503

### Version 3.7.2 - 2021-10-21

- REQUEST - Prevent creation of existing part numbers during item creation
- IMPROVEMENT - Force Oceaneering to first on vendor list for ease of access
- TWEAK - widen qty boxes and add hint to description on inventory

### Version 3.7.1 - 2021-10-16

- BUG - reactivation location tweak
- BUG - Oi part number search erratic behavior closes #505
- HOTFIX - New Item qty not updating properly related #505
- HOTFIX - broken location add

### Version 3.7.0 - 2021-10-15

- ADDED - Inventory Page closes #89
- ADDED - Ability to put attachments on hoses #486
- IMPROVEMENT - Allow hose search by ID closes #492
- IMPROVEMENT - Change some popup spacing to avoid overlap
- IMPROVEMENT - hide hose settings when page is hidden closes #473
- IMPROVEMENT - adjust modified task date block to stand out #427
- BUG - offset hose dates for timezone closes #490
- BUG - fix error thrown on log when closing out without attachments
- BUG - fix validation errors on edit forms closes #494
- BUG - clear hose id on close to prevent dupes closes #491
- BUG - fix inconsistency in hose time intervals closes #495
- BUG - add some basic formatting to avoid overlap happening on some pages closes #499
- BUG - add error handling to asset route closes #501
- HOTFIX - documentation generation error fix

### Version 3.6.0 - 2021-06-15

- REQUEST - add user friendly ID number to To Do's closes #482
- REQUEST - make printable hose list closes #480
- IMPROVEMENT - added 3 and 4 years to default intervals closes #483
- DOCUMENTATION - add part number search reference to help doc closes #489
- BUG - added ability to grab sitelogs database entries during backups
- BUG - fix calender display for task forced due date closes #474
- BUG - page redirect when login timout closes #475
- BUG - zip/excel only creates temp file to self delete at later time closes #477
- HOTFIX - Comment out erroneous code deleting attachments for later review related #477

### Version 3.5.1 - 2020-11-29

- ADDED - ability to create issue/report/request via email by sending email to `maintcenter+goofballtech-maintcenter-2-issue-@gmail.com`
- BUG - add loading screen to attachment list for longer loading delays
- BUG - fig log download error without tenant info closes #463
- BUG - Fix bug not allowing user to change group name on forms related #476
- BUG - tweaks to form stepper to close #476
- HOTFIX - make all assets show on each of the pages instead of limiting results

### Version 3.5.0 - 2020-09-25

- ADDED - Basic version of assets page related #10
- IMPROVEMENT - add other business unit data and improve detail display for OI Part search
- TWEAK - limit update script to maint docker only
- BUG - Improper creation of email object from existing admin users
- BUG - add size limit so attachment images don't get clipped off top and bottom when viewing
- BUG - timezones not showing layup tasks depending on zulu date vs local time

### Version 3.4.15 - 2020-09-14

- REQUEST - add ability for form completion and task list to be zipped as a set closes #442
- IMPROVEMENT - Split related tasks popup into two categories to make it more clear closes #412
- IMPROVEMENT - spawn load balancing instances that allows a server failure to recover without a break in functionality
- IMPROVEMENT - increase task submission refresh timer and show counter-button closes #450
- IMPROVEMENT - add vendor column to order table so they are searchable closes #451
- IMPROVEMENT - add ability to change hose change/intervals in batch when multiples need to be modded. closes #421
- IMPROVEMENT - added notes along side delays for context on tasks when later reviewed closes #455
- IMPROVEMENT - add ability to export previous completion form data along side list #442
- IMPROVEMENT - added OI Part quick search button to order page #461
- IMPROVEMENT - OI part search improvements #461
- IMPROVEMENT - show data for OI number when moused over within an order #461
- BUG - Show loading on table and button when oi part search is active related #459
- BUG - prevent loss of logged in user details after page refresh closes #452
- BUG - form availability after creation closes #432
- BUG - clear file upload list when moving away from the upload display closes #441
- BUG - modified submission date not attached to form submissions properly closes #437
- BUG - allow fallback reading for number of days on hose interval if clear text option not found in list
- BUG - make task clone behavior more consistent closes #456
- BUG - if no slugs are set then no attachment names should be added to completion object closes #458
- BUG - New order category box not cleaning on second order item created closes #460
- BUG - file path fix for changelog with update emails
- BUG - Tooltips on switch not working closes #462
- BUG - OI part box doesn't clear on close #461

### Version 3.4.14 - 2020-08-25

- REQUEST - Add categories to print list closes #428
- IMPROVEMENT - add attachments to To Do related #156
- IMPROVEMENT - Add ability to attach files to To Order items closes #156
- TWEAK - dark theme documentation
- TWEAK - allow updated file upload path when externally triggering upload
- BUG - To Do group not inserting on form blur properly
- BUG - Debug logs entry table not pulling more than a few days of entries closes #449
- HOTFIX - Point at wrong path for OI part lookup images

### Version 3.4.13 - 2020-08-23

- REQUEST - ability to show frequency tag directly in task table closes #446
- ADDED - ability to set up notification emails when the server updates to a newer version closes #414
- ADDED - ability to search for OI part numbers (see To Order help page for complimentary image location placement) closes #443
- IMPROVEMENT - add sending info and loader graphic when sending test email #430
- IMPROVEMENT - theme site based on preference setting in windows/mac app theme when available
- TWEAK - make date input component for consistancy in presentation
- TWEAK - allow user to see existing meters when creating new one for guidance on naming closes #436
- TWEAK - added ability to see the random loading fact via icon at top right of page closes #439
- BUG - Added ability to remove dates in orders and to dos closes #419
- BUG - clear items related to orders when moving between detail pages closes #424
- BUG - Fix inconsistancy of changing fitting numbers with hose edit closes #416
- BUG - Some errors show up as [object Object] in Logs page closes #429
- BUG - date parsing on email and single email address inserting for test closes #431
- BUG - modify data entry form not properly populating existing form closes #434
- BUG - allow use of task id from deactivated task on new task clone closes #433
- BUG - remove search term from input component when existing item selected
- BUG - Adjust mode transition logic to better avoid adding unrequired tasks to list #384
- BUG - Tech Name prompt not working on new log entry closes #438
- BUG - Related tasks list not properly compiling
- BUG - prevent combobox from entering search term as list entry on form blur
- BUG - fix numbering bug and tweak display on logs page closes #447
- BUG - remove non-group values from to do groups closes #422
- BUG - remove pending tag on order items marked as ordered closes #444

### Version 3.4.12 - 2020-08-02

- BUG - inconsistent results using .flat() on related tasks

### Version 3.4.11 - 2020-07-25

- IMPROVEMENT - added task categories to completion exports closes #418

### Version 3.4.10 - 2020-06-30

- ADDED - Automatic email capability and settings for orders, tasks completions, and backups closes #198
- ADDED - ability for user to define email addresses to receive early task notifications
- ADDED - ability to include attachment file names to task completions for later reference
- IMPROVEMENT - added existing admin users to the email selection dropdown
- IMPROVEMENT - improvements on category display on tasks page closes #407
- IMPROVEMENT - added random facts to some loading messages when available closes #408
- IMPROVEMENT - improve error handling on server
- IMPROVEMENT - slight modification of task relations closes #412
- BUG - add better reporting for a date mismatch on server causing issues with backups closes #413

### Version 3.4.9 - 2020-04-25

- IMPROVEMENT - added ability to set a fixed due date based on external timer closes #396
- IMPROVEMENT - make task clone a searchable dropdown for ease of use closes #401
- IMPROVEMENT - clear form errors on new tasks with subsequent creation closes #403
- IMPROVEMENT - Add ability to create task completion on new task creation closes #402
- IMPROVEMENT - remove note delete button for general users
- IMPROVEMENT - refactor task info form creation and display closes #392
- DOCUMENTATION - add required documentation missed for recent changes closes #391
- DOCUMENTATION - Add items to task documentation for new features
- BUG - limit menu counts to current hardware closes #398
- BUG - Hide download all attachments button when there are no attachments
- BUG - fix double backup creation when done automatically closes #390
- BUG - repair mismatch for hoses during backups
- BUG - To Do and Order's don't properly refresh when newly created closes #404
- BUG - Intermittent invalid time error on details popup open

### Version 3.4.8 - 2020-04-15

- REQUEST - remove attachment delete button for general users closes #383
- IMPROVEMENT - added indicators for due items in relevant options to menu closes #353
- IMPROVEMENT - hide empty data items in to do details window closes #386
- IMPROVEMENT - added created date on to do details page closes #385
- IMPROVEMENT - Added asterisks to required fields on forms closes #387
- IMPROVEMENT - added quick date range buttons to task exports closes #382
- IMPROVEMENT - Add option for cron schedule of updates instead of daily checks
- IMPROVEMENT - Add boolean option to additional info form closes #351
- IMPROVEMENT - added ability to delay tasks closes #30
- IMPROVEMENT - add versatility to upload component readout
- IMPROVEMENT - add ability to restore a local version of a backup closes #92
- IMPROVEMENT - Make backup and restore more robust
- IMPROVEMENT - allows download of all attachments on a task closes #237
- IMPROVEMENT - Added ability to download archive of all items closes #190
- BUG - when all due tasks complete on new page load message not appearing
- BUG - tweak date formatter to make sure 1970 date doesn't show closes #388
- BUG - force refresh for task mode changes closes #384

### Version 3.4.7 - 2020-03-25

- ADDED - Exporting task completions is now possible closes #367
- IMPROVEMENT - Allow PDF's to be opened directly in the browser closes #378
- IMPROVEMENT - Added reorder button to previous order items closes #296
- IMPROVEMENT - Add pending state to orders closes #364
- IMPROVEMENT - allow admin log in to persist over hardware changes and page refreshes closes #380

### Version 3.4.6 - 2020-03-17

- IMPROVEMENT - make tasks button go grey once clicked closes #368
- IMPROVEMENT - make landing page flow better with loading graphic closes #372
- IMPROVEMENT - added confirmation dialog to log delete, related to #377
- BUG - added ability for admin to delete log entry closes #377
- BUG - Add days button not showing in Operation's mode. closes #369
- BUG - when closing #362 introduced error in backup date math closes #374
- BUG - Fixes error editing existing log entry closes #376
- BUG - add math to offset zulu time by timezone on requesting device, related to #370

### Version 3.4.5 - 2020-02-28

- ADDED - System logs page closes #28,#208
- ADDED - thumbnails and image viewer for attachment component closes #360
- ADDED - Ability to download logs closes #217
- IMPROVEMENT - Only call list when clicked to reduce unnecessary traffic closes #344
- IMPROVEMENT - add date selections to log page
- IMPROVEMENT - Change sort order of previous task completions closes #356
- DOCUMENTATION - add details about how to best capture bug info and request features closes #342
- DOCUMENTATION - upgrade page changes
- DOCUMENTATION - updated some links and added minor info for feedback
- DOCUMENTATION - misspelled link closes #352
- IMPROVEMENT - upgrades to file upload dialogs
- IMPROVEMENT - change spacing on to do details closes #350
- IMPROVEMENT - change the previous to do table formatting to have better relevance closes #361
- TWEAK - Auto go to page if only one exists on landing closes #349
- TWEAK - add spaces to task list for easy viewing
- BUG - Added categories to task lists for references closes #348
- BUG - Unbind enter key from note entry to allow use of new lines closes #354
- BUG - Missing popup message for OQE on edit dialog closes #355
- BUG - Hose order calculation for large hoses
- BUG - Removed debounce timer from techname field
- BUG - Missing folders for deployment structure
- BUG - form tweak and allow blank emails to bypass unique test in admin closes #346,#347
- BUG - fix minor issues on hose page closes #345 IMPROVEMENT - add button to download excel form of hose order information
- BUG - reducing group form number does not remove end fields closes #340
- BUG - remove extra task data call closes #343
- BUG - Unable to properly edit to do items. closes #363
- BUG - change backup table to sort recent first closes #362

### Version 3.4.4 - 2020-01-14

- BUG - Form data display error causes no task details popup

### Version 3.4.3 - 2020-01-14

- BUG - broken links and upgrade document tweaks

### Version 3.4.2 - 2020-01-12

- IMPROVEMENT - landing page initial database and button layouts closes #320, #335
- IMPROVEMENT - hide test server button from deployed app closes #331
- IMPROVEMENT - Adding issue templates closes #240
- BUGS - found a few bugs when doing training on a system install closes #338

### Version 3.4.1 - 2020-01-10

- IMPROVEMENT - added creation and modified date to task closes #330
- BUG - fix hole where inactive task would display on due page sometimes closes #333
- BUG - Allow search on tasks page using a number for task ID searches closes #332

### Version 3.4.0 - 2019-12-19

- ADDED - roll documentation pages into docker for easy access closes #207
- DOCUMENTATION - add markdown cheat sheet to documentation
- DOCUMENTATION - test and add information on upgrade processes for site to documentation closes #316
- IMPROVEMENT - reduce docker size and streamline build process closes #321
- IMPROVEMENT - move new page creation to admin page for more
- IMPROVEMENT - move initial settings and dropdowns loaders to more seamless location in code closes #318
- IMPROVEMENT - Add ability to select none on hose sizes than can be made on site closes #319
- BUG - Changing transit mode with no tasks in list causes server lock up
- BUG - remove add days icon when not relevant closes #309
- BUG - lock up on initial load of hose default sizes

### Version 3.3.9 - 2019-12-07

- REQUEST - add delete button on hoses for admin use closes #314
- IMPROVEMENT - improve theme toggle accessibility closes #313
- TWEAK - add docker folder structure template and scripts to repo
- TWEAK - clean up code and integrate eslint (might induce bugs as it was a lot of small changes)
- BUG - menu still sticking when related to log in closes #315
- BUG - minor bugs post code cleanup

### Version 3.3.8 - 2019-12-06

- HOTFIX - duplicate keys on early reminder tasks causes lock up
- HOTFIX - Sticky menu bar
- HOTFIX - Note deletion on tasks error
- HOTFIX - Add new user error #310

### Version 3.3.7 - 2019-11-17

- REQUEST - Allow strings as hose ID's closes #305
- TWEAK - Give user an option for persistent side menu closes #297
- TWEAK - make text larger on meter input box
- TWEAK - added a basic error page to catch basic errors closes #230
- BUG - mod docker to allow hashing compilation
- BUG - time based interval not required for meter based closes #308
- BUG - tweak logic for meter based tasks closes #306

### Version 3.3.6 - 2019-11-14

- ADDED - Admin functionality closes #107
- HOTFIX - better escaping of excel import for special characters in text

### Version 3.3.5 - 2019-11-10

- REQUEST - added task hours closes #302
- TWEAK - work on import template closes #252

### Version 3.3.4 - 2019-11-10

- REQUEST - Add ability to switch hardware within the nav menu
- REQUEST - add SOC and OQE chips to category if they are activated closes #293
- TWEAK - remove log and drop techname debounce timer
- BUG - apply sorting to the button column to make it more consistent
- BUG - Search error if no description present in task
- BUG - No categories set on deactivated tasks showed some math instead of blank
- BUG - Allow 0 days (One Time) to be entered into import sheet
- BUG - Typo on SOC switches closes #292
- BUG - auto add vendor when saved order closes #295
- BUG - Creation of first meter errored out on validation closes #299

### Version 3.3.3 - 2019-11-02

- REQUEST - Add SOC and OQE to tasks closes #289
- TWEAK - adjust calenders to remove buttons and limit selections closes #287
- BUG - close row expansion when submitted closes #290
- BUG - remove button on details window closes #291

### Version 3.3.2 - 2019-11-01

- ADDED - To Order Page closes #243, #218, #227, #279
- ADDED - Hose Registry page
- IMPROVEMENT - move API test script to folder
- TWEAK - add test path to task route
- BUG - Make tech name dialog persistent to avoid accidental clearing
- HOTFIX - allow formatted text in the excel import sheet without error

### Version 3.2.3 - 2019-10-20

- IMPROVEMENT - added console output for docker log closes #221
- TWEAK - green button color on light theme
- BUG - prevent details from popping up on task edit save closes #281
- BUG - only update related when it has changed closes #255
- BUG - make search on task table persist between task saves closes #280
- HOTFIX - Build dependency fix's

### Version 3.2.2 - 2019-10-20

- IMPROVEMENT - Move notes dropdown button in To Do to better location in row
- IMPROVEMENT - Upgrade framework libraries
- FORMATTING - move description to left hand formatting for better visuals when using mark down
- TESTING - added api path to doc for reference when developing and testing
- BUG - Table loader not showing when searches executed
- BUG - new task numbering not properly incrementing

### Version 3.2.1 - 2019-10-09

- IMPROVEMENT - Add Wiki Link to menu bar
- BUG - missing To Do from backups closes #274
- BUG - move site version info to site settings page since php upgrade are gone closes #273
- BUG - print list updates on task change closes #276
- BUG - info form mods cause data to carry to other tasks closes #275
- BUG - toggle from edit mode didn't go back to details closes #277
- BUG - Hide non-existent menu options in the short term since the database till tries to show them

### Version 3.2.0 - 2019-10-04

- ADDED - Task export page
- BUG - fix error on task auto-number for first task in database
- BUG - tweak docker release name to match deployments
- BUG - Remove max character limit on task description
- BUG - Not showing messages when there are no tasks due
- BUG - Sticky loader button on day increment
- BUG - Theme default set not working
- BUG - Menu toggles showing pages that were not yet available
- BUG - fix task imports if no tasks already exist in database

### Version 3.1.0 - 2019-09-17

- ADDED - To Do Page
- IMPROVEMENT - made tech name entry field more responsive - closes #264
- IMPROVEMENT - visual indication of table search ongoing - closes #265
- IMPROVEMENT - add ability to add/modify/delete dropdown options in settings page
- BUG - Ignore leading/trailing spaces in task search
- BUG - Pushing log write without appropriate format would e

### Version 3.0.0

- ADDED - Ability to build additional information form and attach to a task
- ADDED - Ability to import tasks from excel document (4536cb1)
- IMPROVEMENT - add first/last buttons to table pagination on tasks (2404342)
- IMPROVEMENT - Set logs to rotate daily and archive up to a limit closes #248 (8deb9af)
- IMPROVEMENT - set submit buttons to show refresh status (closes #244) (9386fbf)
- IMPROVEMENT - Added tenant information to log (34fcac3)
- IMPROVEMENT - Move setting API URL to a vuex function
- IMPROVEMENT - Set up form field validation
- IMPROVEMENT - Adjust task print list to not print the website formatting
- IMPROVEMENT - Change empty database page to have button instead of plain text for new task
- IMPROVEMENT - Move to Nuxt/Express/Mongo to be better prepared for future mods
- BUG - Minimal auto-backup at 2 days
- BUG - limit visible options when editing meter task (3eccc18)
- BUG - Edit task in previous complete did not prompt for name (77484b5)
- BUG - change button coloring based on due date closes #250 (a229ba1)
- BUG - Inactive tasks showing up on active list (48e023a)
- BUG - Prevent accidental double task submission
- BUG - Prevent ability to make two meters of the same name
- BUG - Cross tenant results on catagory list creation
- BUG - Change URL for api calls to prevent double stack of path
- BUG - Move fibers to production build
- BUG - Move sass build to production

### Version 2.6.3

- IMPROVEMENT - added tooltips with string version of dates to easy reference (1 day ago, 28 days ago, 1 year from now, etc)
- IMPROVEMENT - added autofocus and ability for enter key to submit on single box forms (names, meter entries, etc)
- IMPROVEMENT - Flexible page layouts that better fit to screen sizes
- IMPROVEMENT - drag and drop capable uploads, modularized for general use
- IMPROVEMENT - Sort old to do items by descending date
- IMPROVEMENT - Modify tasks page to advise user of an empty table (usually seen only on new database creation)
- IMPROVEMENT - finish intigration of central variable store for easier future page adds
- IMPROVEMENT - rename "Recently Completed" to "Previously Completed" so it's clear the list is a full archive rather than one with a time based limitation
- IMPROVEMENT - Add loading bar to page when changing modes
- BUG - submitters name didnt show up when marking an item as ordered
- BUG - fix bug where user couldn't type in description field of to do/to order pages
- BUG - fix issue where a new meter would clear pending task data already filled out
- BUG - Fix log writes

### Version 2.6.2

- BUG - Notify user of folder permissions issue on initial load

### Version 2.6.1

- ADDED - Set up auto mailer for To Order page in order to be able to update folks at home what can be hand carried out
- BUG - small imporovements on default values in To Order page

### Version 2.6.0

- ADDED - Items To Order page
- IMPROVEMENT - Set up an auto refresh timer that works based on idle/non-idle states
- IMPROVEMENT - Log download button in the backups page
- IMPROVEMENT - Added updage log download (on error) button as well as auto-emailer on update error
- BUG - Remove Obs card export email if there are no new cards to export
- BUG - Observation card export email not pulled correct date range

### Version 2.5.1

- ADDED - Notes for To Do completion
- IMPROVEMENT - Minor tweaks to To Do list UI
- IMPROVEMENT - Added progress bar for upgrade process

### Version 2.5.0

- ADDED - Auto emailer function for observation cards and offsite backups
- ADDED - Update function for pages that are conntected to the internet
- IMPROVEMENT - refactor site to use a central repository for broadly used variables

### Version 2.4.0

- ADDED - Safety category and Observation card page
- IMPROVEMENT - change support link to git.rovcode.com
- IMPROVEMENT - Add error checking for database connection and report issues to user
- IMPROVEMENT - move success info log into toast message call
- BUG - add initial value to remove minor error on new task creation
- BUG - SQL template didn't include TODO table creation
- BUG - Fix initial table query results issue

### Version 2.3.0

- ADDED - To Do List Page
- IMPROVEMENT - added automatic creation of required folders in supportDocs
- IMPROVEMENT - add icon for shorcuts and browser tab
- BUG - fix hour interval dropdown in new task popup
- BUG - set active/idle status to prevent auto-refresh while tasks are being entered

### Version 2.2.0

- ADDED - Settings page including meters, deactivated tasks list, backups
- ADDED - Added task export page with ability to export whole tasks and/or completions
- ADDED - ability to change task mode between Ops, Transit, Layup, and Demobe and have due dates modified as needed
- IMPROVEMENT - Added rotating log files for debugging
- IMPROVEMENT - set required fields when creating a new task
- IMPROVEMENT - due task list refreshes once a minute to remove tasks signed off at other terminals
- IMPROVEMENT - added ability to set default theme for selected database
- IMPROVEMENT - added ability to change database by changing web address etc: /#/maxx1/tasks
- IMPROVEMENT - moved freqOptions, transitionalIntervals, and meterIntervals into tables to facilitate later expansion

### Version 2.1.1

- IMPROVEMENT - Remove 'Hour Meter' option from category selections to reduce chances of accidental selection
- BUG - If array empty, set starting ID for new task properly
- BUG - make menu choices more consistant when switching modes
- BUG - fix bug where databases error's if special character exists in category field

### Version 2.1.0

- IMPROVEMENT - add data to hour meter details window

### Version 2.0.2

- BUG - task submission after hourly task cancellation

### Version 2.0.1

- BUG - fix due date calculation bug

### Version 2.0.0

- integrate version numbering system
