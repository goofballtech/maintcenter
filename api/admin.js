const express = require('express')
const logger = require('../server/logger')
const bcrypt = require('bcryptjs')

const adminRoute = express.Router({ mergeParams: true })
const user = require('./models/usersModel')

adminRoute
  .route('/user/:id?')
  .get(async function(req, res) {
    try {
      let users = await user.find({}, { __v: 0, password: 0 })

      return res.status(200).json(users)
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json('An error has occurred, see log for details')
    }
  })
  .put(async function(req, res) {
    let userToUpdate = req.params.id
    let newUserData = req.body
    try {
      if (newUserData.password) {
        newUserData.password = await bcrypt.hash(newUserData.password, 5)
      }
      await user.findByIdAndUpdate(userToUpdate, newUserData)
      return res.status(200).json(`${newUserData.username} has been updated`)
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json('An error has occurred, see log for details')
    }
  })
  .post(async function(req, res) {
    const newUser = new user(req.body)
    newUser.password = await bcrypt.hash(newUser.password, 5)
    newUser
      .save()
      .then(() => {
        res.status(200).json(`Added user: ${newUser.username}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(async function(req, res) {
    let userToDelete = req.params.id
    try {
      await user.findByIdAndRemove(userToDelete)
      return res.status(200).json({ confirmed: true, message: `User has been deleted` })
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json('An error has occurred, see log for details')
    }
  })

adminRoute.route('/login').post(async function(req, res) {
  const userToTest = req.body

  try {
    const usernameLogin = await user.findOne({
      username: { $regex: userToTest.username, $options: '/i' },
    })
    if (usernameLogin) {
      attemptLogin(userToTest, usernameLogin)
    } else {
      return res.status(200).json('Username Not Found')
    }
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(200).json('An error has occurred, see log for details')
  }

  async function attemptLogin(userToTest, existingUserObject) {
    const passCheck = await bcrypt.compare(userToTest.password, existingUserObject.password)
    if (passCheck && userToTest.password == 'Ocean123') {
      return res.status(200).json({ forceChange: true, _id: existingUserObject._id })
    } else if (passCheck) {
      let trimmedObject = JSON.parse(JSON.stringify(existingUserObject))
      delete trimmedObject.__v
      delete trimmedObject.password
      return res.status(200).json(trimmedObject)
    } else {
      return res.status(200).json('Password does not match')
    }
  }
})

module.exports = adminRoute

// https://medium.com/createdd-notes/starting-with-authentication-a-tutorial-with-node-js-and-mongodb-25d524ca0359
