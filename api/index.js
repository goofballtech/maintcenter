require('dotenv').config() // load up the .env file as process.env[varName]
const express = require('express')
require('express-async-errors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const autoFunctions = require('./autoFunctions')
const app = express()

const dev = process.env.NODE_ENV === 'development'

//? If there is an error about connection to undefined:27017 then check the .env file
const mongoServer = dev ? process.env.MONGO_HOST : 'mongo'
const mongoPort = dev ? process.env.MONGO_PORT : '27017'

const mongoURI = `mongodb://${mongoServer}:${mongoPort}/maintCenter?authSource=admin`


mongoose.connect(mongoURI, {
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  serverSelectionTimeoutMS: 60000, // MongoDB driver will try to find a server to send any given operation to, and keep retrying for serverSelectionTimeoutMS milliseconds
  connectTimeoutMS: 30000, // MongoDB driver will wait before killing a socket due to inactivity during initial connection
  socketTimeoutMS: 45000, // MongoDB driver will wait before killing a socket due to inactivity after initial connection
})

const db = mongoose.connection

// eslint-disable-next-line no-console
// db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
  // eslint-disable-next-line no-console
  console.log(`Database Connection successful at ${mongoServer}:${mongoPort}`)
})

// mount the router on the app
app.use(cors())
app.options('*', cors())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use('/api', require('./routes'))

autoFunctions

mongoose.set('useFindAndModify', false) // allows use of findOneAnd* functions
mongoose.set('useCreateIndex', true)

module.exports = app
