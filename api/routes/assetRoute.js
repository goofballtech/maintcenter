const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const assetRoute = express.Router({ mergeParams: true })

const baseAssets = require('../models/assetsModel')

/** # Assets API routes */

/**
 ## assets/categories 
 ### get: returns a list of categories currently applied to assets
 #### query: 
  - active: [true] set false if results should also include inactive assets
 */
assetRoute.route('/categories').get(async function(req, res) {
  try{
    let Assets
    if (req.query.allHardware) {
      Assets = baseAssets
    } else {
      Assets = baseAssets.byTenant(req.params.db)
    }
    let active
    req.query.active ? (active = req.query.active) : (active = true)
    let assetList = await Assets.find({ active: { $eq: active } }).distinct('category')
    res.status(200).json(assetList.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' })))
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

/**
 ## assets/manufacturers 
 ### get: returns a list of manufacturers currently applied to assets
 #### query:
  - active: [true] set false if results should also include inactive assets
 */
assetRoute.route('/manufacturers').get(async function(req, res) {
  try{
    let Assets
    if (req.query.allHardware) {
      Assets = baseAssets
    } else {
      Assets = baseAssets.byTenant(req.params.db)
    }
    let active
    req.query.active ? (active = req.query.active) : (active = true)
    let manList = await Assets.find({ active: { $eq: active } }).distinct('manufacturer')
    res.status(200).json(manList.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' })))
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

/**
 ## assets/locations 
 ### get: returns a list of locations currently applied to assets
 #### query:
  - active: [true] set false if results should also include inactive assets
 */
assetRoute.route('/locations').get(async function(req, res) {
  try{
    let Assets
    if (req.query.allHardware) {
      Assets = baseAssets
    } else {
      Assets = baseAssets.byTenant(req.params.db)
    }
    let active
    req.query.active ? (active = req.query.active) : (active = true)
    let locationList = await Assets.find({ active: { $eq: active } }).distinct('location.description')
    res.status(200).json(locationList.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' })))
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

/**
 ## assets/list 
 ### get: returns a list of assets as text/value objects. Value is the mongo _id
 #### query:
  - active: [true] set false if results should also include inactive assets
 */
assetRoute.route('/list').get(async function(req, res) {
  try{
    let Assets
    if (req.query.allHardware) {
      Assets = baseAssets
    } else {
      Assets = baseAssets.byTenant(req.params.db)
    }
    let active
    req.query.active ? (active = req.query.active) : (active = true)
    let assetList = await Assets.find({ active: { $eq: active } })
    if (assetList.length > 1) {
      assetList = assetList
        .map(asset => {
          return {
            value: asset._id,
            text: `${asset.assetID} : ${asset.name} (${asset.category.join(', ')})`,
          }
        })
        .sort((a, b) => a.text.localeCompare(b.text, undefined, { sensitivity: 'base' }))
    }
    res.status(200).json(assetList)
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

/** 
 ## assets/:id?
 ### get: returns all asset objects for :db in params path
 #### params: 
  - id: Optional, if provided results are limited to only the specified asset. Should be mongo _id, if _id does not exists will return error
 #### query:
  - checkID: pass a value to check against the assetID field of all existing assets in the database. Will return if the assetID is available for use or already used. If used other params/queries will be ignored
 ### put: id is required, will update specified assets or notify user if id is not a valid mongo _id
 ### post: creates a new asset from object provided in req.body
 */
assetRoute
  .route('/:id?')
  .get(async function(req, res) {
    try{
    if (req.query.checkID) {
      let isUsed = await baseAssets.findOne({ assetID: req.query.checkID })
      res.status(200).json(isUsed ? { status: 'not available' } : { status: 'available' })
    } else if (req.params.id) {
      let returnedAsset = {}
      if (mongoose.Types.ObjectId.isValid(req.params.id)) {
        returnedAsset = await baseAssets.findById(req.params.id)
      }
      res.status(200).json(returnedAsset)
    } else {
      res.status(200).json(await baseAssets.find())
    }
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
  })
  .put(async function(req, res) {
  try{
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      if (req.query.addToArray) {
        const toAddDetails = req.body
        let addTo = await baseAssets.findByIdAndUpdate(
          req.params.id,
          { $push: { [req.query.addToArray]: toAddDetails } },
          { runValidators: true }
        )
        return res
          .status(200)
          .json(`New ${req.query.addToArray} added to Asset "${addTo.name}" by ${toAddDetails.enteredBy}`)
      } else if (req.query.addMovedFrom) {
        const movedFrom = req.body
        await baseAssets.findByIdAndUpdate(
          req.params.id,
          { $set: { 'location.$[recent].dateMovedFrom': movedFrom.dateMovedFrom } },
          { arrayFilters: [{ 'recent._id': req.query.locationID }] }
        )
        return res.status(200).json('Updated move from date on current location entry')
      } else {
        let returnedAsset = await baseAssets.findByIdAndUpdate(req.params.id, req.body, { runValidators: true })
        return res.status(200).json(`Successfully updated ${returnedAsset.name}`)
      }
    } else {
      res.status(200).json(`Asset ID is not valid`)
    }
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
  })
  .post(async function(req, res) {
  try{
    const newAsset = new baseAssets(req.body)
    newAsset.location = {
      enteredBy: newAsset.techName,
      dateMovedTo: new Date(),
      description: req.body.location,
    }
    newAsset.tenantId = req.params.db
    await newAsset.save()
    res.status(200).json(`${newAsset.createdBy} added new a asset named ${newAsset.name}`)
  }catch(err){
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
  })

module.exports = assetRoute
