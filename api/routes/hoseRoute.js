const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const hoseRoute = express.Router({ mergeParams: true })

const baseHose = require('../models/hosesModel')
const baseHoseDefaults = require('../models/hoseDefaultsModel')
const baseHoseCompletions = require('../models/hoseCompletionsModel')
const { addDays } = require('date-fns/esm')

// get existing list of hardware from hoses in table
hoseRoute.route('/hardware').get(async function (req, res) {
  const Hose = baseHose.byTenant(req.params.db)
  try {
    const hardwareList = await Hose.find({ tenantId: req.params.db }).distinct('hardware')
    hardwareList.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    res.status(200).json(hardwareList)
  } catch (err) {
    if (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
  }
})

hoseRoute.route('/list').get(async function (req, res) {
  const Hose = baseHose.byTenant(req.params.db)
  try {
    let hardwareList = await Hose.find({})
    hardwareList = hardwareList.map(hose => {
      return {
        text: `Tag:${hose.tagNumber} - ${hose.hardware} - ${hose.location}`,
        value: hose._id,
      }
    })
    res.status(200).json(hardwareList)
  } catch (err) {
    if (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
  }
})

hoseRoute.route('/batch').put(async function (req, res) {
  if (req.body.selection && req.body.oldValue && req.body.newValue && req.body.techName) {
    let Hose = baseHose.byTenant(req.params.db)
    const changes = req.body
    if (req.query.interval) {
      let changeNames = {
        interval: '',
        next: '',
      }
      switch (changes.selection) {
        case 'inspectionInterval':
          changeNames.interval = 'inspectionInterval'
          changeNames.next = 'nextInspection'
          changeNames.last = 'lastInspection'
          break
        case 'changeInterval':
          changeNames.interval = 'changeInterval'
          changeNames.next = 'nextChange'
          changeNames.last = 'lastChange'
          break
        default:
          break
      }

      let allHoses = await Hose.find({ [changes.selection]: changes.oldValue })
      if (allHoses.length) {
        allHoses = allHoses.map(hose => {
          const newDate = addDays(hose[changeNames.last], changes.newValue)
          return {
            updateOne: {
              filter: { _id: hose._id },
              update: {
                $set: { [changeNames.interval]: changes.newValue, [changeNames.next]: newDate },
              },
            },
          }
        })
        let updated = await Hose.bulkWrite(allHoses)
        res
          .status(200)
          .json(
            `${changes.techName} has done a batch hose mod to modify ${changes.selection} from ${changes.oldValue} to ${changes.newValue
            }. ${updated.nModified} records were modified`
          )
      } else {
        res.status(200).json('No hoses were found with the selected value.')
      }
    }
  } else {
    res.status(200).json('Some required values missing, nothing updated')
  }
})

// Setting/getting default part numbers for hoses by size
hoseRoute
  .route('/defaults/:size?')
  .get(async function (req, res) {
    //? get the whole list
    let HoseDefaults = baseHoseDefaults.byTenant(req.params.db)
    //? Filter out the old way of keeping the below values
    function filterHoseDefaults(defaults) {
      const filtered = defaults.filter(item => {
        if (
          item.value == 'defaultInspection' ||
          item.value == 'maxInspection' ||
          item.value == 'defaultChange' ||
          item.value == 'maxChange' ||
          item.value == 'maxHoseSizeMadeOnSite'
        ) {
          return false
        } else {
          return true
        }
      })
      filtered.sort((a, b) => a.value - b.value)
      return filtered
    }
    //? If it exists but does say intervals then look for the id provided
    if (req.params.size && req.params.size !== 'intervals') {
      HoseDefaults.findOne({ value: req.params.size }).exec(function (err, defaults) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(defaults)
      })
    } else {
      //? if no id is provided then give the whole list or create one from defaults if required
      try {
        const defaults = await HoseDefaults.find({})
        if (!defaults.length) {
          const defaultsCreate = require('../objects/hoseDefaults.json')
          const withTenant = defaultsCreate.map(item => {
            item.tenantId = req.params.db
            return item
          })
          const tryAgain = await baseHoseDefaults.insertMany(withTenant)
          logger.info(
            JSON.stringify({
              details: 'No hose defaults present in database, inserting default object',
              url: req.originalUrl,
              method: req.method,
            })
          )
          res.status(200).json(filterHoseDefaults(tryAgain))
        } else {
          res.status(200).json(filterHoseDefaults(defaults))
        }
      } catch (error) {
        logger.error(
          JSON.stringify({
            details: error,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(200).json('An error has occurred, see log for details')
      }
    }
  })
  .put(function (req, res) {
    const HoseDefaults = baseHoseDefaults.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.size)) {
      HoseDefaults.findByIdAndUpdate(req.params.size, req.body, { runValidators: true }, function (err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully updated hose defaults`)
      })
    } else {
      res.status(200).json(`Hose default ID is not valid`)
    }
  })
  .post(function (req, res) {
    const newDefaultHose = new baseHoseDefaults(req.body)
    newDefaultHose.tenantId = req.params.db
    newDefaultHose
      .save()
      .then(() => {
        res.status(200).json(`${req.body.techName} added default hose data for ${newDefaultHose.text}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function (req, res) {
    const HoseDefaults = baseHoseDefaults.byTenant(req.params.db)
    HoseDefaults.findByIdAndDelete(req.params.size, function (err, hose) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Successfully removed hose defaults for ${hose.text}`)
    })
  })

// check availability of tag number and/or give the next number in line
hoseRoute.route('/checktag/:tag?').get(function (req, res) {
  const Hoses = baseHose.byTenant(req.params.db)
  if (req.params.tag) {
    if (req.query.custom) {
      Hoses.find({ customTag: req.params.tag }).exec(function (err, hose) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        if (hose.length) {
          res.status(200).json('exists')
        } else {
          res.status(200).json('custom')
        }
      })
    } else {
      Hoses.find({ tagNumber: req.params.tag }).exec(function (err, hose) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        if (hose.length) {
          res.status(200).json('exists')
        } else {
          res.status(200).json('available')
        }
      })
    }
  } else {
    Hoses.findOne({})
      .sort({ tagNumber: -1 })
      .exec(function (err, highest) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        if (highest) {
          res.status(200).json({
            message: `The next available tag is ${highest.tagNumber + 1}`,
            nextNumber: highest.tagNumber + 1,
          })
        } else {
          res.status(200).json({
            message: `There are no tags stored yet`,
            nextNumber: 1,
          })
        }
      })
  }
})

// inspect/change completions
hoseRoute
  .route('/completions/:id?')
  .get(function (req, res) {
    const HoseCompletions = baseHoseCompletions.byTenant(req.params.db)

    if (req.params.id) {
      if (mongoose.Types.ObjectId.isValid(req.params.id)) {
        HoseCompletions.find({ hoseId: req.params.id }).exec(function (err, completions) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(completions)
        })
      } else {
        HoseCompletions.find({ tagNumber: req.params.id }).exec(function (err, completions) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(completions)
        })
      }
    } else {
      HoseCompletions.find({}).exec(function (err, completions) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(completions)
      })
    }
  })
  .post(function (req, res) {
    const newCompletion = new baseHoseCompletions(req.body)
    newCompletion.tenantId = req.params.db
    newCompletion
      .save()
      .then(() => {
        res
          .status(200)
          .json(
            `${newCompletion.techName} added ${newCompletion.inspectionDate ? 'inspection' : 'change'} for hose #${req.body.tagNumber ? req.body.tagNumber : req.body.customTag
            }`
          )
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })

// CRUD Hose
hoseRoute
  .route('/:id?')
  .get(async function (req, res) {
    const Hoses = baseHose.byTenant(req.params.db)
    if (req.params.id) {
      if (mongoose.Types.ObjectId.isValid(req.params.id)) {
        Hoses.find({ _id: req.params.id }).exec(function (err, hose) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(hose)
        })
      } else {
        Hoses.find({ tagNumber: req.params.id }).exec(function (err, hose) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(hose)
        })
      }
    } else {
      if (req.query.count) {
        try {
          let numbers = {
            nextInspection: 0,
            nextChange: 0,
          }
          await Promise.all(
            ['nextInspection', 'nextChange'].map(async column => {
              return (numbers[column] = await Hoses.find({
                active: { $eq: true },
                [column]: { $lte: new Date() },
              }))
            })
          )

          numbers.nextInspection = numbers.nextInspection.length - numbers.nextChange.length
          numbers.nextChange = numbers.nextChange.length

          return res.status(200).json({ numbers })
        } catch (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
      } else {
        Hoses.find({ active: { $eq: true } }).exec(function (err, hose) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(hose)
        })
      }
    }
  })
  .put(function (req, res) {
    const Hoses = baseHose.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      Hoses.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }, function (err, hose) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res
          .status(200)
          .json(
            `${req.body.techName} has successfully updated hose #${hose.tagNumber ? hose.tagNumber : hose.customTag}`
          )
      })
    } else {
      res.status(200).json(`Hose ID is not valid`)
    }
  })
  .post(function (req, res) {
    const newHose = new baseHose(req.body)
    newHose.tenantId = req.params.db
    newHose
      .save()
      .then(() => {
        res
          .status(200)
          .json(`${req.body.techName} added hose #${newHose.tagNumber ? newHose.tagNumber : newHose.customTag}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function (req, res) {
    const Hoses = baseHose.byTenant(req.params.db)
    Hoses.findByIdAndDelete(req.params.id, function (err, hose) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Successfully removed ${hose.tagNumber}`)
    })
  })

module.exports = hoseRoute
