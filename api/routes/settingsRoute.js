const express = require('express')
const logger = require('../../server/logger')

const settingsRoute = express.Router({ mergeParams: true })

const baseSettings = require('../models/settingsModel')
const baseDropDowns = require('../models/dropDownsModel')

import { updateAnEmailObject } from '../utils/settingsHelpers'

// Add a new set of hardware to the exiting server
settingsRoute.route('/createNewHardware').get(async function (req, res) {
  const Settings = baseSettings.byTenant(req.params.db)
  const settingsObj = await Settings.find().catch(function (err) {
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(200).json({
      error: 'An error has occurred, see log for details',
    })
  })

  if (Object.keys(settingsObj).length) {
    res.status(200).json('It seems like that page already exists')
  } else {
    const newSettings = new baseSettings()
    newSettings.tenantId = req.params.db
    newSettings.friendlyName = req.params.db
    // all other default settings are made by their defaults in the schema
    newSettings
      .save()
      .then(res.status(200).json('New hardware created'))
      .catch(err => {
        res.status(200).json({ error: `unable to save settings to database: ${err}` })
      })
  }
})

settingsRoute
  .route('/email')
  .get(async function (req, res) {
    const Settings = baseSettings.byTenant(req.params.db)
    const Users = require('../models/usersModel')

    const emailsTo = await Settings.find({}).distinct('autoEmailOptions.toOrder.emailTo')
    const emailsCC = await Settings.find({}).distinct('autoEmailOptions.toOrder.emailCC')
    const emailsBCC = await Settings.find({}).distinct('autoEmailOptions.toOrder.emailBCC')
    const usersWithEmail = await Users.find({})
    const userEmails = usersWithEmail.map(user => (user.email ? `${user.username} <${user.email}>` : undefined))

    //? If the username is set but not an email the above sets that to item undefined. Final filter kicks those from the list before sending.
    const emailList = new Set([...emailsTo, ...emailsCC, ...emailsBCC, ...userEmails].filter(item => item))
    res.status(200).json([...emailList])
  })
  .put(async function (req, res) {
    const Settings = baseSettings.byTenant(req.params.db)
    if (req.query.taskSearch) {
      Settings.findOneAndUpdate(
        {},
        { $set: { 'autoEmailOptions.taskExport.searchCriteria': req.body } },
        { runValidators: true, new: true },
        function (err) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json({
              error: 'An error has occurred, see log for details',
            })
          }
          res.status(200).json(`Successfully updated auto email task query options`)
        }
      )
    } else if (req.query.updateWholeObject) {
      Settings.findOneAndUpdate(
        {},
        { $set: { autoEmailOptions: req.body } },
        { runValidators: true, new: true },
        function (err) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json({
              error: 'An error has occurred, see log for details',
            })
          }
          res.status(200).json(`Successfully updated auto email options`)
        }
      )
    } else if (req.body.update && req.body.objectName) {
      const updatedEmail = await updateAnEmailObject(req.params.db, req.body.update, req.body.objectName)
      res
        .status(200)
        .json(`Successfully updated auto email options for ${updatedEmail.autoEmailOptions[req.body.objectName].name}`)
    } else {
      res.status(200).json('Something was missing from the query, cannot complete operation')
    }
  })

// get menu options from the specified dropdown list ex:meterIntervals/frequencyOptions
settingsRoute.route('/:menu').get(async function (req, res) {
  const DropDowns = baseDropDowns.byTenant(req.params.db)

  const menuOptions = await DropDowns.find({ menu: req.params.menu })
    .sort({ value: 1 })
    .catch(function (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    })

  if (!menuOptions.length) {
    const defaultsCreate = require('../objects/dropDowns.json')
    const withTenant = defaultsCreate.map(item => {
      item.tenantId = req.params.db
      return item
    })
    await baseDropDowns.insertMany(withTenant)
    logger.info(
      JSON.stringify({
        details: 'No menu defaults present in database, inserting default object',
        url: req.originalUrl,
        method: req.method,
      })
    )
    const tryAgain = await DropDowns.find({ menu: req.params.menu }).sort({ value: 1 })
    res.status(200).json(tryAgain)
  } else {
    res.status(200).json(menuOptions)
  }
})

settingsRoute
  .route('/dropDowns/:id?')
  // new menu item
  .post(function (req, res) {
    if (req.body.text && req.body.value && req.body.menu) {
      const newOption = new baseDropDowns(req.body)
      newOption.tenantId = req.params.db
      newOption
        .save()
        .then(() => {
          res.status(200).json(`Added ${req.body.text} option on ${req.body.menu}`)
        })
        .catch(err => {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json({
            error: 'An error has occurred, see log for details',
          })
        })
    } else {
      res.status(200).json(
        `Error: Need Menu, value, and display text. 
      menu:${req.body.menu}, 
      value:${req.body.value}, 
      text:${req.body.text}`
      )
    }
  })
  // update list item
  .put(function (req, res) {
    const DropDowns = baseDropDowns.byTenant(req.params.db)
    DropDowns.findByIdAndUpdate(req.params.id, req.body, { runValidators: true, new: true }, function (err, item) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json({
          error: 'An error has occurred, see log for details',
        })
      }
      res.status(200).json(`Successfully updated drop down option for ${item.menu}`)
    })
  })
  // delete by ID
  .delete(function (req, res) {
    const DropDowns = baseDropDowns.byTenant(req.params.db)
    DropDowns.findByIdAndDelete(req.params.id, function (err, item) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json({
          error: 'An error has occurred, see log for details',
        })
      }
      res.status(200).json(`Successfully removed ${item.text} from ${item.menu}`)
    })
  })

// get main site settings object
settingsRoute
  .route('/')
  .get(async function (req, res) {
    try {
      const Settings = baseSettings.byTenant(req.params.db)
      const settingsObj = await Settings.find()

      settingsObj.length
        ? res.status(200).json(settingsObj)
        : res.status(200).json(`That hardware does not currently exist`)
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })
  .put(function (req, res) {
    const Settings = baseSettings.byTenant(req.params.db)
    Settings.findOneAndUpdate({}, req.body, { runValidators: true, new: true }, function (err, settings) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json({
          error: 'An error has occurred, see log for details',
        })
      }
      res.status(200).json(`Successfully updated settings for ${settings.friendlyName}`)
    })
  })

module.exports = settingsRoute
