const express = require('express')
const fs = require('fs-extra')
const zip = require('adm-zip')
const logger = require('../../server/logger')

const path = require('path')

const backupRoute = express.Router({ mergeParams: true })

import { makeZip } from '../utils/helpers'
import { format } from 'date-fns/esm'
import { createBackup } from '../utils/backupHelpers'

const baseDir = path.join(process.cwd(), 'supportDocs')

backupRoute.route('/download/:date?').get(async function(req, res) {
  // This downloads everything for all the tenants
  if (req.params.date === 'everything') {
    if (req.query.prepare === 'true') {
      const options = {
        mode: 0o2775, // file/folder creation mode
      }
      await fs.ensureDir(path.join(baseDir, 'downloads'), options)
      let zippedAttachments = new zip()

      zippedAttachments.addLocalFolder(baseDir)

      zippedAttachments.writeZip(
        path.join(
          baseDir,
          'downloads',
          `${req.params.db}_files_${format(new Date(), 'yyyy-MM-dd')}_CompleteBackupDownload.zip`
        )
      )

      res.status(200).json({
        zipPrepared: true,
        zipPath: path.join(
          baseDir,
          'downloads',
          `${req.params.db}_files_${format(new Date(), 'yyyy-MM-dd')}_CompleteBackupDownload.zip`
        ),
      })
    } else {
      let prepared = fs.existsSync(req.query.zipPath)
      if (prepared) {
        res.download(req.query.zipPath)
        fs.remove(path.join(baseDir, 'downloads'))
      } else {
        res.status(200).json("The specified backup file doesn't seem to exist")
      }
    }
    // This downloads all attachments for just the current tenant
  } else if (req.params.date === 'allAttachments') {
    const dir = path.join(baseDir, req.params.db)
    // TODO Dumping the pictur update for now until i can add something to ui for selection and test this better so it doesnt time out per #623
    //! const extDir = path.join(baseDir, 'ExternalUpdate')
    if (req.query.prepare === 'true') {
      const options = {
        mode: 0o2775, // file/folder creation mode
      }
      await fs.ensureDir(path.join(dir, 'downloads'), options)
      let zippedAttachments = new zip()

      zippedAttachments.addLocalFolder(dir)
      //! zippedAttachments.addLocalFolder(extDir)

      zippedAttachments.writeZip(
        path.join(dir, 'downloads', `${req.params.db}_files_${format(new Date(), 'yyyy-MM-dd')}_download.zip`)
      )

      res.status(200).json({
        zipPrepared: true,
        zipPath: path.join(dir, 'downloads', `${req.params.db}_files_${format(new Date(), 'yyyy-MM-dd')}_download.zip`),
      })
    } else {
      let prepared = fs.existsSync(req.query.zipPath)
      if (prepared) {
        res.download(req.query.zipPath)
        fs.remove(path.join(dir, 'downloads'))
      } else {
        res.status(200).json("The specified backup file doesn't seem to exist")
      }
    }
  } else if (req.params.date) {
    let zipObj

    if (req.query.encodeBackup) {
      zipObj = await makeZip(
        path.join(baseDir, req.params.db, 'backups'),
        req.params.date,
        `${req.params.date}_${req.params.db}_backup`,
        true
      )
      return res.status(200).json({
        filename: zipObj.filename,
        base64: zipObj.base64,
      })
    } else {
      zipObj = await makeZip(
        path.join(baseDir, req.params.db, 'backups'),
        req.params.date,
        `${req.params.date}_${req.params.db}_backup`
      )
      res.download(path.join(zipObj.path, zipObj.filename))
    }
  } else {
    res.status(200).json('No backup specified in the download request')
  }
})

backupRoute
  .route('/restore/:date?')
  .get(async function(req, res) {
    const backupPath = path.join(baseDir, req.params.db, 'backups')

    try {
      const pathExists = await fs.pathExists(backupPath)
      if (pathExists && req.params.date) {
        const requestedDateExists = await fs.pathExists(path.join(backupPath, req.params.date))
        if (requestedDateExists) {
          fs.readdir(path.join(backupPath, req.params.date), async function(err, backups) {
            if (err) {
              logger.error(
                JSON.stringify({
                  details: err,
                  url: req.originalUrl,
                  method: req.method,
                })
              )
              return res.status(400).json({
                error: 'An error has occurred, see log for details',
              })
            }
            for (let i = 0; i < backups.length; i++) {
              backups[i] = backups[i].replace(/.json/i, '')
            }
            res.status(200).json(backups)
          })
        } else {
          res
            .status(400)
            .json(
              `A restore attempted was made for ${req.params.date} on the ${req.params.db} database, no such backup exists`
            )
        }
      } else {
        res.status(200).json('Either there are no backups or one was not specified for restore')
      }
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
    }
  })
  .put(async function(req, res) {
    const backupPath = req.query.path ? req.query.path : path.join(baseDir, req.params.db, 'backups')
    let restored = 'restored'
    try {
      const fullPath = req.query.path ? req.query.path : path.join(backupPath, req.params.date)
      const pathExists = await fs.pathExists(fullPath)

      if (pathExists) {
        let restoreFileList = await fs.readdir(fullPath)
        let modelList = await fs.readdir('./api/models')
        let backupDetails = {}
        if (fs.existsSync(path.join(fullPath, 'dbInfo.json'))) {
          backupDetails = fs.readJsonSync(path.join(fullPath, 'dbInfo.json'))
          if (backupDetails.tenantId !== req.params.db) {
            return res
              .status(200)
              .json(
                `The backup attempting to be restored does not match the current page. Please create the ${backupDetails.tenantId} page and restore from there (we are currently on ${req.params.db})`
              )
          } else {
            logger.log({
              level: 'info',
              message: `Backup being restored was saved on ${backupDetails.backupDate}`,
              tenant: req.params.db,
            })
          }
        }

        for (let i = 0; i < restoreFileList.length; i++) {
          if (!req.query[restoreFileList[i].replace(/.json/i, '')]) {
            const restoreObject = fs.readJsonSync(path.join(fullPath, restoreFileList[i]))

            const thisModelFile = modelList.find(
              (file) => file.toLowerCase().includes(restoreFileList[i].substr(0, restoreFileList[i].length - 6)) // pull .json and the s from the back of the name for better matching
            )

            if (thisModelFile) {
              modelList.splice(modelList.indexOf(thisModelFile), 1)
              let update = {
                name: restoreFileList[i].replace(/.json/i, ''),
                object: require(`../models/${thisModelFile}`),
              }
              if (restoreFileList[i] !== 'settings.json' && restoreFileList[i] !== 'users.json') {
                await update.object.deleteMany({ tenantId: req.params.db })
                await update.object.insertMany(restoreObject, { ordered: false })
                restored = `${restored} ${restoreFileList[i].replace(/.json/i, '')},`
              }
            }
          }
        }
        res.status(200).json(restored)
      } else {
        res.status(200).json('Either there are no backups or one was not specified for restore')
      }
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
    }
  })

backupRoute
  .route('/:date?')
  .get(async function(req, res) {
    const backupPath = path.join(baseDir, req.params.db, 'backups')
    try {
      const pathExists = await fs.pathExists(backupPath)
      if (pathExists) {
        fs.readdir(backupPath, (error, backupFolders) => {
          if (error) {
            logger.error(
              JSON.stringify({
                details: error,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json({
              error: 'An error has occurred, see log for details',
            })
          }
          let downloadIndex = backupFolders.indexOf('downloads')
          if (downloadIndex > -1) {
            backupFolders.splice(downloadIndex, 1)
          }

          res.status(200).json(
            backupFolders.map((backup) => {
              return { date: backup }
            })
          )
        })
      } else {
        res.status(200).json([])
      }
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      res.status(400).json('An error has occurred, see log for details')
    }
  })
  .post(async function(req, res) {
    createBackup(req.params.db, req.query.maxBackups, req, res)
  })
  .delete(async function(req, res) {
    const baseBackupPath = req.query.path
      ? path.join(baseDir, req.params.db, req.query.path)
      : path.join(baseDir, req.params.db, 'backups')
    try {
      if (req.query.path) {
        const pathExists = await fs.pathExists(baseBackupPath)
        if (pathExists) {
          await fs.remove(baseBackupPath)
          return res.status(200).json(`Removed temporary files for uploaded backup`)
        }
      } else if (req.params.date) {
        const pathExists = await fs.pathExists(path.join(baseBackupPath, req.params.date))
        if (pathExists) {
          await fs.remove(path.join(baseBackupPath, req.params.date))
          return res.status(200).json(`Manually removed backup for ${req.params.date}`)
        }
      } else {
        return res.status(400).json(`A backup deletion was attempted but no backup was specified`)
      }
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
    }
  })

module.exports = backupRoute
