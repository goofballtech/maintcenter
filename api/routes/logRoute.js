const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const logRoute = express.Router({ mergeParams: true })

const baseLogs = require('../models/logsModel')

logRoute.route('/getid').get(function(req, res) {
  baseLogs
    .findOne({})
    .sort({ entryNumber: -1 })
    .exec(function(err, highest) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      if (highest && highest.entryNumber) {
        res.status(200).json({
          message: `The next log number is ${highest.entryNumber + 1}`,
          nextNumber: highest.entryNumber + 1,
        })
      } else {
        res.status(200).json({
          message: `There are no logs stored yet`,
          nextNumber: 1,
        })
      }
    })
})

logRoute.route('/categories').get(async function(req, res) {
  const Logs = baseLogs.byTenant(req.params.db)
  try {
    let categories = await Logs.find({ tenantId: req.params.db }).distinct('category')
    if (categories.length) {
      categories.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    }
    res.status(200).json(categories)
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

logRoute
  .route('/:id?')
  .get(async function(req, res) {
    const Logs = baseLogs.byTenant(req.params.db)
    // specify the type required with a comma separated list
    const typesArray = req.query.types ? req.query.types.split(',') : []
    try {
      const systemLogs = typesArray.length
        ? await Logs.find({ type: { $in: typesArray } })
        : req.params.id
        ? await Logs.find({ entryNumber: req.params.id })
        : await Logs.find({})
      res.status(200).json(systemLogs)
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
  })
  .put(function(req, res) {
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      baseLogs.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }, function(err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully updated log entry`)
      })
    } else {
      res.status(200).json(`Log ID is not valid`)
    }
  })
  .post(function(req, res) {
    const newLog = new baseLogs(req.body)
    newLog.tenantId = req.params.db
    newLog
      .save()
      .then(() => {
        res.status(201).json(`Log entry ${newLog.entryNumber} saved to database by ${req.body.techName}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function(req, res) {
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      baseLogs.findByIdAndDelete(req.params.id, function(err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully deleted log entry`)
      })
    } else {
      res.status(200).json(`Log ID for attempted delete is not valid`)
    }
  })

module.exports = logRoute
