const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const todoRoute = express.Router({ mergeParams: true })

const baseTodos = require('../models/todosModel')

todoRoute.route('/groups').get(async function(req, res) {
  const Todos = baseTodos.byTenant(req.params.db)
  let groups = await Todos.find({ tenantId: req.params.db, active: { $eq: true } }).distinct('group')
  if (groups.length) {
    groups = groups.filter(group => group).sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    res.status(200).json(groups)
  } else {
    res.status(200).json([])
  }
})

// get tasks on a list or all tasks as a basic list
todoRoute.route('/list').get(function(req, res) {
  const Todos = baseTodos.byTenant(req.params.db)
  let todos = []
  let todoObj = []
  if (req.query.todos) {
    todos = req.query.todos.split(',')
    todoObj = todos.map(id => mongoose.Types.ObjectId(id))
  }
  if (todoObj.length) {
    Todos.find(
      {
        active: { $eq: true },
        _id: {
          $in: todoObj,
        },
      },
      function(err, todoList) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        let listToReturn = []
        todoList.forEach(todo => {
          listToReturn.push({
            value: todo._id,
            text: todo.name,
          })
        })
        res.status(200).json(listToReturn)
      }
    )
  } else {
    Todos.find(
      {
        active: { $eq: true },
      },
      function(err, todoList) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        let listToReturn = []
        todoList.forEach(todo => {
          listToReturn.push({
            value: todo._id,
            text: todo.name,
          })
        })
        res.status(200).json(listToReturn)
      }
    )
  }
})

todoRoute
  .route('/:id?')
  .get(async function(req, res) {
    const Todos = baseTodos.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      Todos.find({ _id: req.params.id }).exec(function(err, todo) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(todo)
      })
    } else {
      if(req.query.groups || req.query.priority){
        let findObject = { active: { $eq: true } }    
        
        let groups = []
        if (req.query.groups){
          groups = req.query.groups.split(',')
          if (groups.length){
            findObject.group = { $in : groups}
          }
        }

        if(req.query.priority !== '0'){
          findObject.priority = req.query.priority
        }

        Todos.find(findObject).exec(function(err, todo) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(todo)
        })
      } else if (req.params.id == 'past') {
        Todos.find({  }).exec(function(err, todo) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(todo)
        })
      } else {
        if (req.query.count) {
          try {
            const dueToDoCount = await Todos.countDocuments({
              active: { $eq: true },
              tenantId: { $eq: req.params.db },
              dueDate: { $lte: new Date() },
            })
            return res.status(200).json({ number: dueToDoCount })
          } catch (error) {
            logger.error(
              JSON.stringify({
                details: error,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
        } else {
          Todos.find({ active: { $eq: true } }).exec(function(err, todo) {
            if (err) {
              logger.error(
                JSON.stringify({
                  details: err,
                  url: req.originalUrl,
                  method: req.method,
                })
              )
              return res.status(400).json('An error has occurred, see log for details')
            }
            res.status(200).json(todo)
          })
        }
      }
    }
  })
  .put(function(req, res) {
    const Todos = baseTodos.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      Todos.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }, function(err, todo) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully updated ${todo.name}`)
      })
    } else {
      res.status(200).json(`To Do ID is not valid`)
    }
  })
  .post(async function(req, res) {
    const newTodo = new baseTodos(req.body)
    const toDoCount = await baseTodos.countDocuments({
      tenantId: { $eq: req.params.db },
    })
    newTodo.tenantId = req.params.db
    newTodo.idNumber = toDoCount + 1

    newTodo
      .save()
      .then(result => {
        res.status(200).json({
          object: result,
          message: `${newTodo.enteredBy} added new a To Do named ${newTodo.name}`,
        })
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function(req, res) {
    const Todos = baseTodos.byTenant(req.params.db)
    Todos.findByIdAndDelete(req.params.id, function(err, todo) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Successfully removed ${todo.name}`)
    })
  })

module.exports = todoRoute
