// create new Router instance for api routes
const router = require('express').Router()

router.use('/', require('./baseRoute'))

router.use('/admin', require('../admin'))

// router.use('/admin', ensureAuthenticated, require('./admin'))

router.use('/:db/inventory', require('./inventoryRoute'))
router.use('/:db/parts', require('./oiPartsRoute'))

router.use('/:db/log', require('./loggerRoute')) // for diagnostics logs
router.use('/:db/files', require('./filesRoute'))
router.use('/:db/mailer', require('./mailerRoute'))
router.use('/:db/excel', require('./excelRoute'))
router.use('/:db/backup', require('./backupRoute'))

router.use('/:db/settings', require('./settingsRoute'))

router.use('/:db/meters', require('./meterRoute'))
router.use('/:db/taskdata', require('./taskRoute'))
router.use('/:db/assets', require('./assetRoute'))

router.use('/:db/systemlogs', require('./logRoute'))
router.use('/:db/packages', require('./packageRoute'))
router.use('/:db/todo', require('./todoRoute'))
router.use('/:db/toorder', require('./toOrderRoute'))
router.use('/:db/hoses', require('./hoseRoute'))

module.exports = router
