const express = require('express')
const mailer = require('../../server/nodemailer')
const logger = require('../../server/logger')

const mailerRoute = express.Router({ mergeParams: true })

//? remove the bang from below in order to force email sending to entire list while in dev mode
const dev = !(process.env.NODE_ENV === 'production')

mailerRoute.route('/').post(async function(req, res) {
  try {
    let toSend = req.body

    if (dev) {
      toSend.tempTo = toSend.to
      toSend.tempCC = toSend.cc
      toSend.tempBCC = toSend.bcc
      toSend.to = ['goofballtech@gmail.com'] // address to send dev mode tests
      toSend.cc = []
      toSend.bcc = []
    }

    !toSend.text && !toSend.html ? (toSend.text = 'No Data was added to the email body') : null
    toSend.from = 'Maintenance Center <maintcenter@gmail.com>'

    let info = await mailer.sendMail(toSend)

    dev
      ? res.status(200).json({
          devRes: `Emails were neutered by dev mode. Addresses attempting to be sent were.
            to: ${toSend.tempTo}, 
            cc: ${toSend.tempCC}, 
            bcc: ${toSend.tempBCC}`,
        })
      : res.status(200).json(info)
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

//https://nodemailer.com/about/

module.exports = mailerRoute
