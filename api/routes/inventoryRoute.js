const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const inventoryRoute = express.Router({ mergeParams: true })

const basePartLocation = require('../models/partLocationsModel')
const baseInventoryItem = require('../models/inventoryPartsModel')
const OiVendors = require('../models/oiVendorModel')
const ObjectId = mongoose.Types.ObjectId

import { getAllVendors, removeDuplicates } from '../utils/helpers'

// /** # Inventory API routes */

inventoryRoute
  .route('/search/')
  .get(async function (req, res) {
    const inventory = baseInventoryItem.byTenant(req.params.db)
    const locations = basePartLocation.byTenant(req.params.db)

    let search = req.query
    let resultArray = []

    try {
      if (search.all) {
        res.status(200).json(await inventory.aggregate([
          {
            $match: {
              active: true
            }
          },
          {
            $addFields: {
              totalOnHand: { $sum: "$locations.qty" }
            }
          }
        ]))
      } else {
        if (search.category) {
          search.category = search.category.split(',')
          const itemsFromCat = await inventory.aggregate([
            {
              $match: {
                category: { $in: search.category },
                active: true
              }
            },
            {
              $addFields: {
                totalOnHand: { $sum: "$locations.qty" }
              }
            }
          ])
          itemsFromCat.forEach(item => resultArray.push(item))
        }
        if (search.vendor) {
          search.vendor = search.vendor.split(',')
          const itemsFromVendor = await inventory.aggregate([
            {
              $match: {
                'partNumber.vendor': { $in: search.vendor },
                active: true
              }
            },
            {
              $addFields: {
                totalOnHand: { $sum: "$locations.qty" }
              }
            }
          ])
          itemsFromVendor.forEach(item => resultArray.push(item))
        }
        if (search.location) {
          search.location = search.location.split(',')
          let withChildren = await Promise.all(search.location.map((location) => {
            return getAllDescendants(location, locations)
          }))
          let justTheChildren = []
          withChildren.forEach(e => {
            if (e.length) {
              justTheChildren.push(...e[0].descendants)
            }
          })
          justTheChildren = [...search.location, ...justTheChildren.map(i => i._id)]
          let asSet = new Set(justTheChildren)
          let uniqueChildren = Array.from(asSet)
          const itemsFromLocation = await inventory.aggregate([
            {
              $match: {
                'locations.location': { $in: uniqueChildren },
                active: true
              }
            },
            {
              $addFields: {
                totalOnHand: { $sum: "$locations.qty" }
              }
            }
          ])
          itemsFromLocation.forEach(item => resultArray.push(item))
        }
        if (search.name) {
          const itemsFromName = await inventory.aggregate([
            {
              $match: {
                name: {
                  $regex: `.*${search.name}.*`,
                  $options: 'i',
                },
                active: true
              }
            },
            {
              $addFields: {
                totalOnHand: { $sum: "$locations.qty" }
              }
            }
          ])
          itemsFromName.forEach(item => resultArray.push(item))
        }
        if (search.partNumber) {
          const itemsFromPartNumber = await inventory.aggregate([
            {
              $match: {
                'partNumber.partNumber': {
                  $regex: `.*${search.partNumber}.*`,
                  $options: 'i',
                },
                active: true
              }
            },
            {
              $addFields: {
                totalOnHand: { $sum: "$locations.qty" }
              }
            }
          ])
          itemsFromPartNumber.forEach(item => resultArray.push(item))
        }
        if (search.desc) {
          const itemsFromDesc = await inventory.aggregate([
            {
              $match: {
                description: {
                  $regex: `.*${search.desc}.*`,
                  $options: 'i',
                },
                active: true
              }
            },
            {
              $addFields: {
                totalOnHand: { $sum: "$locations.qty" }
              }
            }
          ])
          itemsFromDesc.forEach(item => resultArray.push(item))
        }
        if (resultArray.length) {
          let queryResults = removeDuplicates(resultArray, '_id')
          res.status(200).json(queryResults)
        } else {
          res.status(200).json([])
        }
      }
    }
    catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
  })


inventoryRoute
  .route('/locations/:type/:id?')
  .get(async function (req, res) {
    try {
      const locations = basePartLocation.byTenant(req.params.db)
      let searchResult
      if (mongoose.Types.ObjectId.isValid(req.params.id) && req.params.type === 'parents') {
        res.status(200).json(await addParentsArray(req.params.id, locations))
      } else if (mongoose.Types.ObjectId.isValid(req.params.id) && req.params.type === 'children') {
        //? Get all the child locations of the specified id (everything that also has it listed as their "parent")
        let location = req.params.id
        searchResult = await locations.find({ parent: ObjectId(location) })
        searchResult = searchResult.sort((a, b) => a.name.localeCompare(b.name, undefined, { sensitivity: 'base', numeric: true }))
        res.status(200).json(searchResult)
      } else if (mongoose.Types.ObjectId.isValid(req.params.id) && req.params.type === 'allchildren') {
        //? Get all the descendants of the given parent id as an array on the object
        searchResult = await getAllDescendants(req.params.id, locations)
        res.status(200).json(searchResult)
      } else if (mongoose.Types.ObjectId.isValid(req.params.id) && req.params.type === 'itemcount') {
        const inventory = baseInventoryItem.byTenant(req.params.db)
        let childrenTree = await getAllDescendants(req.params.id, locations)
        if (childrenTree.length) {
          childrenTree = [childrenTree[0]._id, ...childrenTree[0].descendants.map(e => (e._id))]
        } else {
          childrenTree = [ObjectId(req.params.id)]
        }
        let results = await inventory.find(
          {
            'locations.location': { $in: childrenTree },
            active: true
          }
        )
        res.status(200).json({ itemsAtLocation: results.length })
      } else if (req.params.type === 'root') {
        searchResult = await locations.find({ parent: null })
        res.status(200).json(searchResult)
      } else if (req.params.type === 'list') {
        let fullList = await locations.find({}).lean()
        let rootItem = await locations.findOne({ parent: null }).lean()
        let allItemsWithParents = await Promise.allSettled(fullList.map(async location => {
          return await addParentsArray(location._id, locations)
        }))
        let list = allItemsWithParents.map(item => {
          if (item.value.length) {
            let treeList = item.value[0].tree.map(l => l.name)
            return {
              text: `${treeList.join('>')}>${item.value[0].name}`,
              value: item.value[0]._id,
            }
          }
        })
        list = list.filter(i => i != null)
        list.splice(0, 0, { text: rootItem.name, value: rootItem._id })
        res.status(200).json(list.sort((a, b) => a.text.localeCompare(b.text, undefined, { sensitivity: 'base' })))
      } else {
        res.status(200).json('No results to return')
      }

    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })
  .put(async function (req, res) {
    try {
      //? locations/update/:id
      const locations = basePartLocation.byTenant(req.params.db)
      if (mongoose.Types.ObjectId.isValid(req.params.id)) {
        let locationUpdate = await locations.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true })
        res.status(200).json(await addParentsArray(locationUpdate._id, locations))
      } else {
        res.status(200).json(`Location is not valid`)
      }
    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })
  .post(async function (req, res) {
    try {
      //? locations/new/:id
      let newLocation = await new basePartLocation(req.body)
      newLocation.tenantId = req.params.db
      let afterCreation = await newLocation.save(newLocation)
      res.status(200).json(await addParentsArray(afterCreation._id, basePartLocation))
    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })
  .delete(async function (req, res) {
    try {
      //? locations/delete/:id
      let locations = basePartLocation.byTenant(req.params.db)
      let searchResult = await getAllDescendants(req.params.id, locations)
      let deleteArray
      if (searchResult.length) {
        deleteArray = [searchResult[0]._id, ...searchResult[0].descendants.map(e => (e._id))]
      } else {
        deleteArray = [ObjectId(req.params.id)]
      }
      let results = await locations.deleteMany({ _id: { $in: deleteArray } })
      res.status(200).json(results)
    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })

inventoryRoute
  .route('/lists/:type')
  .get(async function (req, res) {
    if (req.params.type === 'vendors') {
      let fromOrders = await getAllVendors(req.params.db)
      res.status(200).json(fromOrders)
    } else if (req.params.type === 'categories') {
      const inventory = baseInventoryItem.byTenant(req.params.db)
      let categoryList = await inventory.find({ tenantId: req.params.db, active: { $eq: true } }).distinct('category')
      // case insensitive sorting
      categoryList.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
      res.status(200).json(categoryList)
    } else if (req.params.type === 'uom') {
      const inventory = baseInventoryItem.byTenant(req.params.db)
      let uomList = await inventory.find({ tenantId: req.params.db, active: { $eq: true } }).distinct('uom')
      // case insensitive sorting
      uomList.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
      res.status(200).json(uomList)
    }
  })

inventoryRoute
  .route('/items/:type/:id?')
  .get(async function (req, res) {
    const inventory = baseInventoryItem.byTenant(req.params.db)
    if (req.params.type === 'duplicates') {
      let item = await inventory.find({ active: true, 'partNumber.partNumber': req.query.check }).lean()
      res.status(200).json(item)
    } else if (req.params.id && req.params.type === 'single' && mongoose.Types.ObjectId.isValid(req.params.id)) {
      const locations = basePartLocation.byTenant(req.params.db)
      let item = await inventory.findById(req.params.id).lean()

      if (item && item.partNumber.length) {
        let partNumbers = item.partNumber.map(p => p.partNumber)
        let vendorMatches = await OiVendors.find({
          $or: [
            { OINumber: { $in: partNumbers } },
            { SupplierNumber: { $in: partNumbers } },
          ],
        }).lean()
        item.vendorMatches = vendorMatches
      }

      if (item && item.locations && item.locations.length) {
        item.locations = await Promise.all(item.locations.map(async location => {
          let temp = await addParentsArray(location.location, locations)
          if (temp.length) {
            location.locationName = temp[0].name
            location.locationTree = temp[0].tree
          } else {
            let locObject = await locations.findById(location.location).lean()
            location.locationName = locObject.name
            location.locationTree = []
          }
          return location
        }))
      }
      res.status(200).json(item)
    } else if (req.params.id && req.params.type === 'location' && mongoose.Types.ObjectId.isValid(req.params.id)) {
      const locations = basePartLocation.byTenant(req.params.db)
      let childrenTree = await getAllDescendants(req.params.id, locations)
      if (childrenTree.length) {
        childrenTree = [childrenTree[0]._id, ...childrenTree[0].descendants.map(e => (e._id))]
      } else {
        childrenTree = [ObjectId(req.params.id)]
      }
      let results = await inventory.aggregate([
        {
          $match: {
            'locations.location': { $in: childrenTree },
            active: true
          }
        },
        {
          $addFields: {
            totalOnHand: { $sum: "$locations.qty" }
          }
        }
      ])
      res.status(200).json(results)
    } else if (req.params.type === 'all') {
      let items = await inventory.aggregate([
        {
          $match: {
            active: true
          }
        },
        {
          $addFields: {
            totalOnHand: { $sum: "$locations.qty" }
          }
        }
      ])
      res.status(200).json(items)
    } else if (req.params.type === 'inactive') {
      let items = await inventory.aggregate([
        {
          $match: {
            active: false
          }
        },
        {
          $addFields: {
            totalOnHand: { $sum: "$locations.qty" }
          }
        }
      ])
      res.status(200).json(items)
    } else {
      res.status(200).json('Invalid search params, no results found')
    }
  })
  .put(async function (req, res) {
    //items/update/:id
    try {
      let updated = await baseInventoryItem.findByIdAndUpdate(req.params.id, req.body, { runValidators: true, new: true })
      res.status(200).json(updated)
    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })
  .post(async function (req, res) {
    try {
      let newItem = await new baseInventoryItem(req.body)
      newItem.tenantId = req.params.db
      let afterCreation = await newItem.save(newItem)
      res.status(200).json(afterCreation)
    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  })

async function addParentsArray(id, locations) {
  //? returns the provided location with a "tree" array of the parent locations in order
  let searchResult = await locations.aggregate([
    { "$match": { _id: ObjectId(id) } },
    {
      "$graphLookup": {
        from: "partlocations",
        startWith: "$parent",
        connectFromField: "parent",
        connectToField: "_id",
        as: "tree",
        depthField: "order",
      }
    },
    { "$unwind": "$tree" },
    { "$sort": { "tree.order": -1 } },
    {
      "$group": {
        _id: "$_id",
        "name": { "$first": "$name" },
        "parent": { "$first": "$parent" },
        "tree": {
          "$push": {
            _id: "$tree._id",
            "name": "$tree.name",
            "parent": "$tree.parent"
          }
        }
      }
    }
  ])
  return searchResult
}

async function getAllDescendants(id, locations) {
  let searchResult = await locations.aggregate([
    { "$match": { _id: ObjectId(id) } },
    {
      "$graphLookup": {
        from: "partlocations",
        startWith: "$_id",
        connectFromField: "_id",
        connectToField: "parent",
        as: "descendants",
        depthField: "order",
      }
    },
    { "$unwind": "$descendants" },
    { "$sort": { "descendants.order": -1 } },
    {
      "$group": {
        _id: "$_id",
        "name": { "$first": "$name" },
        "parent": { "$first": "$parent" },
        "descendants": {
          "$push": {
            _id: "$descendants._id",
            "name": "$descendants.name",
            "parent": "$descendants.parent"
          }
        }
      }
    }
  ])
  return searchResult
}

module.exports = inventoryRoute
