const express = require('express')
const logger = require('../../server/logger')

const meterRoute = express.Router({ mergeParams: true })

const baseMeters = require('../models/metersModel')

meterRoute.route('/list').get(async function(req, res) {
  const Meters = baseMeters.byTenant(req.params.db)
  let meters = await Meters.find()
  res.status(200).json(
    meters.map(meter => {
      return {
        text: meter.name,
        value: meter.name,
      }
    })
  )
})

meterRoute.route('/types').get(function(req, res) {
  const Meters = baseMeters.byTenant(req.params.db)
  Meters.find()
    .distinct('type')
    .exec(function(err, types) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      // case insensitive sorting
      types.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
      res.status(200).json(types)
    })
})

// get all the meter objects
meterRoute
  .route('/:id?')
  .get(function(req, res) {
    const Meters = baseMeters.byTenant(req.params.db)
    if (req.params.id) {
      Meters.find({ name: req.params.id }).exec(function(err, singleMeter) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(singleMeter)
      })
    } else {
      Meters.find().exec(function(err, meters) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(meters)
      })
    }
  })
  .put(function(req, res) {
    const Meters = baseMeters.byTenant(req.params.db)
    if (req.body.currentReading == req.body.previousReading) {
      res.status(200).json(`the new value and the previous value seem to be the same`)
    } else {
      const prevObject = {
        techName: req.body.techName,
        reading: req.body.previousReading,
      }
      const updateAndAddPrev = {
        $set: { currentReading: req.body.currentReading },
        $push: { pastReadings: prevObject },
      }
      Meters.findByIdAndUpdate(req.params.id, updateAndAddPrev, { runValidators: true }, function(err, meter) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully updated reading on ${meter.name}`)
      })
    }
  })
  .post(function(req, res) {
    const newMeter = new baseMeters(req.body)
    newMeter.tenantId = req.params.db
    newMeter
      .save()
      .then(() => {
        res.status(200).json(`Added new meter named ${newMeter.name}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        console.log(err)
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function(req, res) {
    const Meters = baseMeters.byTenant(req.params.db)
    Meters.findByIdAndDelete(req.params.id, function(err, meter) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Successfully removed ${meter.name}`)
    })
  })

module.exports = meterRoute
