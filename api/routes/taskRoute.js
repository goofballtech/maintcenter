const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const taskRoute = express.Router({ mergeParams: true })
const baseTask = require('../models/tasksModel')
const basePrev = require('../models/previousCompletionsModel')

import { removeDuplicates, offsetDateByTimezone } from '../utils/helpers'
import {
  addDays,
  parseISO,
  isAfter,
  startOfDay,
} from 'date-fns/esm'

import { searchCompletions } from '../utils/taskHelpers'

taskRoute.route('/mode/').put(async function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  const newMode = req.query.updatedMode || 'Transit'
  const lastMode = req.query.lastMode || 'none'
  const TZ = req.query.timezone || 0

  let bulkUpdateArray = []
  const modeCategories = ['In Layup', 'Out Of Layup', 'Mobilization', 'De-Mobilization']

  try {
    let tasksToUpdate
    if(newMode.includes('custom')){
      tasksToUpdate = await Task.find({ active: { $eq: true }})
    }else{
      tasksToUpdate = await Task.find({ active: { $eq: true }, category: { $nin: [newMode] } })
    }
    if (tasksToUpdate.length) {
      tasksToUpdate.forEach(task => {
        const categoryDiff = task.category.filter(x => !modeCategories.includes(x))
        if ((newMode == 'Operations' && categoryDiff.length) || newMode.includes('custom') ) {
          if (!task.deferred) {
            task.nextDue = addDays(task.lastCompletion, task.opsFrequency)
          }
        } else if (newMode == 'Transit' && categoryDiff.length) {
          // into transit mode from anywhere, set only Meter tasks to due
          if (lastMode == 'Operations') {
            if (task.category.includes('Meter')) {
              task.nextDue = new Date()
            } else {
              task.nextDue = addDays(task.lastCompletion, task.transitFrequency)
            }
          } else {
            const nextDueCalculated = addDays(task.lastCompletion, task.transitFrequency)
            if (isAfter(new Date(), nextDueCalculated) && task.layupTransition > 0 && task.transitFrequency <= 30) {
              const transVal = task.layupTransition / 100
              task.nextDue = addDays(new Date(), task.transitFrequency * transVal)
              task.deferred = true
            } else {
              task.nextDue = nextDueCalculated
              task.deferred = false
            }
          }
        } else {
          task.nextDue = parseISO('1970-01-01')
        }
        if (!task.deferred) task.deferred = false // in case it's null or undefined, make it false by default
        const taskItemsToUpdate = {
          updateOne: {
            filter: { _id: task._id },
            update: {
              $set: { nextDue: task.nextDue, deferred: task.deferred },
            },
          },
        }
        bulkUpdateArray.push(taskItemsToUpdate)
      })
    }

    if (bulkUpdateArray.length) {
      const singleTasksUpdate = await Task.bulkWrite(bulkUpdateArray, {
        ordered: false,
      })
      const currentUpdate = await Task.updateMany(
        { category: { $in: [newMode] } },
        { $set: { nextDue: offsetDateByTimezone(new Date(), TZ) } }
      )

      if (singleTasksUpdate.ok && currentUpdate.ok) {
        res.status(200).json(`Successfully updated mode`)
      } else {
        logger.error(
          JSON.stringify({
            details: {
              singleTasksUpdate,
              currentUpdate,
            },
            url: req.originalUrl,
            method: req.method,
          })
        )
        res.status(200).json(`Something might have gone wrong, see log for details`)
      }
    } else {
      res.status(200).json(`Successfully updated mode even though there were no tasks to update`)
    }
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    res.status(200).json(`Something might have gone wrong, see log for details`)
  }
})

// get due tasks
taskRoute.route('/due/:days?').get(async function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  const days = req.params.days || 1

  let date = new Date()
  const timezoneOffset = Math.floor(req.query.timezone) || 0

  date = offsetDateByTimezone(date, timezoneOffset)

  try {
    let dueByDate = await Task.find({
      $or: [
        {
          active: { $eq: true },
          nextDue: {
            $lte: startOfDay(addDays(date, days)),
            $gt: parseISO('1970-01-05'),
          },
        },
        {
          active: { $eq: true },
          useForcedDue: { $eq: true },
          forcedDueDate: {
            $lte: startOfDay(addDays(date, days)),
            $gt: parseISO('1970-01-05'),
          },
        },
      ],
    }).sort({customTaskID:1})

    req.query.count ? res.status(200).json({ number: dueByDate.length }) : res.status(200).json(dueByDate)
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

// get tasks on a list or all tasks as a basic list
taskRoute.route('/list').get(function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  let tasks = []
  if (req.query.tasks) {
    tasks = req.query.tasks.split(',')
  }
  if (tasks.length) {
    Task.find(
      {
        active: { $eq: true },
        taskID: {
          $in: tasks,
        },
      },
      function (err, taskList) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(buildList(taskList))
      }
    )
  } else {
    Task.find(
      {
        active: { $eq: true },
      },
      function (err, taskList) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }

        res.status(200).json(buildList(taskList))
      }
    )
  }
})

function buildList(taskList) {
  let listToReturn = []
  taskList.forEach(task => {
    listToReturn.push({
      value: task.taskID,
      text: `${task.customTaskID?task.customTaskID:task.taskID}: ${task.taskName} (${task.category.join(', ')})`,
    })
  })
  return listToReturn
}

//get related tasks
taskRoute.route('/related/:id?').get(async function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  if (req.params.id && req.query.list && req.query.split) {
    let relatedToMe = await Task.find({
      relatedTasks: { $in: [parseInt(req.params.id)] },
    })
    relatedToMe.length ? (relatedToMe = buildList(relatedToMe).sort((a, b) => (a.text < b.text ? -1 : 1))) : null
    const myTask = await Task.findOne({ taskID: req.params.id })
    let tasksImRelatedTo = await Task.find({
      taskID: { $in: myTask.relatedTasks },
    })
    tasksImRelatedTo.length
      ? (tasksImRelatedTo = buildList(tasksImRelatedTo).sort((a, b) => (a.text < b.text ? -1 : 1)))
      : null
    if (relatedToMe.length || tasksImRelatedTo.length) {
      res.status(200).json({
        relatedToMe: relatedToMe.length ? relatedToMe : [],
        tasksImRelatedTo: tasksImRelatedTo.length ? tasksImRelatedTo : [],
      })
    } else {
      res.status(200).json({
        relatedToMe: [],
        tasksImRelatedTo: [],
      })
    }
  } else if (req.params.id && req.query.list) {
    const relatedToMe = await Task.find({
      relatedTasks: { $in: [parseInt(req.params.id)] },
    })
    const myTask = await Task.findOne({ taskID: req.params.id })
    const tasksImRelatedTo = await Task.find({
      taskID: { $in: myTask.relatedTasks },
    })
    if (relatedToMe.length || tasksImRelatedTo.length) {
      let listOfEntries = buildList([...relatedToMe, ...tasksImRelatedTo])
      const finalList = removeDuplicates(listOfEntries, 'value')
      res.status(200).json(finalList.sort((a, b) => (a.text < b.text ? -1 : 1)))
    } else {
      res.status(200).json([])
    }
  } else if (req.params.id) {
    const relatedToMe = await Task.find({ relatedTasks: { $in: [parseInt(req.params.id)] } })
    const myTask = await Task.findOne({ taskID: req.params.id })
    const compiledSet = new Set([...relatedToMe.map(task => task.taskID), ...myTask.relatedTasks])
    res.status(200).json([...compiledSet].sort())
  } else {
    res.status(400).json('No task was specified for comparison')
  }
})

// get all active task categories direct from saved tasks
taskRoute.route('/categories').get(async function (req, res) {
  const Task = baseTask.byTenant(req.params.db) // For some reason the tenant model was not limiting results as expect, work around added to find()
  try {
    if (req.query.count) {
      let categoryList = await Task.getCategoryCounts()
      res.status(200).json(categoryList)
    } else {
      let categories = await Task.find({ tenantId: req.params.db, active: { $eq: true } }).distinct('category')

      for (let i = 0; i < categories.length; i++) {
        // cycle through data to find index of 'Meter' and remove it
        if (categories[i] == 'Meter') {
          categories.splice(i, 1)
          break
        }
      }

      if (!categories.includes('In Layup')) {
        categories.push('In Layup')
      }

      if (!categories.includes('Out Of Layup')) {
        categories.push('Out Of Layup')
      }

      if (!categories.includes('Mobilization')) {
        categories.push('Mobilization')
      }

      if (!categories.includes('De-Mobilization')) {
        categories.push('De-Mobilization')
      }

      // case insensitive sorting
      categories.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
      res.status(200).json(categories)
    }
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

// get all inactive tasks
taskRoute.route('/inactive').get(function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  Task.find({ active: { $eq: false } }).exec(function (err, tasks) {
    if (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
    res.status(200).json(tasks)
  })
})

// add new task to database
taskRoute.route('/add').post(function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  const newTask = new baseTask(req.body)
  if (newTask.taskID == undefined || newTask.taskID == '') {
    Task.findOne({})
      .sort({ taskID: -1 })
      .exec(function (err, highest) {
        if (err) {
          res.status(400).json(`Error finding task ${err}`)
        }
        if (!highest || !highest.taskID) {
          newTask.taskID = 3000
        } else {
          newTask.taskID = highest.taskID + 1
        }
        newTask.tenantId = req.params.db
        newTask.category.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
        newTask
          .save()
          .then(newTask => {
            res
              .status(201)
              .json({ reply: `Task ${newTask.taskID} saved to database by ${req.body.techName}`, newTask: newTask })
          })
          .catch(err => {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          })
      })
  } else {
    newTask.tenantId = req.params.db
    newTask.category.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    newTask
      .save()
      .then(() => {
        res.status(201).json(`Task ${newTask.taskID} saved to database by ${req.body.techName}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  }
})

// previous completions
taskRoute
  .route('/completions/:taskid?')
  .get(async function (req, res) {
    const Prev = basePrev.byTenant(req.params.db)
    if (req.params.taskid) {
      Prev.find({ taskID: req.params.taskid })
        .sort({ dateCompleted: -1 })
        .exec(function (err, completions) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(completions)
        })
    } else if (req.query.prevTable) {
      Prev.aggregate([{ $sort: { createdAt: -1 } }, { $limit: 200 }], function (err, completions) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(completions)
      })
    } else {
      try {
        let prevList
        if (req.query.includeForms) {
          prevList = await searchCompletions(req.params.db, req.query, req.params.db, true)
        } else {
          prevList = await searchCompletions(req.params.db, req.query, req.params.db)
        }

        prevList.length?res.status(200).json(prevList):res.status(200).json([])
      } catch (error) {
        logger.error(
          JSON.stringify({
            details: error,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
    }
  })
  .post(function (req, res) {
    const newPrev = new basePrev(req.body)
    newPrev.tenantId = req.params.db
    newPrev
      .save()
      .then(() => {
        res.status(201).json(`Task ${req.body.customTaskID?req.body.customTaskID:req.body.taskID} completion saved to database by ${req.body.techName}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })

taskRoute
  .route('/formdata/:taskid')
  .get(async function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.taskid)) {
      let task = await Task.findById(req.params.taskid)
      if (Object.keys(task).length) {
        if (
          task.requiresAdditionalInfoSubmissions &&
          task.requiresAdditionalInfoSubmissions.formSubmissions &&
          task.requiresAdditionalInfoSubmissions.formSubmissions.length
        ) {
          return res.status(200).json(task.requiresAdditionalInfoSubmissions.formSubmissions)
        } else {
          return res.status(200).json([])
        }
      }
    }
    return res.status(200).json('Missing task ID')
  })
  .post(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    const submissionData = req.body
    submissionData[submissionData.length - 1].createdAt = new Date()
    Task.findByIdAndUpdate(
      req.params.taskid,
      {
        $push: {
          'requiresAdditionalInfoSubmissions.formSubmissions': [submissionData],
        },
      },
      function (err, task) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res
          .status(201)
          .json(`Form data added to task ${task.customTaskID?task.customTaskID:task.taskID} by ${submissionData[submissionData.length - 1].techName}`)
      }
    )
  })

taskRoute
  .route('/customid/')
  // check if a customID is already used
  .get(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    Task.find({ customTaskID: req.query.id }).exec(function (err, results) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        if (results.length) {
          res.status(200).json('exists')
        } else {
          res.status(200).json('free')
        }
    })
  })

// operations for task notes
taskRoute
  .route('/notes/:taskid/:noteid?')
  // get the notes for a taskid
  .get(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    Task.findById(req.params.taskid, function (err, task) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(task.notes)
    })
  })
  // post a new note into taskid
  .post(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    const note = req.body
    // push the comment to the notes subdocument
    Task.findByIdAndUpdate(req.params.taskid, { $push: { notes: note } }, function (err, task) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(201).json(`Note added to task ${task.customTaskID?task.customTaskID:task.taskID} by ${note.techName}`)
    })
  })
  // delete a note
  .delete(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    const note = req.body
    // pull the comment from the notes subdocument
    Task.findByIdAndUpdate(req.params.taskid, { $pull: { notes: { _id: req.params.noteid } } }, function (err, task) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Note deleted from task ${task.customTaskID?task.customTaskID:task.taskID} by ${note.techName}`)
    })
  })

// operations for single tasks data
taskRoute
  .route('/:id')
  // grab an individual task
  .get(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      Task.findById(req.params.id, function (err, task) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(task)
      })
    } else {
      Task.find({ taskID: req.params.id }, function (err, task) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(task)
      })
    }
  })
  //  update task data
  .put(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    const updated = req.body
    if (updated.category) {
      updated.category.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    }
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      Task.findByIdAndUpdate(req.params.id, updated, { runValidators: true, new: true }, function (err, updatedTask) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`${req.body.techName} has successfully updated Task #${updatedTask.customTaskID?updatedTask.customTaskID:updatedTask.taskID}`)
      })
    } else {
      Task.findOneAndUpdate({ taskID: req.params.id }, updated, { runValidators: true, new: true }, function (
        err,
        updatedTask
      ) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`${req.body.techName} has successfully updated Task #${updatedTask.customTaskID?updatedTask.customTaskID:updatedTask.taskID}`)
      })
    }
  })
  // delete by ID
  .delete(function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    Task.findByIdAndDelete(req.params.id, function (err, task) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`${req.body.techName} has successfully removed Task #${task.customTaskID?task.customTaskID:task.taskID}`)
    })
  })

// get all active tasks
taskRoute.route('/').get(async function (req, res) {
  const Task = baseTask.byTenant(req.params.db)
  let resultArray = []
  if (Object.keys(req.query).length > 0) {
    if (req.query.export) {
      delete req.query.export
      try {
        if (req.query.category) {
          req.query.category = req.query.category.split(',')
          const tasksFromCat = await Task.find({
            category: { $in: req.query.category },
          })
          tasksFromCat.forEach(item => resultArray.push(item))
          delete req.query.category
        }
        if (req.query.taskName) {
          const tasksFromName = await Task.find({
            taskName: {
              $regex: `.*${req.query.taskName}.*`,
              $options: 'i',
            },
          })
          tasksFromName.forEach(item => resultArray.push(item))
          delete req.query.taskName
        }
        if (req.query.detailedDescription) {
          const tasksFromDescription = await Task.find({
            detailedDescription: {
              $regex: `.*${req.query.detailedDescription}.*`,
              $options: 'i',
            },
          })
          tasksFromDescription.forEach(item => resultArray.push(item))
          delete req.query.detailedDescription
        }

        if (Object.keys(req.query).length > 0) {
          let tempObject = JSON.parse(JSON.stringify(req.query))
          if (Object.keys(tempObject).length > 1 && Object.keys(tempObject).includes('active')) {
            if (tempObject.active) {
              delete tempObject.active
            }
          }
          const theRest = await Task.find(tempObject)
          theRest.forEach(item => resultArray.push(item))
        }

        if (req.query.active && req.query.active == 'true') {
          resultArray = resultArray.filter(task => task.active)
        }

        if (resultArray.length) {
          let queryResults = new Set(resultArray)
          res.status(200).json([...queryResults])
        } else {
          res.status(200).json([])
        }
      } catch (error) {
        logger.error(
          JSON.stringify({
            details: error,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
    } else {
      Task.find(req.query).exec(function (err, tasks) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(tasks)
      })
    }
  } else {
    Task.find({ active: { $eq: true } }).exec(function (err, tasks) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(tasks)
    })
  }
})

module.exports = taskRoute
