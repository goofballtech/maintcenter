const express = require('express')
// const mongoose = require('mongoose')
const logger = require('../../server/logger')
const fs = require('fs-extra')
const path = require('path')

const oiPartsRoute = express.Router({ mergeParams: true })

const OiParts = require('../models/oiPartsModel')
const OiVendors = require('../models/oiVendorModel')

const dev = !(process.env.NODE_ENV === 'production')
const imageFolder = path.join(process.cwd(), 'supportDocs', 'ExternalUpdate', 'Pictures')

import { base64_encode } from '../utils/helpers'

if (!dev && process.env.NODE_APP_INSTANCE === '0') {
  updateOiNumbers()
  importVendorNumbers()
}


oiPartsRoute.route('/forceupdate').get(async function(req, res) {
  let oiStatus = await updateOiNumbers()
  let vendorStatus = await importVendorNumbers()
  res.status(200).json(
    {
      oi: oiStatus,
      vendors: vendorStatus,
    }
  )  
})

oiPartsRoute.route('/oiNumber/:number').get(async function(req, res) {
  let result = await OiParts.findOne({OINumber:req.params.number}).lean()
  let vendors = await OiVendors.find({OINumber: req.params.number}).lean()
  if(result && vendors.length){
    result.vendorNumbers = vendors
  }
  let encoded = ''
  if(result !== null && result.OINumber){
    const imageLocation = path.join(imageFolder, `${result.OINumber}.jpg`)
    const imageLocation2 = path.join(imageFolder, `${result.OINumber}.JPG`)
    if (fs.pathExistsSync(imageLocation)) {
      encoded = base64_encode(imageLocation)
      result.image = encoded
    } else if (fs.pathExistsSync(imageLocation2)) {
      encoded = base64_encode(imageLocation2)
      result.image = encoded
    }
  }
  res.status(200).json(result)
})

oiPartsRoute.route('/list').get(async function(req, res) {
  let searchString

  if (req.query.search) {
    let array = req.query.search
      .split(/([^\s"]+|"[^"]*")+/g)
      .filter(string => string && string !== ' ')
      .map(string => string.replace(/"/g, ''))
      .map(string => `.*${string}.*`)

    searchString = array.join('|')
  }

  const searchObj = { $regex: searchString, $options: 'i' }

    let vendors = await OiVendors.find(
      { 
        $or: [
          { OINumber: searchObj },
          { Supplier: searchObj },
          { SupplierNumber: searchObj },
        ], 
      }
    ).lean()

    let parts = await OiParts.find(
      { 
        BU : 'BUMCO',
        $or: [
          { OINumber: searchObj },
          { Description: searchObj },
          { Manufacturer: searchObj },
          { ManufacturerPartNumber: searchObj },
        ], 
      }
    ).lean()
  
  parts = sortAndAddImages(parts)
  vendors = sortAndAddImages(vendors) 

  const results = {
    parts: parts,
    vendors: vendors,
  }

  res.status(200).json(results)
})

function sortAndAddImages(list){
  return list
    .sort((a, b) => a.OINumber.localeCompare(b.OINumber, undefined, { sensitivity: 'base' }))
    .map(item => {
      let encoded = ''
      const imageLocation = path.join(imageFolder, `${item.OINumber}.jpg`)
      const imageLocation2 = path.join(imageFolder, `${item.OINumber}.JPG`)
      if (fs.pathExistsSync(imageLocation)) {
        encoded = base64_encode(imageLocation)
      } else if (fs.pathExistsSync(imageLocation2)) {
        encoded = base64_encode(imageLocation2)
      }
      if (encoded) {
        item.image = encoded
        return item
      }else{
        return item
      }
    })
}

async function updateOiNumbers() {
  //? For the import, grab an export of items from peoplesoft financials
  //? Use query Reporting Tools > Query > Query Viewer >> OI_IN_ACTIVE_BUMCO_ITEM_INFO then fill in date and BU as BUMCO
  //? Verify the columns are names as referenced below, they have changed in the past. 
  //? Make into JSON at http://beautifytools.com/excel-to-json-converter.php
  //? remove the root object and make it just an array of objects
  //? put the resulting array of objects in as api > objects > OiPartNumbersUpdate.json

  try{
    fs.ensureDirSync(imageFolder, { mode: 0o2775 })

    let partsCount = await OiParts.find({}).lean()

    if(partsCount.length){
      const updates = []
      let importArray = []
      importArray = fs.readJsonSync(path.join(process.cwd(),'api', 'objects', 'OiPartNumbersUpdate.json'), {throws:false})

      if(importArray && importArray.length){

        let deleted = await OiParts.find({ OINumber: { $nin: importArray.map(i => i.Item) } })
        deleted = deleted.map(i => i.OINumber)
        deleted = await OiParts.find({ OINumber: { $in: deleted } })
        importArray.forEach(item => {
          // Pull the data from the table names into the proper table values
          item.OINumber = item['Item']
          item.Description = item['Descript']
          item.BU = item['Unit']
          item.BUStatus = 'Active'
          item.QOH = item['Qty On Hand']
          item.Cost = item['Ave Cost']
          item.Manufacturer = item["Primary Mfg ID"]
          item.ManufacturerPartNumber = item["Primary Mfg Itm ID"]

          updates.push({
            updateOne: {
              filter: { OINumber: item.OINumber },
              update: item,
              upsert: true,
            },
          })
        })

        deleted.forEach(item => {
          updates.push({
            updateOne: {
              filter: { OINumber: item.Item },
              update: { BUStatus : 'Inactive' }
            },
          })
        })
        const summary = await OiParts.bulkWrite(updates, {ordered:false})

        const returnSentence = `OI Part numbers have been updated. Incoming array has ${importArray.length} active items. ${summary.nModified} have been updated. 
        ${summary.nUpserted} have been added. ${deleted.length} existing entires in the database are currently set to inactive.`
        
        logger.info(returnSentence)
        return returnSentence
      } else {
        return 'Update file not present.'
      }
     
    } else {
      await importOINumbers()
      updateOiNumbers()
    }
  }catch(error){
    logger.error(error)
    // console.log(error)
    return false
  }
}

async function importOINumbers() {
  //? For the import, grab an export of items from peoplesoft financials
  //? Use query Reporting Tools > Query > Query Viewer >> OI_IN_ACTIVE_BUMCO_ITEM_INFO then fill in date and BU as BUMCO
  //? name columns properly for what we use in oiPartsModel. Make into JSON at http://beautifytools.com/excel-to-json-converter.php
  //? remove the root object and make it just an array of objects
  //? put the resulting object in as api > objects > OiPartNumbers.json

try{
    fs.ensureDirSync(imageFolder, { mode: 0o2775 })
    fs.ensureFileSync(path.join(process.cwd(),'api', 'objects', 'OiPartNumbers.json'))
    const updates = []
    let importArray = []
    importArray = fs.readJsonSync(path.join(process.cwd(),'api', 'objects', 'OiPartNumbers.json'), {throws:false})

    if(importArray && importArray.length){
      importArray.forEach(item => {

        updates.push({
          updateOne: {
            filter: { OINumber: item.OINumber },
            update: item,
            upsert: true,
          },
        })
      })
      const summary = await OiParts.bulkWrite(updates, {ordered:false})
      logger.info(
        `Doing initial load or update of OI part number items. ${summary.nUpserted} have been added.`
      )
      return summary
    }else{
      return 'No updates are present for OI numbers.'
    }
  }catch(error){
    logger.error(error)
    // console.log(error)
    return false
  }
}

async function importVendorNumbers() {
  //? For the import, grab an export of items from peoplesoft financials
  //? Use query Reporting Tools > Query > Query Viewer >> OI_ITEM_BY_VENDOR_BY_BU then fill in BU and Vendor
  //? drop the sheet down to the tabs we use. Make into JSON at http://beautifytools.com/excel-to-json-converter.php
  //? remove the root object and make it just an array of objects in each file
  //? put the results in api > objects > vendorFiles

  try{
    const vendorFilePath = path.join(process.cwd(),'api', 'objects', 'vendorFiles')
    fs.ensureDirSync(vendorFilePath)
    let jsonVendorList = fs.readdirSync(vendorFilePath)
    if(jsonVendorList.length){
      let vendorParts = []
      let updates = []
      jsonVendorList.forEach(vendor => {
        let singleVendor = require(path.join(vendorFilePath, vendor))
        vendorParts.push(singleVendor)
      })
      vendorParts.flat().forEach(item => {
        if (item.OINumber && item.Supplier && item.SupplierNumber && item.SupplierNumber !=='#N/A'){
          updates.push({
            updateOne: {
              filter: { 
                OINumber: item.OINumber,
                Supplier: item.Supplier,
               },
              update: item,
              upsert: true,
            },
          })
        }
      })
      const summary = await OiVendors.bulkWrite(updates, {ordered:false})
      const returnSentence = `OI Vendor numbers have been updated. ${summary.nUpserted} have been added/updated`

      logger.info(returnSentence)
      return returnSentence
    }else{
      return 'No vendor files are present to import/update'
    }
  }catch(error){
    logger.error(error)
    // console.log(error)
    return false
  }
}

module.exports = oiPartsRoute
