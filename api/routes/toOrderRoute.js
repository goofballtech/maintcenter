const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const toOrderRoute = express.Router({ mergeParams: true })

const baseToOrder = require('../models/toOrdersModel')

import { getAllVendors } from '../utils/helpers'
import { subMonths } from 'date-fns/esm'

toOrderRoute.route('/categories').get(function (req, res) {
  const ToOrder = baseToOrder.byTenant(req.params.db) // For some reason the tenant model was not limiting results as expect, work around added to find()
  ToOrder.find({ tenantId: req.params.db })
    .distinct('category')
    .exec(function (err, categories) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      // case insensitive sorting
      categories.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
      res.status(200).json(categories)
    })
})

toOrderRoute.route('/vendors').get(async function (req, res) {
  let vendors = await getAllVendors(req.params.db)
  res.status(200).json(vendors)
})

toOrderRoute.route('/list').get(function (req, res) {
  const ToOrder = baseToOrder.byTenant(req.params.db)
  ToOrder.find({ active: { $eq: true } }).exec(function (err, toorders) {
    if (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
    let listToorder = toorders.map(item => {
      return { text: item.name, value: item._id }
    })
    res.status(200).json(listToorder)
  })
})

// needs match string to compare and optional number of months (defaults to 3)
toOrderRoute.route('/similar').get(function (req, res) {
  const ToOrder = baseToOrder.byTenant(req.params.db)
  let offsetMonths
  if (req.query.months){
    offsetMonths = subMonths(new Date, parseInt(req.query.months))
  } else {
    offsetMonths = subMonths(new Date, parseInt(3))
  }
  if(req.query.match){
    ToOrder.find(
      {
        enteredDate: { $gt: offsetMonths } , 
        "vendor.vendorPartNumber":  {             
          $regex: `.*${req.query.match}.*`,
          $options: 'i',
        }
      }
      ).exec(function (err, similarOrders) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      let listOfSimilar = similarOrders.map(item => {
        return { 
          active: item.active, 
          name: item.name,
          quantity: item.quantity,
          vendor: item.vendor,
          enteredBy: item.enteredBy,
          enteredDate: item.enteredDate, 
          orderNumber: item.orderNumber,
          orderedBy: item.orderedBy,
          orderedDate: item.orderedDate,
        }
      })
      res.status(200).json(listOfSimilar)
    })
  } else {
    res.status(200).json("No query provided")
  }
})

// limit results to active order item lists using the specified vendor
// send vendor name as ?vendor=Oceeaneering
toOrderRoute.route('/byVendor').get(function (req, res) {
  const ToOrder = baseToOrder.byTenant(req.params.db)
  if(req.query.vendor){
    ToOrder.find(
      {
        active: { $eq: true } , 
        "vendor.vendorName":  { $eq: req.query.vendor },
      }).exec(function (err, ordersByVendor) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }

      ordersByVendor = ordersByVendor
      .map(item => {
        return {
          _id: item._id,
          name: item.name,
          quantity: item.quantity,
          partNumbersText: item.vendor.filter( item => item.vendorName == req.query.vendor && !item.isPending).map(number => number.vendorPartNumber).join(' / '),
          partNumbers: item.vendor.filter( item => item.vendorName == req.query.vendor && !item.isPending),
        }
      })
      res.status(200).json(ordersByVendor)
    })
  } else {
    res.status(200).json("No vendor provided")
  }
})

toOrderRoute
  .route('/:id?')
  .get(function (req, res) {
    const ToOrder = baseToOrder.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      ToOrder.findOne({ _id: req.params.id }).exec(function (err, order) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(order)
      })
    } else {
      if (req.params.id == 'past') {
        ToOrder.find({ active: { $eq: false } }).exec(function (err, toorder) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(400).json('An error has occurred, see log for details')
          }
          res.status(200).json(toorder)
        })
      } else {
        if (req.query.count) {
          ToOrder.countDocuments({
            active: { $eq: true },
            tenantId: { $eq: req.params.db },
            neededBy: { $lte: new Date() },
          }).exec(function (err, toorderCount) {
            if (err) {
              logger.error(
                JSON.stringify({
                  details: err,
                  url: req.originalUrl,
                  method: req.method,
                })
              )
              return res.status(400).json('An error has occurred, see log for details')
            }
            return res.status(200).json({ number: toorderCount })
          })
        } else {
          ToOrder.find({ active: { $eq: true } }).exec(function (err, toorder) {
            if (err) {
              logger.error(
                JSON.stringify({
                  details: err,
                  url: req.originalUrl,
                  method: req.method,
                })
              )
              return res.status(400).json('An error has occurred, see log for details')
            }
            res.status(200).json(toorder)
          })
        }
      }
    }
  })
  .put(function (req, res) {
    const ToOrder = baseToOrder.byTenant(req.params.db)
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      ToOrder.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }, function (err, toorder) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully updated ${toorder.name}`)
      })
    } else {
      res.status(200).json(`To Order ID is not valid`)
    }
  })
  .post(function (req, res) {
    const newToOrder = new baseToOrder(req.body)
    newToOrder.tenantId = req.params.db
    newToOrder
      .save()
      .then(result => {
        res.status(200).json({
          object: result,
          message: `${newToOrder.enteredBy} added new a To Order item named ${newToOrder.name}`,
        })
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function (req, res) {
    const ToOrder = baseToOrder.byTenant(req.params.db)
    ToOrder.findByIdAndDelete(req.params.id, function (err, toorder) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Successfully removed ${toorder.name}`)
    })
  })

module.exports = toOrderRoute
