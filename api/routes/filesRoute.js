const express = require('express')
const multer = require('multer')
const fs = require('fs-extra')
const mime = require('mime-types')
const path = require('path')
const zip = require('adm-zip')
const logger = require('../../server/logger')
import { format } from 'date-fns/esm'
import { base64_encode, makeZipFromList } from '../utils/helpers.js'

const baseDir = path.join(process.cwd(), 'supportDocs')

const tmpDir = path.join(baseDir, 'uploadtmp')

const upload = multer({ dest: tmpDir })

const fileRoute = express.Router({ mergeParams: true })

// download a file if it exists
fileRoute.route('/download/:page?/:id?/:file?').get(function(req, res) {
  let file
  if (req.query.path) {
    file = req.query.path
  } else {
    file = path.join(baseDir, req.params.db, req.params.page, req.params.id, req.params.file)
  }
  checkFile(file, res)
  async function checkFile(file, res) {
    try {
      if (!fs.existsSync(file)) {
        return res.status(200).json('Linked file could not be found')
      }
      if (mime.lookup(file) && mime.lookup(file).includes('pdf') && req.query.view) {
        let readFile = fs.readFileSync(file)
        res.contentType('application/pdf')
        res.send(readFile)
      } else {
        res.status(200).download(file)
      }
    } catch (err) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
            fileName: file,
          })
        )
        return res.status(200).json({
          error: 'An error has occurred, see log for details',
        })
      }
    }
  }
})

// upload a file
fileRoute.route('/upload/:page/:id').post(upload.single('file'), function(req, res) {
  const tmpName = req.file.filename
  const dir = path.join(baseDir, req.params.db, req.params.page, req.params.id)
  const fileNameWithDate = appendToFilename(req.file.originalname, format(new Date(), 'yyyy-MM-dd'))
  checkPath(dir, res)
  checkExistsAndMove(path.join(dir, fileNameWithDate), res)
  const options = {
    mode: 0o2775, // file/folder creation mode
  }

  async function checkPath(dir, res) {
    const pathExists = await fs.pathExists(dir)

    if (!pathExists) {
      logger.info(`${dir} folder structure doesn't exist so i'm trying to make it`)
      try {
        await fs.ensureDir(dir, options)
        logger.info(`${dir} folder(s) created successfully`)
      } catch (err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
              directory: dir,
            })
          )
          return res.status(200).json({
            error: 'An error has occurred, see log for details',
          })
        }
      }
    }
  }

  async function checkExistsAndMove(file, res) {
    try {
      if (fs.existsSync(file)) {
        return res.status(200).send('File already exists, did not save')
      }
      moveFile(path.join(tmpDir, tmpName), file, res)
    } catch (err) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
            file: file,
          })
        )
        return res.status(200).json({
          error: 'An error has occurred, see log for details',
        })
      }
    }
  }

  async function moveFile(src, dest, res) {
    try {
      await fs.move(src, dest)
      if (req.query.extract && mime.lookup(dest).includes('zip')) {
        let extractFolder = dest.substr(0, dest.lastIndexOf('.'))
        await fs.ensureDir(extractFolder, options)
        let archive = new zip(dest)
        archive.extractAllTo(extractFolder, true)
        let list = await fs.readdir(extractFolder)
        res.status(200).json({
          reply: `Successfully extracted archive to ${extractFolder}, uploaded by ${req.body.techName}`,
          fileList: list,
          path: extractFolder,
        })
      } else {
        res.status(200).json({
          reply: `Successfully saved file to ${dest}, uploaded by ${req.body.techName}`,
        })
      }
    } catch (err) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
            source: src,
            destination: dest,
          })
        )
        return res.status(400).json({
          error: 'An error has occurred, see log for details',
        })
      }
    }
  }

  function appendToFilename(filename, string) {
    var dotIndex = filename.lastIndexOf('.')
    if (dotIndex == -1) return filename + string
    else return filename.substring(0, dotIndex) + `_${string}` + filename.substring(dotIndex)
  }
})

// delete a file if it exists
fileRoute.route('/delete/:page/:id/:file').delete(async function(req, res) {
  const file = path.join(baseDir, req.params.db, req.params.page, req.params.id, req.params.file)
  try {
    if (!fs.existsSync(file)) {
      return res.status(400).json({ error: 'File could not be found' })
    }
    if (fs.unlink(file)) {
      return res.status(200).send(`${file} deleted by ${req.body.techName}`)
    }
    return res.status(400).json({
      error: `Could not delete existing ${file}`,
    })
  } catch (err) {
    if (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl,
          method: req.method,
          file: file,
        })
      )
      return res.status(200).json({
        error: 'An error has occurred, see log for details',
      })
    }
  }
})

//make zip out of a provided list of files and send back as download
fileRoute.route('/makezip').post(async function(req, res) {
  try {
    let files = req.body
    let zipName = req.query.name?req.query.name:'Zipped File Set'
    const zipInfo = await makeZipFromList(files, req.params.db, zipName)
    return res.status(200).json(zipInfo)
  } catch (err) {
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    res
      .status(200)
      .json('An error has occurred when attempting to download the zip of a list of files, see logs for details')
  }
})

//get file list for a certain folder
fileRoute.route('/:page/:id').get(async function(req, res) {
  const dir = path.join(baseDir, req.params.db, req.params.page, req.params.id)
  try {
    const pathExists = await fs.pathExists(dir)
    if (pathExists && req.query.downloadAll) {
      const options = {
        mode: 0o2775, // file/folder creation mode
      }
      await fs.ensureDir(path.join(baseDir, req.params.db, 'downloads'), options)
      let zippedAttachments = new zip()

      zippedAttachments.addLocalFolder(dir)

      let zipLocation = path.join(
        baseDir,
        req.params.db,
        'downloads',
        `${req.params.page}_${req.params.id}_${format(new Date(), 'yyyy-MM-dd')}_download.zip`
      )
      zippedAttachments.writeZip(zipLocation)

      res.download(zipLocation)

      fs.remove(path.join(baseDir, req.params.db, 'downloads'))
    } else if (pathExists) {
      let files = await fs.readdir(dir)
      let filesObjectArray = []
      files.forEach(item => {
        filesObjectArray.push({
          fileName: item,
          type: mime.lookup(item),
          base64:
            mime.lookup(item) && mime.lookup(item).includes('image') ? base64_encode(path.join(dir, item)) : undefined,
        })
      })
      res.status(200).json(filesObjectArray)
    } else {
      res.status(200).json([])
    }
  } catch (err) {
    logger.error(
      JSON.stringify({
        details: err,
        url: req.originalUrl,
        method: req.method,
      })
    )
    res.status(200).json('An error has occurred when attempting to download the backup, see logs for details')
  }
})

module.exports = fileRoute
