const express = require('express')
const mongoose = require('mongoose')
const logger = require('../../server/logger')

const packageRoute = express.Router({ mergeParams: true })

const basePackages = require('../models/packagesModel')

packageRoute.route('/checkid/:id?').get(function(req, res) {
  const Packages = basePackages.byTenant(req.params.db)
  if (req.params.id) {
      Packages.find({ entryNumber: req.params.id }).exec(function (err, packages) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        if (packages.length) {
          res.status(200).json('exists')
        } else {
          res.status(200).json('available')
        }
  })
  }
})

// operations for package notes
packageRoute
  .route('/notes/:id/:noteid?')
  // get the notes for a package _id
  .get(function (req, res) {
    const Package = basePackages.byTenant(req.params.db)
    Package.findById(req.params.id, function (err, packageItem) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(packageItem.notes)
    })
  })
  // post a new note into _id
  .post(function (req, res) {
    const Package = basePackages.byTenant(req.params.db)
    const note = req.body
    // push the comment to the notes subdocument
    Package.findByIdAndUpdate(req.params.id, { $push: { notes: note } }, function (err, packageItem) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(201).json(`Note added to package ${packageItem.entryNumber} by ${note.techName}`)
    })
  })
  // delete a note
  .delete(function (req, res) {
    const Package = basePackages.byTenant(req.params.db)
    const note = req.body
    // pull the comment from the notes subdocument
    Package.findByIdAndUpdate(req.params.id, { $pull: { notes: { _id: req.params.noteid } } }, function (err, packageItem) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res.status(200).json(`Note deleted from package ${packageItem.entryNumber} by ${note.techName}`)
    })
  })

packageRoute.route('/categories').get(async function(req, res) {
  const Packages = basePackages.byTenant(req.params.db)
  try {
    let categories = await Packages.find({ tenantId: req.params.db }).distinct('category')
    if (categories.length) {
      categories.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    }
    res.status(200).json(categories)
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

packageRoute.route('/systems').get(async function(req, res) {
  const Packages = basePackages.byTenant(req.params.db)
  try {
    let systems = await Packages.find({ tenantId: req.params.db }).distinct('system')
    if (systems.length) {
      systems.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    }
    res.status(200).json(systems)
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    return res.status(400).json('An error has occurred, see log for details')
  }
})

packageRoute
  .route('/:id?')
  .get(async function(req, res) {
    let category = []
    if (req.query.category){
      category = req.query.category.split(',')
    }

    const Packages = basePackages.byTenant(req.params.db)
    if (category.length || req.query.status || req.query.system){
      let findObject = {}

      if (req.query.system && req.query.system !== 'All'){
        findObject.system = req.query.system
      }

      if(category.length){
        findObject.category = { $in : category }
      }

      if (req.query.status){
        switch(req.query.status) {
          case 'All':
            // Do nothing
            break
          case 'Currently Open':
            findObject.openDate = { $lte: new Date() }
            findObject.closeDate = { $exists: false }
          break
          case 'Not Opened':
            findObject.openDate = { $exists: false }
            findObject.closeDate = { $exists: false }
          break
        }      
      }
      const systemPackages = await Packages.find(findObject)
      res.status(200).json(systemPackages)
    } else {
      try {
        const systemPackages = req.params.id
          ? await Packages.find({ entryNumber: req.params.id })
          : await Packages.find({})
        res.status(200).json(systemPackages)
      } catch (error) {
        logger.error(
          JSON.stringify({
            details: error,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
    }
  })
  .put(function(req, res) {
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      basePackages.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }, function(err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully updated package entry`)
      })
    } else {
      res.status(200).json(`Package ID is not valid`)
    }
  })
  .post(function(req, res) {
    const newPackage = new basePackages(req.body)
    newPackage.tenantId = req.params.db
    newPackage
      .save()
      .then(() => {
        res.status(201).json(`Package entry ${newPackage.entryNumber} saved to database by ${req.body.techName}`)
      })
      .catch(err => {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      })
  })
  .delete(function(req, res) {
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      basePackages.findByIdAndDelete(req.params.id, function(err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        res.status(200).json(`Successfully deleted package entry`)
      })
    } else {
      res.status(200).json(`Package ID for attempted delete is not valid`)
    }
  })

module.exports = packageRoute