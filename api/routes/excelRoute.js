const express = require('express')
const fs = require('fs-extra')
const logger = require('../../server/logger')
const multer = require('multer')
const path = require('path')
const excel = require('exceljs')

const excelRoute = express.Router({ mergeParams: true })
const baseDir = path.join(process.cwd(), 'supportDocs')
const tmpDir = path.join(baseDir, 'uploadtmp')
const upload = multer({ dest: tmpDir })
const baseTask = require('../models/tasksModel')

import { addDays, format } from 'date-fns/esm'
import { makeExcel, makeEncompassImport, makePRExcel } from '../utils/excelHelpers'

excelRoute.route('/export')
.post(async function (req, res) {
  try {
    const sheetData = req.body
    if (sheetData.sheetDataSets || (sheetData.columns && sheetData.data)) {
      res.status(200).json(await makeExcel(req.params.db, sheetData, req.query.encodeFiles))
    } else {
      return res.status(200).json('Export was missing the columns or table data, cannot build sheet to export')
    }
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    res.status(200).json({
      error: 'An error has occurred, see log for details',
    })
  }
})

excelRoute.route('/exportToEncompass')
.post(async function (req, res) {
  // fileName: file name for export, default set if not provided
  // requestDate: Date
  // jobNumber: ''
  // data: [ quantity, partNumber ]
  try {
    let sheetData = req.body
    if(!sheetData.filename) sheetData.filename = `EncompassTemplate_${format(new Date(), 'yyyy-MM-dd')}`
    
    if (sheetData.data && sheetData.data.length) {
      res.status(200).json(await makeEncompassImport(req.params.db, sheetData, req.query.encodeFiles))
    } else {
      return res.status(200).json('Export was missing required information, could not build')
    }
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    res.status(200).json({
      error: 'An error has occurred, see log for details',
    })
  }
})

excelRoute.route('/exportToPR')
.post(async function (req, res) {
  // fileName: file name for export, default set if not provided
  // requestDate: Date
  // jobNumber: ''
  // data: [ quantity, partNumber ]
  try {
    let sheetData = req.body
    if(!sheetData.filename) sheetData.filename = `PRTemplate_${format(new Date(), 'yyyy-MM-dd')}`
    
    if (sheetData.data && sheetData.data.length) {
      res.status(200).json(await makePRExcel(req.params.db, sheetData, req.query.encodeFiles))
    } else {
      return res.status(200).json('Export was missing required information, could not build')
    }
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
        url: req.originalUrl,
        method: req.method,
      })
    )
    res.status(200).json({
      error: 'An error has occurred, see log for details',
    })
  }
})

excelRoute.route('/download/:filename').get(function (req, res) {
  let exportDir
  if(req.query.path){
    exportDir = path.join(baseDir, req.params.db, req.query.path)
  } else {
    exportDir = path.join(baseDir, req.params.db, 'excel')
  }
  const file = path.join(exportDir, req.params.filename)
  checkFile(file, res)
  async function checkFile(file, res) {
    try {
      if (fs.existsSync(file)) {
        res.status(200).download(file)
        setTimeout(() => fs.unlink(file), 1000 * 60 * 15)
      } else {
        return res.status(200).json('Linked file could not be found')
      }
    } catch (err) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
            fileName: file,
          })
        )
        return res.status(400).json({
          error: 'An error has occurred, see log for details',
        })
      }
    }
  }
})

excelRoute.route('/import/tasks')
  .post(upload.single('file'), function (req, res) {
    const Task = baseTask.byTenant(req.params.db)
    const tmpName = req.file.filename
    if (req.file.originalname !== 'MaintCenter task import template.xlsx') {
      return res.status(200).json('Import attempted from a file with a different name than the template')
    }
    checkPath(tmpDir, res)
    checkExistsAndImport(path.join(tmpDir, tmpName), path.join(tmpDir, 'toImport.xlsx'), res)

    async function checkPath(dir, res) {
      const options = {
        mode: 0o2775, // file/folder creation mode
      }
      const pathExists = await fs.pathExists(dir)
      if (!pathExists) {
        logger.info(`${dir} folder structure doesn't exist so i'm trying to make it`)
        try {
          await fs.ensureDir(dir, options)
          logger.info(`${dir} folder(s) created successfully`)
        } catch (err) {
          if (err) {
            logger.error(
              JSON.stringify({
                details: err,
                url: req.originalUrl,
                method: req.method,
                directory: dir,
              })
            )
            return res.status(400).json({
              error: 'An error has occurred, see log for details',
            })
          }
        }
      }
    }

    async function checkExistsAndImport(tmp, file, res) {

      try {
        if (fs.existsSync(file)) {
          fs.unlinkSync(file)
        }
        await fs.move(tmp, file)
        let workbook = new excel.Workbook()
        workbook.properties.date1904 = true

        let headers = []
        let tasksToWrite = []
        let newID = 0

        workbook.xlsx.readFile(file).then(async function () {
          const worksheet = workbook.getWorksheet(1)

          try {
            const getNewID = await Task.findOne({}).sort({ taskID: -1 })

            if (getNewID && getNewID.taskID) {
              newID = getNewID.taskID + 1
            } else {
              newID = 3000
            }
          } catch (error) {
            logger.error(
              JSON.stringify({
                details: error,
                url: req.originalUrl,
                method: req.method,
              })
            )
            return res.status(200).json('An error has occurred, see log for details')
          }

          worksheet.eachRow(function (row, rowNumber) {
            if (rowNumber == 1) {
              headers = row.values
            } else {
              // need to do getCell(1).text to ignore formatting in fields is it's there
              let tempItem = ''
              for (let i = 0; i < row.values.length; i++) {
                if (headers[i] && (row.values[i] || row.values[i] == '0')) {
                  if (row.values[i].richText && row.values[i].richText.length) {
                    row.values[i].richText.forEach((element, index) => {
                      if (index == 0) {
                        row.values[i] = element.text
                      } else {
                        row.values[i] = `${row.values[i]}${element}.text`
                      }
                    })
                  }
                  if (tempItem == '') {
                    tempItem = `{"${headers[i]}":${JSON.stringify(row.values[i])}`
                  } else if (i == row.values.length - 1) {
                    tempItem = `${tempItem},"${headers[i]}":${JSON.stringify(row.values[i])}}`
                  } else {
                    tempItem = `${tempItem},"${headers[i]}":${JSON.stringify(row.values[i])}`
                  }
                }
              }

              const taskObject = JSON.parse(tempItem)
              taskObject.tenantId = req.params.db

              if (taskObject.taskID == undefined || taskObject.taskID == '') {
                taskObject.taskID = newID
                newID++
                // if task ID is not a number, assume it's custom
              } else if (isNaN(taskObject.taskID)){
                taskObject.customTaskID = taskObject.taskID
                taskObject.taskID = newID
                newID++
              }

              if (taskObject.lastCompletion && taskObject.opsFrequency) {
                taskObject.nextDue = addDays(Date.parse(taskObject.lastCompletion), taskObject.opsFrequency)
              } else {
                taskObject.nextDue = new Date()
              }

              if (taskObject.category) {
                taskObject.category = taskObject.category.split(',').map(item => item.trim())

                if (taskObject.category.includes('ops')) {
                  taskObject.category.splice(taskObject.category.indexOf('ops'), 1)
                }
                if (taskObject.category.includes('transit')) {
                  taskObject.category.splice(taskObject.category.indexOf('transit'), 1)
                }
                if (taskObject.category.includes('Meter')) {
                  taskObject.category.splice(taskObject.category.indexOf('Meter'), 1)
                }
              }

              tasksToWrite.push(taskObject)
              logger.info(`Adding task #${taskObject.taskID} to the queue from imported spreadsheet`)
            }
          })

          await Task.insertMany(tasksToWrite, function(err) {
            if (err) {
              logger.error(err)
            } 
          })

          res.status(200).json(`${tasksToWrite.length} tasks imported to the database from the provided sheet`)

        })
      } catch (err) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
              file: file,
            })
          )
          return res.status(400).json({
            error: 'An error has occurred, see log for details',
          })
        }
      }
    }
  })

module.exports = excelRoute