const express = require('express')
const logger = require('../../server/logger')
import { parseISO, startOfDay, endOfDay } from 'date-fns/esm'

const logRoute = express.Router({ mergeParams: true })

const baseSiteLogs = require('../models/siteLogsModel.js')

const dev = !(process.env.NODE_ENV === 'production')

//? Sync the indexes in the database with those that have been expressly created in the model.
baseSiteLogs.syncIndexes()

// get all tasks
logRoute
  .route('/')
  .get(async function(req, res) {
    let query = {}

    const limit = parseInt(req.query.limit) || 1000

    if (req.query.fromDate || req.query.untilDate) {
      if (req.query.fromDate && !req.query.untilDate) {
        query.timestamp = { $gte: startOfDay(parseISO(req.query.fromDate)) }
      } else if (!req.query.fromDate && req.query.untilDate) {
        query.timestamp = { $lte: endOfDay(parseISO(req.query.untilDate)) }
      } else if (req.query.fromDate && req.query.untilDate) {
        query.timestamp = {
          $lte: endOfDay(parseISO(req.query.untilDate)),
          $gte: startOfDay(parseISO(req.query.fromDate)),
        }
      }
    }
    if (req.query.level) {
      query.level = req.query.level
    }
    if (req.query.onlyCurrent) {
      query['meta.tenant'] = req.params.db
    }

    let logResults = await baseSiteLogs
      .find(query)
      .limit(limit)
      .sort({ timestamp: -1 })

    res.status(200).json(logResults)
  })
  .post(function(req, res) {
    const logEntry = req.body
    logger.log({
      level: logEntry.level,
      message: logEntry.body,
      metadata: { tenant: req.params.db },
    })
    if (!dev) {
      // eslint-disable-next-line
    console.log(`${req.params.db}-${logEntry.level}: ${logEntry.body}`)
    }
    res.status(200).json(logEntry)
  })

module.exports = logRoute
