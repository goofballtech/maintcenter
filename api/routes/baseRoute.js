const express = require('express')
const logger = require('../../server/logger')

const baseRoute = express.Router({ mergeParams: true })

const baseSettings = require('../models/settingsModel')
import { removeElemWithIdAndValue } from '../utils/helpers'

const dev = !(process.env.NODE_ENV === 'production')

baseRoute
  .route('/tenants/:id?')
  .get(function(req, res) {
    baseSettings
      .aggregate([
        {
          $group: {
            _id: '$tenantId',
            friendlyName: {
              $first: '$friendlyName',
            },
            mongoID: {
              $first: '$_id',
            },
          },
        },
      ])
      .exec(function(err, tenants) {
        if (err) {
          logger.error(
            JSON.stringify({
              details: err,
              url: req.originalUrl,
              method: req.method,
            })
          )
          return res.status(400).json('An error has occurred, see log for details')
        }
        if (!dev){
          tenants = removeElemWithIdAndValue(tenants, "_id", "dev")
        }
        // case insensitive sorting
        tenants.sort((a, b) =>
          a.friendlyName.localeCompare(b.friendlyName, undefined, {
            sensitivity: 'base',
          })
        )
        res.status(200).json(tenants)
      })
  })
  .delete(function(req, res) {
    baseSettings.findByIdAndDelete(req.params.id, function(err, settings) {
      if (err) {
        logger.error(
          JSON.stringify({
            details: err,
            url: req.originalUrl,
            method: req.method,
          })
        )
        return res.status(400).json('An error has occurred, see log for details')
      }
      res
        .status(200)
        .json(
          `Settings for ${
            settings.friendlyName
          } have been removed, all the data is still in the database though. It can be accessed by visiting "${
            settings.tenantId
          }/tasks"`
        )
    })
  })

module.exports = baseRoute
