const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')
const { ObjectId } = require('mongodb')

const partLocationSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    name: String,
    parent: ObjectId,
    children: { type: Array, default: [] },

  },
  { timestamps: true }
)

partLocationSchema.plugin(mongoTenant)

const PartLocations = mongoose.model('PartLocations', partLocationSchema)

module.exports = PartLocations
