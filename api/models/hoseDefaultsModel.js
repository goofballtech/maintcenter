const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const hoseDefaultsSchema = new mongoose.Schema(
  {
    value: { type: String, trim: true },
    text: { type: String, trim: true },
    hosePressureRating: Number,
    hosePartNumber: { type: String, trim: true },
    hoseFitting: { type: String, trim: true },
  },
  { timestamps: true }
)

hoseDefaultsSchema.plugin(mongoTenant)

const HoseDefaults = mongoose.model('HoseDefaults', hoseDefaultsSchema)

module.exports = HoseDefaults
