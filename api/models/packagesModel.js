const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const noteSchema = new mongoose.Schema(
  {
    techName: { type: String, required: true, trim: true },
    note: { type: String, trim: true },
    manualDate: { type: Date },
  },
  { timestamps: true }
)

const packagesSchema = new mongoose.Schema(
  {
    entryNumber: { type: String, trim: true }, 
    task: {
      type: String,
      trim: true,
      required: 'A task for the entry is required',
    },
    category: Array,
    system: {
      type: String,
      trim: true,
      required: 'A system for the entry is required',
    },
    description: String,
    notes: [noteSchema],
    openDate: Date,
    closeDate: Date,
    techName: { type: String, trim: true },
    lastEditedBy: { type: String, trim: true },
  },
  { timestamps: true }
)

packagesSchema.plugin(mongoTenant)

const Packages = mongoose.model('Packages', packagesSchema)

module.exports = Packages
