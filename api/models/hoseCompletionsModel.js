const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')
const { ObjectId } = require('mongodb')

const hoseCompletionSchema = new mongoose.Schema(
  {
    hoseId: ObjectId,
    techName: { type: String, trim: true },

    notes: { type: String, trim: true },
    inspectionDate: Date,
    changeDate: Date,
  },
  { timestamps: true }
)

hoseCompletionSchema.plugin(mongoTenant)

const HoseCompletion = mongoose.model('HoseCompletion', hoseCompletionSchema)

module.exports = HoseCompletion
