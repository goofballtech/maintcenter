const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoTenant = require('mongo-tenant')

const itemSpecsSchema = new mongoose.Schema(
  {
    detail: { type: String, trim: true },
    date: Date,
  },
  { timestamps: true }
)

const partLocationSchema = new mongoose.Schema(
  {
    location: Schema.ObjectId,
    qty: Number,
    additionalNotes: { type: String, trim: true },
    eachItemDetails: [itemSpecsSchema],
    lastChecked: Date,
    lastCheckedBy: { type: String, trim: true },
  },
  { timestamps: true }
)

const partNumberSchema = new mongoose.Schema(
  {
    vendor: { type: String, trim: true },
    partNumber: { type: String, trim: true },
  },
  { timestamps: true }
)

const drawingRefSchema = new mongoose.Schema(
  {
    drawing: { type: String, trim: true },
    bomLine: { type: String, trim: true },
  },
  { timestamps: true }
)

const inventoryPartsSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    name: { type: String, trim: true },
    description: { type: String, trim: true },
    min: Number,
    max: Number,
    uom: { type: String, default: "Each", trim: true },
    criticalSpare: { type: Boolean, default: false },
    detailNeeded: { type: Boolean, default: false },
    detailLabel: { type: String, trim: true },
    dateNeeded: { type: Boolean, default: false },
    dateLabel: { type: String, trim: true },
    compliance: { type: Boolean, default: false },
    relatedParts: [Schema.ObjectId],
    locations: [partLocationSchema],
    drawingRef: [drawingRefSchema],
    keywords: [{ type: String, trim: true }],
    category: [{ type: String, trim: true }],
    partNumber: [partNumberSchema],
    customerID: { type: String, trim: true, },
    PO: { type: String, trim: true, },
  },
  { timestamps: true }
)

inventoryPartsSchema.plugin(mongoTenant)

inventoryPartsSchema.statics.getCategoryCounts = function () {
  return this.aggregate([{ $unwind: '$category' }, { $group: { _id: '$category', count: { $sum: 1 } } }])
}

const inventoryParts = mongoose.model('inventoryParts', inventoryPartsSchema)

module.exports = inventoryParts
