const mongoose = require('mongoose')

const oiPartSchema = new mongoose.Schema(
  {
    OINumber: {
      type: String,
      unique: true,
      index: true,
      trim: true,
    },
    Description: {
      type: String,
      index: true,
      trim: true,
    },
    BU: {
      type: String,
      trim: true,
    },
    BUStatus: {
      type: String,
      trim: true,
    },
    QOH: {
      type: String,
      trim: true,
    },
    Cost: {
      type: String,
      trim: true,
    },
    UOM: {
      type: String,
      trim: true,
    },
    LeadDays: {
      type: String,
      trim: true,
    },
    M1to12Usage: {
      type: String,
      trim: true,
    },
    M13to24Usage: {
      type: String,
      trim: true,
    },
    Manufacturer: {
      type: String,
      trim: true
    },
    ManufacturerPartNumber: {
      type: String,
      trim: true,
    },
  },
  { timestamps: true }
)

const OiParts = mongoose.model('OiParts', oiPartSchema)

module.exports = OiParts
