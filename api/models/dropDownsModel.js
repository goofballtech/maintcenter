const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const dropDownSchema = new mongoose.Schema(
  {
    menu: String,
    value: Number,
    text: String,
  },
  { timestamps: true }
)

dropDownSchema.plugin(mongoTenant)

const DropDown = mongoose.model('DropDown', dropDownSchema)

module.exports = DropDown
