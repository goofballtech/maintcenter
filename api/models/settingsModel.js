const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')
import { subDays } from 'date-fns'

const settingsSchema = new mongoose.Schema(
  {
    siteTheme: { type: String, default: 'dark' },
    hiddenMenu: { type: Boolean, default: true },
    friendlyName: { type: String, default: 'No Name Set :(' },
    
    activeStatus: { type: String, default: 'Transit' },
    previousStatus: { type: String, default: 'Transit' },
    taskModeType: { type: String, default: 'classic' },
    taskModeShown: { type: Boolean, default: true },
    modeOptions: {
      mode1: { 
        active: { type: Boolean, default: true },
        name: { type: String, default: 'Mode 1' },
        color: { 
          type: String,
          default: 'none', 
          enum: ['none', 'primary', 'accent', 'secondary', 'success', 'info', 'warning', 'error']
        }
      },
      mode2: { 
        active: { type: Boolean, default: false },
        name: { type: String, default: 'Mode 2' },
        color: { 
          type: String,
          default: 'none', 
          enum: ['none', 'primary', 'accent', 'secondary', 'success', 'info', 'warning', 'error']
        }
      },
      mode3: {  
        active: { type: Boolean, default: false },
        name: { type: String, default: 'Mode 3' },
        color: { 
          type: String,
          default: 'none',
          enum: ['none', 'primary', 'accent', 'secondary', 'success', 'info', 'warning', 'error']
        }
      },
      mode4: { 
        active: { type: Boolean, default: false },
        name: { type: String, default: 'Mode 4' },
        color: { 
          type: String,
          default: 'none', 
          enum: ['none', 'primary', 'accent', 'secondary', 'success', 'info', 'warning', 'error']
        }
      }
    },

    menuOptions: {
      maintenanceTasks: { type: Boolean, default: true },
      toDo: { type: Boolean, default: true },
      logs: { type: Boolean, default: true },
      packages: { type: Boolean, default: true },
      hoses: { type: Boolean, default: true },
      toOrder: { type: Boolean, default: true },

      inventory: { type: Boolean, default: true },
      assets: { type: Boolean, default: true },
      safety: { type: Boolean, default: true },
      msds: { type: Boolean, default: true },
      techBulletins: { type: Boolean, default: true },
      rigging: { type: Boolean, default: true },
      hospital: { type: Boolean, default: true },
      contacts: { type: Boolean, default: true },
    },
    showMenuValues: { type: Boolean, default: true },
    numBackupsToKeep: { type: Number, default: 10 },
    daysPerBackup: { type: Number, default: 7 },

    defaultInspection: { type: Number, default: 90 },
    maxInspection: { type: Number, default: 90 },
    defaultChange: { type: Number, default: 1095 },
    maxChange: { type: Number, default: 1095},
    maxHoseSizeMadeOnSite: { type: String, default: 16 },
    hoseLengthUnit: { type: String, enum: ['meters', 'feet'], default: 'feet' },

    attachmentSlugs: { type: Array, default: [] },
    
    showTaskDelayOption: { type: Boolean, default: false },
    showFreqColumn: { type: Boolean, default: false },
    inventoryMode: { type: Boolean, default: false },
    
    orderMonths: { type: Number, default: 3 },

    autoEmailOptions: {
      toOrder: {
        enabled: { type: Boolean, default: false },
        name: { type: String, default: 'To Order' },
        emailSubject: { type: String, trim: true, default: 'To Order' },
        emailTo: { type: Array, default: [] },
        emailCC: { type: Array, default: [] },
        emailBCC: { type: Array, default: [] },
        daysBetweenAuto: { type: Number, default: 0 },
        dateLastSent: Date,
        manualNextSend: Date,
      },
      taskExport: {
        enabled: { type: Boolean, default: false },
        name: { type: String, default: 'Task Export' },
        emailSubject: { type: String, trim: true, default: 'Task Export' },
        emailTo: { type: Array, default: [] },
        emailCC: { type: Array, default: [] },
        emailBCC: { type: Array, default: [] },
        daysBetweenAuto: { type: Number, default: 0 },
        dateLastSent: Date,
        manualNextSend: Date,
        searchCriteria: {
          type: Object,
          default: {
            days: 30,
            techName: '',
            category: [],
            taskID: '',
            taskName: '',
            includeForms: false,
          },
        },
      },
      offsiteBackup: {
        enabled: { type: Boolean, default: true },
        name: { type: String, default: 'Offsite Backups (Database Only)' },
        emailSubject: { type: String, trim: true, default: 'Offsite Backup' },
        emailTo: { type: Array, default: [] },
        emailCC: { type: Array, default: [] },
        emailBCC: { type: Array, default: [] },
        daysBetweenAuto: { type: Number, default: 14 },
        dateLastSent: { type: Date, default: subDays(new Date(), 14) },
        manualNextSend: Date,
      },
      siteUpdate: {
        enabled: { type: Boolean, default: true },
        name: { type: String, default: 'Site Version Updates' },
        emailSubject: { type: String, trim: true, default: 'Maintenance site has been updated' },
        emailTo: { type: Array, default: [] },
        emailCC: { type: Array, default: [] },
        emailBCC: { type: Array, default: [] },
        currentVersion: { type: String, default: 'none' },
        dateLastSent: Date,
      },
    },
  },
  { timestamps: true }
)
settingsSchema.plugin(mongoTenant)

const Settings = mongoose.model('Settings', settingsSchema)

module.exports = Settings
