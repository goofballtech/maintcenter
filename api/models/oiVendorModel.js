const mongoose = require('mongoose')

const oiVendorSchema = new mongoose.Schema(
  {
    OINumber: {
      type: String,
      index: true,
      trim: true,
    },
    Supplier: {
      type: String,
      trim: true,
    },
    SupplierNumber: {
      type: String,
      trim: true,
    },
  },
  { timestamps: true }
)

const OiVendors = mongoose.model('OiVendors', oiVendorSchema)

module.exports = OiVendors
