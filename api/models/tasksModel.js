const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const noteSchema = new mongoose.Schema(
  {
    techName: { type: String, required: true, trim: true },
    note: { type: String, trim: true },
    manualDate: { type: Date },
    enteredDate: { type: Date },
  },
  { timestamps: true }
)

const additionalInfoForm = new mongoose.Schema(
  {
    formFields: Array,
  },
  { timestamps: true, strict: false }
)

const additionalInfoSubmissions = new mongoose.Schema(
  {
    formSubmissions: Array,
  },
  { timestamps: true, strict: false }
)

const taskSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    taskID: {
      type: Number,
      unique: true,
      required: true,
      trim: true,
    },
    customTaskID: {
      type: String,
      trim: true,
    },
    taskName: { type: String, required: true, trim: true },
    category: { type: Array, required: true, trim: true },
    taskHours: Number,
    detailedDescription: { type: String, trim: true },
    // date completion info
    lastCompletion: Date,
    opsFrequency: { type: Number },
    transitFrequency: { type: Number },
    layupTransition: { type: Number, default: 0 },
    nextDue: { type: Date },
    deferred: { type: Boolean, default: false },
    // forced date
    useForcedDue: { type: Boolean, default: false },
    forcedDueDate: { type: Date },
    forcedDateReason: { type: String, trim: true },
    // task on hold
    onHold: { type: Boolean, default: false },
    onHoldType: { type: String, trim: true },
    onHoldReason: { type: String, trim: true },
    onHoldRelated: Object,
    onHoldBy: { type: String, trim: true },
    onHoldDate: { type: String, trim: true },
    // lead time for task
    earlyNotification: { type: Boolean, default: false },
    earlyNotificationDays: Number,
    earlyNotificationText: { type: String, trim: true },
    earlyNotificationSubmissionDate: Date,
    earlyNotificationSubmissionTech: { type: String, trim: true },
    earlyNotificationComplete: { type: Boolean, default: false },
    // email Notifications
    emailTo: { type: Array, default: [] },
    emailAlreadySent: { type: Boolean, default: false },
    // meter info
    meterRequired: { type: Boolean, default: false },
    meterName: { type: String, trim: true },
    meterFrequency: Number,
    lastMeterReadingCompleted: Number,
    nextMeterReadingDue: Number,
    meterEarlyNotificationComplete: { type: Boolean, default: false },
    // additional items
    taskTools: { type: String, trim: true },
    relatedTasks: Array,
    notes: [noteSchema],
    requiresAdditionalInfo: { type: Boolean, default: false },
    requiresAdditionalInfoForm: additionalInfoForm,
    requiresAdditionalInfoSubmissions: additionalInfoSubmissions,
    // Highlighted attachment
    highlightedAttachment: Object,
    // QA Specific
    soc: Boolean, // Scope of Compliance
    oqe: Boolean, // Objective Quality Evidence
  },
  { timestamps: true }
)

taskSchema.plugin(mongoTenant)

taskSchema.statics.getCategoryCounts = function() {
  return this.aggregate([{ $unwind: '$category' }, { $group: { _id: '$category', count: { $sum: 1 } } }])
}

const Task = mongoose.model('Task', taskSchema)


// Forces an index resync to remove unique form the custom ID Field, 
// this was to fix a bug that was making an error on duplicate cuatomTaskID when it wasn't set at all gitlab issue #616
async function indexUpdate(){
  await Task.syncIndexes()
}
indexUpdate()
//End of bog fix mod

module.exports = Task
