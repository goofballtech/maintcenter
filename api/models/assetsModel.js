const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const locationObj = new mongoose.Schema(
  {
    enteredBy: { type: String, trim: true },
    requestedBy: { type: String, trim: true },
    description: { type: String, trim: true },
    dateMovedTo: Date,
    dateMovedFrom: Date,
  },
  { timestamps: true }
)

const transferRequest = new mongoose.Schema(
  {
    enteredBy: { type: String, trim: true },
    requestedBy: { type: String, trim: true },
    requestedDate: Date,
    expectedReturn: Date,
  },
  { timestamps: true }
)

const rmaInfo = new mongoose.Schema(
  {
    enteredBy: { type: String, trim: true },
    rmaNumber: { type: String, trim: true },
    rmaContact: { type: String, trim: true },
    address: { type: String, trim: true },
    dateSent: Date,
    expectedReturn: Date,
    actualReturn: Date,
  },
  { timestamps: true }
)

const notes = new mongoose.Schema(
  {
    enteredBy: { type: String, trim: true },
    note: { type: String, trim: true },
  },
  { timestamps: true }
)

const parts = new mongoose.Schema(
  {
    enteredBy: { type: String, trim: true },
    partID: { type: String, trim: true },
    note: { type: String, trim: true },
    qty: Number,
  },
  { timestamps: true }
)

const assetSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    assetID: {
      type: String,
      unique: 'Asset ID must be unique',
      trim: true,
    },
    name: { type: String, trim: true },
    description: { type: String, trim: true },
    manufacturer: { type: String, trim: true },
    model: { type: String, trim: true },
    serialNumber: { type: String, trim: true },
    assetCost: { type: String, trim: true },
    accountableContractNumber: { type: String, trim: true },
    postingReference: { type: String, trim: true },
    postingDate: { type: Date, trim: true },
    disposition: { type: String, trim: true },
    createdBy: { type: String, trim: true },
    lastEditedBy: { type: String, trim: true },
    location: [locationObj],
    transferRequest: [transferRequest],
    rma: [rmaInfo],
    notes: [notes],
    relatedParts: [parts],
    category: [String],
    relatedOrders: [String],
    relatedAssets: [String],
    relatedToDos: [String],
    relatedTasks: [String],
    relatedHoses: [String],
    dateAcquired: Date,
    acquiredDateEstimated: Boolean,
    dateInService: Date,
    inServiceDateEstimated: Boolean,
  },
  { timestamps: true }
)

assetSchema.plugin(mongoTenant)

const Asset = mongoose.model('Asset', assetSchema)

module.exports = Asset
