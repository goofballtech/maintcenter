const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const meterPastSchema = new mongoose.Schema(
  {
    techName: String,
    reading: Number,
  },
  { timestamps: true }
)

const meterSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true, unique: 'That meter name has already been used in the database' },
    type: { type: String, trim: true },
    currentReading: { type: Number, default: 0, required: true },
    pastReadings: [meterPastSchema],
  },
  { timestamps: true }
)
meterSchema.plugin(mongoTenant)

const Meters = mongoose.model('Meters', meterSchema)

module.exports = Meters
