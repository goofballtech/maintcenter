const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const prevSchema = new mongoose.Schema(
  {
    taskID: { type: Number, required: true },
    customTaskID: String,
    task_id: { type: String, required: true },
    techName: { type: String, required: true, trim: true },
    taskName: { type: String, required: true, trim: true },
    hourCompleted: Number,
    dateCompleted: Date,
    // store attachment name for process rev information
    attachmentNames: Array,
    // for auto emails
    alreadyAutoExported: { type: Boolean, default: false },
  },
  { timestamps: true }
)

prevSchema.plugin(mongoTenant)

const Prev = mongoose.model('PreviousCompletion', prevSchema)

module.exports = Prev
