const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const logSchema = new mongoose.Schema(
  {
    entryNumber: { type: Number, unique: 'Entry number must be unique' },
    type: Array,
    category: Array,
    title: {
      type: String,
      trim: true,
      required: 'A title for the entry is required',
    },
    note: {
      type: String,
      trim: true,
      required: 'A note for the entry is required',
    },
    dateFrom: Date,
    dateTo: Date,
    techName: { type: String, trim: true },
    lastEditedBy: { type: String, trim: true },
  },
  { timestamps: true }
)

logSchema.plugin(mongoTenant)

const Logs = mongoose.model('Log', logSchema)

module.exports = Logs
