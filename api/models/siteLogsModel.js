const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const baseSiteLogs = new mongoose.Schema({
  level: String,
  message: String,
  timestamp: {
    type:Date, 
    default: Date.now,
    index: { expires: '2y' },
  },
  meta: Object,
})

baseSiteLogs.plugin(mongoTenant)

const siteLog = mongoose.model('siteLog', baseSiteLogs)

module.exports = siteLog
