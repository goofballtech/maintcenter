const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const todoSchema = new mongoose.Schema(
  {
    name: { type: String, trim: true },
    idNumber: { type: Number, unique: true },
    dueDate: Date,
    priority: { type: Number, default: 1 },
    description: { type: String, trim: true },
    group: { type: String, default: '', trim: true },
    relatedOrders: Array,

    active: { type: Boolean, default: true },
    notes: { type: String, trim: true },
    enteredBy: { type: String, trim: true },
    lastEditedBy: { type: String, trim: true },
    completionDate: Date,
    doneBy: { type: String, trim: true },
    removedBy: { type: String, trim: true },
    removedDate: Date,
  },
  { timestamps: true }
)

todoSchema.plugin(mongoTenant)

const Todos = mongoose.model('Todo', todoSchema)

module.exports = Todos
