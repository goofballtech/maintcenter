const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const hoseSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    tagNumber: { type: Number },
    customTag: { type: String, trim: true },
    hardware: { type: String, trim: true },
    location: { type: String, trim: true },
    description: { type: String, trim: true },

    hoseSize: Number,
    hoseLength: Number,
    hoseOINumber: { type: String, trim: true },

    hosePressureRating: Number,
    hosePartNumber: { type: String, trim: true },
    sideAFitting: { type: String, trim: true },
    sideBFitting: { type: String, trim: true },

    lastInspection: Date,
    inspectionInterval: { type: Number },
    nextInspection: Date,

    lastChange: Date,
    changeInterval: { type: Number },
    nextChange: Date,
  },
  { timestamps: true }
)

hoseSchema.plugin(mongoTenant)

const Hose = mongoose.model('Hose', hoseSchema)

module.exports = Hose
