const mongoose = require('mongoose')
const mongoTenant = require('mongo-tenant')

const Vendors = new mongoose.Schema({
  vendorName: { type: String, trim: true },
  vendorPartNumber: { type: String, trim: true },
  quantity: { type: Number, default: 1 },
  pendingBy: { type: String, trim: true },
  isPending: { type: Boolean, default: false },
})

const toOrderSchema = new mongoose.Schema(
  {
    active: { type: Boolean, default: true },
    name: { type: String, trim: true },
    description: { type: String, trim: true },
    quantity: { type: Number, default: 1 },
    neededBy: { type: Date },
    priority: { type: Number, default: 1 },
    linkedInventoryItem: { type: String, trim: true },

    orderNumber: { type: String, trim: true },
    tasksCurrentlyLinked: Array,
    todosCurrentlyLinked: Array,

    vendor: [Vendors],
    category: Array,
    notes: { type: String, trim: true },

    enteredBy: { type: String, trim: true },
    enteredDate: Date,
    modifiedBy: { type: String, trim: true },
    modifiedDate: Date,
    pendingBy: { type: String, trim: true },
    isPending: Boolean,
    orderedBy: { type: String, trim: true },
    orderedDate: Date,
    removedBy: { type: String, trim: true },
    removedDate: Date,

    // for auto emails
    alreadyAutoExported: { type: Boolean, default: false },
  },
  { timestamps: true }
)

toOrderSchema.plugin(mongoTenant)

const ToOrder = mongoose.model('ToOrder', toOrderSchema)

module.exports = ToOrder
