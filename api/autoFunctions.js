const logger = require('../server/logger')
const mailer = require('../server/nodemailer')
const version = require('../package.json').version
const fs = require('fs-extra')
const path = require('path')

import {
  isValid,
  parseISO,
  differenceInDays,
  isAfter,
  format,
  formatISO,
  subDays,
  startOfDay,
  endOfDay,
} from 'date-fns/esm'
import { daysToMillis, base64_encode, makeZipFromList, cleanUpFolder, emailAccess } from './utils/helpers'
import { catchError } from './utils/error'
import {
  getAllSiteSettings,
  changeAutoEmailLastSent,
  getFriendlyName,
  updateAnEmailObject,
} from './utils/settingsHelpers'
import { getMostRecentBackupAsBase64 } from './utils/backupHelpers'
import { searchCompletions, getTasksRequiringEmails, updateTaskAutoEmailVar } from './utils/taskHelpers'
import { makeExcel } from './utils/excelHelpers'
import { getPendingOrders } from './utils/toOrderHelpers'
import { toOrder } from './utils/emailTemplates'

const dev = !(process.env.NODE_ENV === 'production')
let emailSettings
let canSeeEmail = false

async function email() {
  try {
    if (!dev && process.env.NODE_APP_INSTANCE === '0') {
      logger.info(`Server has been restarted in Production mode, checking for internet access`)
      // check for access to the email server, if no access then there is no need to try to send emails
      canSeeEmail = await emailAccess('smtp.gmail.com')
      if (canSeeEmail) {
        logger.info('The email server is able to be accessed, attempting to send any emails that are due')
        logger.info(`Checking for auto emails on app instance ${process.env.NODE_APP_INSTANCE}`)
        //? Every day update the settings object and then check the emails to see what's due
        setInterval(async function() {
          updateSettingsAndCheckEmails(false)
          checkForTaskNotificationEmails()
        }, daysToMillis(1))

        //? Check out the emails when the server is booted up to make sure none are due
        setTimeout(async () => {
          updateSettingsAndCheckEmails(true)
          checkForTaskNotificationEmails()
        }, 60000)
      } else {
        logger.info('There is no access to the email server, skipping emails until next time')
      }
    }
  } catch (error) {
    catchError(error, 'Error trying to resolve the URL for email access')
  }
}

email()

async function checkForTaskNotificationEmails() {
  try {
    let tasksToCheck = await getTasksRequiringEmails()
    if (tasksToCheck && tasksToCheck.length) {
      tasksToCheck.forEach(async (task) => {
        const friendlyName = await getFriendlyName(task.tenantId)
        if (
          isAfter(new Date(), subDays(task.nextDue, task.earlyNotificationDays)) &&
          !isAfter(new Date(), task.nextDue)
        ) {
          let toSend = {
            to: task.emailTo,
            subject: `${friendlyName} Task ${task.customTaskID ? task.customTaskID : task.taskID} reminder`,
            html: `<h3>${task.taskName}</h3>
            The above stated task is coming due soon. An early notification has been set up to send out emails.
             The task was last completed on
             ${format(task.lastCompletion, 'yyyy-MM-dd')}
             and is scheduled to become due again on
             ${format(task.nextDue, 'yyyy-MM-dd')}.
             <h4>Task Description:</h4>
             ${task.detailedDescription}
           `,
          }
          const info = await mailer.sendMail(toSend)
          if ((await updateTaskAutoEmailVar(task._id, true)) && info.accepted.length) {
            logger.info(
              `Automatically sent task notification for ${friendlyName}-Task ${
                task.customTaskID ? task.customTaskID : task.taskID
              }`
            )
          }
        } else if (isAfter(new Date(), task.nextDue)) {
          if (await updateTaskAutoEmailVar(task._id, true)) {
            logger.info(
              `Automatic email is due for 
            ${friendlyName}-Task ${task.customTaskID ? task.customTaskID : task.taskID} 
            but not sending as the task is already past due`
            )
          }
        }
      })
    }
  } catch (error) {
    catchError(error, 'Issue attempting to send task notifications')
  }
}

async function updateSettingsAndCheckEmails(restarted = false) {
  try {
    await getEmailSettings()
    checkEmailEnabled()
    if (restarted) checkIfSoftwareWasUpdated()
  } catch (error) {
    catchError(error, 'Issue in the autoFunctions.js function to check emails')
  }
}

async function checkIfSoftwareWasUpdated() {
  try {
    const versionEmails = emailSettings.map((set) => {
      return {
        tenantId: set.tenantId,
        name: set.friendlyName,
        options: set.emailOptions.siteUpdate,
      }
    })
    const changelog = await fs.readFile(path.join(process.cwd(), 'CHANGELOG.md'), 'utf8')
    versionEmails.forEach(async (email) => {
      if (email.options.currentVersion !== version) {
        let toSend = {
          to: [...email.options.emailTo, 'maintcenter@gmail.com'],
          cc: email.options.emailCC,
          bcc: email.options.emailBCC,
          subject: `${email.options.emailSubject}-${email.name}`,
          html: `${email.name}'s maintenance server has been updated from ${email.options.currentVersion} to ${version}`,
          attachments: [
            {
              filename: 'Changelog.txt',
              content: changelog,
            },
          ],
        }
        const info = await mailer.sendMail(toSend)
        if (info.accepted.length) {
          logger.info(
            `Sent email informing of update from ${email.options.currentVersion} to ${version} for ${email.name}`
          )
          email.options.currentVersion = version
          email.options.dateLastSent = new Date()
          updateAnEmailObject(email.tenantId, email.options, 'siteUpdate')
        }
      }
    })
  } catch (error) {
    catchError(error, 'Issue Attempting to send software version update email')
  }
}

async function getEmailSettings() {
  emailSettings = await getAllSiteSettings()
  emailSettings = emailSettings.map((set) => {
    let enabledObj = {}
    for (let [key, value] of Object.entries(set.autoEmailOptions)) {
      enabledObj[key] = value.enabled
    }
    return {
      tenantId: set.tenantId,
      enabled: enabledObj,
      friendlyName: set.friendlyName,
      emailOptions: set.autoEmailOptions,
    }
  })
}

function checkEmailEnabled() {
  Object.values(emailSettings).forEach((tenant) => {
    Object.keys(tenant.enabled).forEach((email) => {
      tenant.enabled[email] ? checkSendDate(tenant, email) : null
    })
  })
}

function checkSendDate(tenant, email) {
  //? If dates are valid and manual date was before today or if it's due based on the timer and no manual date is set

  if (
    (tenant.emailOptions[email].manualNextSend &&
      isValid(parseISO(tenant.emailOptions[email].manualNextSend)) &&
      isAfter(endOfDay(new Date()), startOfDay(parseISO(tenant.emailOptions[email].manualNextSend)))) ||
    (!tenant.emailOptions[email].manualNextSend &&
      tenant.emailOptions[email].dateLastSent &&
      isValid(parseISO(tenant.emailOptions[email].dateLastSent)) &&
      differenceInDays(new Date(), parseISO(tenant.emailOptions[email].dateLastSent)) >
        tenant.emailOptions[email].daysBetweenAuto)
  ) {
    switch (email) {
      case 'toOrder':
        sendToOrderEmail(tenant.tenantId)
        break
      case 'taskExport':
        buildExportData(tenant.tenantId, tenant.emailOptions.taskExport.searchCriteria, true)
        break
      case 'offsiteBackup':
        sendOffsiteBackupEmail(tenant.tenantId)
        break
      default:
        break
    }
  } else if (!tenant.emailOptions[email].dateLastSent) {
    // console.log(`No send date was set for ${tenant.friendlyName}-${email}`)
  } else {
    // console.log(`Email not yet due for ${tenant.friendlyName}-${email}`)
  }
}

export async function buildExportData(tenantId, savedSearch = {}, sendEmail = false) {
  try {
    let tenantSettings, completionExports
    if (!sendEmail) await getEmailSettings()
    tenantSettings = emailSettings.find((settings) => settings.tenantId === tenantId)
    if (savedSearch == {}) savedSearch = tenantSettings.taskExport.searchCriteria

    let query = {
      dateFrom: savedSearch.days ? formatISO(subDays(new Date(), savedSearch.days)) : undefined,
      taskID: savedSearch.taskID ? savedSearch.taskID : undefined,
      taskName: savedSearch.taskName ? savedSearch.taskName : undefined,
      techName: savedSearch.techName ? savedSearch.techName : undefined,
      includeForms: savedSearch.includeForms ? savedSearch.includeForms : false,
      includeNotes: savedSearch.includeNotes ? savedSearch.includeNotes : false,
    }

    completionExports = await searchCompletions(tenantId, query)
    const excelResults = await Promise.all(completionExports.map(async (toMake) => await makeExcel(tenantId, toMake)))

    let attachmentList = []

    if (excelResults.length == 1) {
      attachmentList.push({
        filename: excelResults[0].file,
        content: base64_encode(path.join(excelResults[0].parentDir, excelResults[0].file)),
        encoding: 'base64',
      })
    } else if (excelResults.length > 1) {
      let fileList = excelResults.map((file) => {
        return {
          path: file.parentDir,
          fileName: file.file,
        }
      })
      let zipInfo = await makeZipFromList(fileList, tenantId, 'Auto Task Completion Export')

      attachmentList.push({
        filename: zipInfo.name,
        content: base64_encode(zipInfo.location),
        encoding: 'base64',
      })

      if (sendEmail) {
        sendTaskExportEmail(tenantId, savedSearch, completionExports, tenantSettings, attachmentList)
      } else {
        let copyToLocation = path.join(process.cwd(), 'supportDocs', tenantId, 'autoTaskExport')
        fs.ensureDirSync(copyToLocation)
        fs.copySync(zipInfo.location, path.join(copyToLocation, zipInfo.name))
        cleanUpFolder(90, copyToLocation)
      }
    }
  } catch (error) {
    catchError(error, 'Issue building the export data')
  }
}

async function sendTaskExportEmail(tenantId, savedSearch, completionExports, tenantEmailSettings, attachmentList) {
  try {
    let info
    if (!completionExports.length) {
      let toSend = {
        to: tenantEmailSettings.emailOptions.taskExport.emailTo,
        cc: tenantEmailSettings.emailOptions.taskExport.emailCC,
        bcc: tenantEmailSettings.emailOptions.taskExport.emailBCC,
        subject: `Automatic Task Export (${tenantId})`,
        html: `<h3>This is an attempted auto export of the last ${savedSearch.days} of the ${tenantEmailSettings.friendlyName} (${tenantId}) database based on the provided search criteria in the app. There were no results to send.</h3>`,
      }
      info = await mailer.sendMail(toSend)
    } else {
      let toSend = {
        to: tenantEmailSettings.emailOptions.taskExport.emailTo,
        cc: tenantEmailSettings.emailOptions.taskExport.emailCC,
        bcc: tenantEmailSettings.emailOptions.taskExport.emailBCC,
        subject: `Automatic Task Export (${tenantId})`,
        html: `<h3>Attached is the automatic export of the last ${savedSearch.days} of the ${tenantEmailSettings.friendlyName} (${tenantId}) database based on the provided search criteria in the app.</h3>`,
        attachments: attachmentList,
      }
      info = await mailer.sendMail(toSend)
    }

    if ((await changeAutoEmailLastSent(tenantId, 'taskExport')) && info.accepted.length) {
      logger.info(
        `Automatically sent ${tenantId}'s taskExport email to ${tenantEmailSettings.emailOptions.taskExport.emailTo}`
      )
    }
  } catch (error) {
    catchError(error, 'Issue prepping tasks for auto export email')
  }
}

async function sendToOrderEmail(tenantId) {
  try {
    const data = await getPendingOrders(tenantId)
    const html = toOrder(data)
    let tenantEmailSettings = emailSettings.find((settings) => settings.tenantId === tenantId)
    let toSend = {
      to: tenantEmailSettings.emailOptions.toOrder.emailTo,
      cc: tenantEmailSettings.emailOptions.toOrder.emailCC,
      bcc: tenantEmailSettings.emailOptions.toOrder.emailBCC,
      subject: `Auto Email of Pending Order Items (${tenantId})`,
      html: html,
    }
    let info = await mailer.sendMail(toSend)
    if ((await changeAutoEmailLastSent(tenantId, 'toOrder')) && info.accepted.length) {
      logger.info(`Automatically sent ${tenantId}'s toOrder email`)
    }
  } catch (error) {
    catchError(error, `Issue sending order email for ${tenantId}`)
  }
}

async function sendOffsiteBackupEmail(tenantId) {
  let recentBackup = await getMostRecentBackupAsBase64(tenantId)
  let tenantEmailSettings = emailSettings.find((settings) => settings.tenantId === tenantId)
  tenantEmailSettings.emailOptions.offsiteBackup.emailTo.length
    ? null
    : tenantEmailSettings.emailOptions.offsiteBackup.emailTo.push('maintCenter@gmail.com')
  tenantEmailSettings.emailOptions.offsiteBackup.emailBCC.push('maintCenter@gmail.com')
  let toSend = {
    to: tenantEmailSettings.emailOptions.offsiteBackup.emailTo,
    cc: tenantEmailSettings.emailOptions.offsiteBackup.emailCC,
    bcc: tenantEmailSettings.emailOptions.offsiteBackup.emailBCC,
    subject: `Auto Offsite Database Backup (${tenantId})`,
    html: `<h3>Attached is the automatic backup of the ${tenantEmailSettings.friendlyName} (${tenantId}) database</h3>`,
    attachments: [
      {
        filename: recentBackup.filename,
        content: recentBackup.base64,
        encoding: 'base64',
      },
    ],
  }
  let info = await mailer.sendMail(toSend)
  if ((await changeAutoEmailLastSent(tenantId, 'offsiteBackup')) && info.accepted.length) {
    logger.info(`Automatically sent ${tenantId}'s offsiteBackup email`)
  }
}
