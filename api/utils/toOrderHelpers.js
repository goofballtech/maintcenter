const baseToOrder = require('../models/toOrdersModel')

import { catchError } from '../utils/error'

export async function getPendingOrders(tenantId) {
  try {
    const ToOrder = baseToOrder.byTenant(tenantId)
    const orders = await ToOrder.find({ active: { $eq: true } })
    return orders
  } catch (error) {
    catchError(error, 'Issue grabbing orders for auto email')
  }
}