const fs = require('fs-extra')
const Zip = require('adm-zip')
const path = require('path')
import { format, subHours, addHours, parseISO, isValid } from 'date-fns/esm'
import logger from '../../server/logger'
import { catchError } from './error'

export function base64_encode(file) {
  // read binary data
  const fileRead = fs.readFileSync(file)
  // convert binary data to base64 encoded string
  return new Buffer.from(fileRead).toString('base64')
}

// offsets a given time as string or date object by the provided number of hours
// invert allows this to be flipped depending on which direction the time shift should occur
// this offset depends on the time zone setting of the server,
// if it's not set to zulu the offsets may be different than expected
export function offsetDateByTimezone(date, offset = 0, invert = false) {
  invert ? (offset = offset * -1) : null
  const offsetInt = Math.abs(Math.floor(offset))
  if (isValid(parseISO(date))) {
    if (offset > 0) {
      return subHours(parseISO(date), parseInt(offsetInt))
    } else if (offset < 0) {
      return addHours(parseISO(date), parseInt(offsetInt))
    } else {
      return date
    }
  } else if (isValid(date)) {
    if (offset > 0) {
      return subHours(date, parseInt(offsetInt))
    } else if (offset < 0) {
      return addHours(date, parseInt(offsetInt))
    } else {
      return date
    }
  } else {
    return date
  }
}

export function removeElemWithIdAndValue(arr = [], targetId, targetValue) {
  const index = arr.map((e) => e[targetId]).indexOf(targetValue)
  if (index >= 0) arr.splice(index, 1)
  return arr
}

export async function makeZip(basePath, folder, requestedFileName, encode = false) {
  const checkExists = await fs.pathExists(path.join(basePath, folder))
  if (checkExists) {
    const options = {
      mode: 0o2775, // file/folder creation mode
    }

    let downloadPath = path.join(basePath, 'downloads')

    await fs.ensureDir(downloadPath, options)
    let outputZip = new Zip()
    let filesToZip = await fs.readdir(path.join(basePath, folder))
    filesToZip.forEach((file) => {
      outputZip.addLocalFile(path.join(basePath, folder, file))
    })

    outputZip.writeZip(path.join(basePath, 'downloads', `${requestedFileName}.zip`))

    let base64Encoded
    if (encode) {
      base64Encoded = base64_encode(path.join(basePath, 'downloads', `${requestedFileName}.zip`))
    }

    setTimeout(() => fs.unlink(path.join(basePath, 'downloads', `${requestedFileName}.zip`)), 1000 * 60 * 15) // 15 minute timeout until the zip file self deletes

    return {
      filename: `${requestedFileName}.zip`,
      path: path.join(basePath, 'downloads'),
      base64: encode ? base64Encoded : undefined,
    }
  } else {
    return false
  }
}

export async function makeZipFromList(files, db, outputName = 'Zipped File Set') {
  //? files object expects an array of objects in the following form
  //? {
  //?   path: '/app/supportDocs/things',
  //?   filename: 'something.xlsx',
  //? }
  // eslint-disable-next-line no-async-promise-executor
  return new Promise(async (resolve, reject) => {
    try {
      if (files.length) {
        let zippedFiles = new Zip()
        if (files.length) {
          files.forEach((file) => {
            if (!fs.existsSync(path.join(file.path, file.fileName))) {
              return "One or more files in the list doesn't exist"
            } else {
              zippedFiles.addLocalFile(path.join(file.path, file.fileName))
            }
          })
        }

        let zipFile = path.join(
          process.cwd(),
          'supportDocs',
          db,
          'downloads',
          `${outputName}_${format(new Date(), 'yyyy-MM-dd')}.zip`
        )

        zippedFiles.writeZip(zipFile)
        setTimeout(function() {
          fs.unlink(zipFile)
        }, 1000 * 60 * 15) // 15 minute timeout until the zip file self deletes

        resolve({
          location: zipFile,
          name: `${outputName}_${format(new Date(), 'yyyy-MM-dd')}.zip`,
        })
      } else {
        reject('No file list provided')
      }
    } catch (error) {
      logger.error(`Error making zip from list: ${error}`)
    }
  })
}

//? pull duplicate objects from an array of objects, pass in array and the property to use as the comparitor
export function removeDuplicates(originalArray, prop) {
  if (Array.isArray(originalArray) && typeof prop === 'string') {
    var newArray = []
    var lookupObject = {}
    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i]
    }
    for (i in lookupObject) {
      newArray.push(lookupObject[i])
    }
    return newArray
  } else {
    return []
  }
}

// Clean up any files in provided folder that are older than time limit
export function cleanUpFolder(timeLimitInDays, folderToClean) {
  fs.readdir(folderToClean, (err, files) => {
    if (err) {
      logger.error('Error reading folder:', err)
      return
    }

    files.forEach((file) => {
      const filePath = path.join(folderToClean, file)
      fs.stat(filePath, (err, stats) => {
        if (err) {
          logger.error('Error getting file stats:', err)
          return
        }

        const now = new Date().getTime()
        const fileAge = now - stats.mtime.getTime()

        if (fileAge > daysToMillis(timeLimitInDays)) {
          fs.unlink(filePath, (err) => {
            if (err) {
              logger.error('Error deleting file:', err)
            } else {
              logger.log('Deleted:', filePath)
            }
          })
        }
      })
    })
  })
}

export function daysToMillis(days) {
  return 1000 * 60 * 60 * 24 * days
}

export function checkJSON(string) {
  if (typeof string === 'string') {
    try {
      JSON.parse(string)
      return true
    } catch (error) {
      return false
    }
  }
  return false
}

export async function getAllVendors(tenantId) {
  try {
    const baseToOrder = require('../models/toOrdersModel')
    const baseInventory = require('../models/inventoryPartsModel')

    let orderVendors = await baseToOrder.find({ tenantId: tenantId }).distinct('vendor.vendorName')
    let itemVendors = await baseInventory.find({ tenantId: tenantId }).distinct('partNumber.vendor')

    let mergedVendors = [...orderVendors, ...itemVendors]
    let vendorSet = [...new Set(mergedVendors)]

    let vendors = Array.from(vendorSet)
    vendors.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
    vendors.splice(0, 0, '', 'Oceaneering')

    return vendors
  } catch (error) {
    catchError(error, 'Issue getting vendor list from Orders')
  }
}

//? The below will timeout after a minute so with no access the fail message may be delayed by up to a minute
export function emailAccess(url) {
  return new Promise((resolve, reject) => {
    try {
      let dns = require('dns')
      let connected = false
      dns.resolve(url, (err, records) => {
        if (records && records.length) {
          connected = true
        } else if (err) {
          //? Only used for diagnostics. Error kicks if the resolve timeouts out due to no internet, removing to not clutter the log
          // catchError(err, 'checking for URL timed out, likely no access')
          connected = false
        } else {
          connected = false
        }
        resolve(connected)
      })
    } catch (error) {
      reject(error)
    }
  })
}
