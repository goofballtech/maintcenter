const mongoose = require('mongoose')
const fs = require('fs-extra')
const logger = require('../../server/logger')
const path = require('path')

import { format, parseISO, max } from 'date-fns/esm'
import { isAfter } from 'date-fns'
import { makeZip } from '../utils/helpers'
import { buildExportData } from '../autoFunctions'

const baseDir = path.join(process.cwd(), 'supportDocs')

export async function createBackup(dbName, maxBackups, req = false, res = false) {
  let collectionsToBackup = []

  if (!dbName || !maxBackups) {
    logger.error(
      JSON.stringify(
        'A backup creation was attempted but a database name was not specified or the max backups was not specified'
      )
    )
  } else {
    const baseBackupPath = path.join(baseDir, dbName, 'backups')
    const newBackupPath = path.join(baseDir, dbName, 'backups', format(new Date(), 'yyyy-MM-dd'))
    try {
      const pathExists = await fs.pathExists(baseBackupPath)

      if (pathExists) {
        let backupFolders = await fs.readdir(baseBackupPath)

        if (backupFolders.length) {
          if (isAfter(parseISO(backupFolders[0]), new Date())) {
            logger.error(
              JSON.stringify(
                `The date on the server is set before the oldest backup. ServerDate: ${new Date().toISOString()}`
              )
            )
            return res
              .status(200)
              .json(`The date on the server is before existing backups. ServerDate: ${new Date().toISOString()}`)
          }
        }

        if (backupFolders.length > maxBackups) {
          let numToRemove = backupFolders.length - maxBackups
          for (let i = 0; i < numToRemove; i++) {
            fs.remove(path.join(baseBackupPath, backupFolders[i]))
          }
        }
      } else {
        if (fs.ensureDirSync(baseBackupPath)) {
          logger.info(`Backup dir created ${baseBackupPath}`)
        }
      }

      let list = await mongoose.connection.db.listCollections().toArray()
      list = list.map((collection) => collection.name)
      const modelFiles = await fs.readdir('./api/models')
      list.forEach((collection) => {
        if (collection !== 'sessions' && collection !== 'oiparts') {
          // pull off the "Model.js" from each file and compare that to the database name
          const path = modelFiles.find((file) => file.substr(0, file.length - 8).toLowerCase() === collection)
          if (path) {
            if (collection == 'users') {
              collectionsToBackup.push({
                collectionName: collection,
                object: require('../models/usersModel'),
              })
            } else if (collection == 'sitelogs') {
              collectionsToBackup.push({
                collectionName: collection,
                object: require(`../models/${path}`),
              })
            } else {
              collectionsToBackup.push({
                collectionName: collection,
                object: require(`../models/${path}`).byTenant(dbName),
              })
            }
          }
        }
      })

      // TODO Check the below
      const baseSettings = require('../models/settingsModel')
      const Settings = baseSettings.byTenant(req.params.db)
      const settingsObj = await Settings.find()
      // console.log(settingsObj[0].autoEmailOptions.taskExport.searchCriteria)
      buildExportData(dbName, settingsObj[0].autoEmailOptions.taskExport.searchCriteria)
    } catch (error) {
      logger.error(
        JSON.stringify({
          details: error,
          url: req.originalUrl || 'Auto Backup',
          method: req.method || 'Auto Backup',
        })
      )
    }

    let backupData = {
      techName: req.query.techName || 'system',
      backupDate: format(new Date(), 'yyyy-MM-dd'),
      tenantId: req.params.db,
      collectionEntries: {},
    }
    try {
      let data = await Promise.all(collectionsToBackup.map((collection) => collection.object.find({})))

      data.forEach((item, index) => {
        backupData.collectionEntries[collectionsToBackup[index].collectionName] = item.length
        if (makeJSONfile(newBackupPath, collectionsToBackup[index].collectionName, item)) {
          logger.info(JSON.stringify(`${dbName} - ${collectionsToBackup[index].collectionName} Backup Success`))
        } else {
          logger.info(JSON.stringify(`${dbName} - ${collectionsToBackup[index].collectionName} Backup Error`))
        }
      })

      if (makeJSONfile(newBackupPath, 'dbInfo', backupData)) {
        logger.info(JSON.stringify(`${dbName} - Data file created for backup`))
      } else {
        logger.info(JSON.stringify(`${dbName} - Error creating data file creating for backup`))
      }
      if (res) {
        logger.info(JSON.stringify(`${dbName} - Manual Backup Created`))
        return res.status(200).json(`Backup Created`)
      } else {
        logger.info(JSON.stringify('Automatic Backup has been created'))
      }
    } catch (err) {
      logger.error(
        JSON.stringify({
          details: err,
          url: req.originalUrl || 'Auto Backup',
          method: req.method || 'Auto Backup',
        })
      )
      return res.status(400).json('An error has occurred, see log for details')
    }
  }
}

async function makeJSONfile(jsonPath, fileName, data) {
  try {
    fs.outputJsonSync(path.join(jsonPath, `${fileName}.json`), data, { spaces: 2 })
    return true
  } catch (err) {
    logger.error(
      JSON.stringify({
        details: err,
        path: path.join(jsonPath, fileName),
      })
    )
    return false
  }
}

export async function getMostRecentBackupAsBase64(tenantId) {
  const backupPathBase = path.join(baseDir, tenantId, 'backups')
  const pathExists = await fs.pathExists(backupPathBase)
  if (pathExists) {
    const folders = await fs.readdir(backupPathBase)
    const mostRecentDate = format(
      max(folders.filter((date) => (date === 'downloads' ? false : true)).map((date) => parseISO(date))),
      'yyyy-MM-dd'
    )
    const zipObj = makeZip(backupPathBase, mostRecentDate, `${mostRecentDate}_${tenantId}_backup`, true)
    return zipObj
  } else {
    return false
  }
}
