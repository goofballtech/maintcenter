import { catchError } from './error'

const baseSettings = require('../models/settingsModel')

export async function getAllSiteSettings() {
  try {
    let allSettings = await baseSettings.find()
    return JSON.parse(JSON.stringify(allSettings))
  } catch (error) {
    catchError(error, `Error getting all settings objects`)
  }
}

export async function getFriendlyName(tenantId) {
  try {
    const settingsObject = await baseSettings.findOne({ tenantId: tenantId })
    return settingsObject.friendlyName
  } catch (error) {
    catchError(error, `Error getting site friendly name`)
  }
}

export async function changeAutoEmailLastSent(tenantId, email, date = new Date()) {
  try {
    if (tenantId && email) {
      const Settings = baseSettings.byTenant(tenantId)
      const manualDateLocation = `autoEmailOptions.${email}.manualNextSend`
      const lastDateLocation = `autoEmailOptions.${email}.dateLastSent`
      await Settings.findOneAndUpdate(
        {},
        { $unset: { [manualDateLocation]: '' }, $set: { [lastDateLocation]: date } },
        { runValidators: true, new: true }
      )
      return true
    } else {
      return false
    }
  } catch (error) {
    catchError(error, `Error in settingsRoute trying to change dateLastSent for ${tenantId}-${email}`)
    return false
  }
}

export async function updateAnEmailObject(db, updatedObject, objectName) {
  const Settings = baseSettings.byTenant(db)
  return await Settings.findOneAndUpdate(
    {},
    { $set: { [`autoEmailOptions.${objectName}`]: updatedObject } },
    { runValidators: true, new: true }
  )
}
