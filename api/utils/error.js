const logger = require('../../server/logger')

export function catchError(systemError, errorMessage = '') {
  logger.error(
    JSON.stringify({
      details: systemError,
      userMessage: errorMessage ? errorMessage : undefined,
    })
  )
}
