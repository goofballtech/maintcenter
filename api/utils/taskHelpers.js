const logger = require('../../server/logger')

const baseTask = require('../models/tasksModel')
const basePrev = require('../models/previousCompletionsModel')

import { parseISO, isAfter, isBefore, startOfDay, endOfDay, isDate, format } from 'date-fns/esm'

export async function searchCompletions(tenantId, query = {}) {
  try {
    const Prev = basePrev.byTenant(tenantId)
    let excelCreation = []
    let searchObject = {}
    const prevOutputColumnsForExcel = [
      {
        key: 'taskID',
        header: 'ID',
        width: 10,
      },
      {
        key: 'taskName',
        header: 'Task Name',
        width: 40,
      },
      {
        key: 'dateCompleted',
        type: 'date',
        header: 'Date Completed',
        width: 15,
      },
      {
        key: 'hourCompleted',
        header: 'Hour Completed',
        width: 15,
      },
      {
        key: 'techName',
        header: 'Tech Name',
        width: 15,
      },
      {
        key: 'attachmentNames',
        header: 'Attachments',
        width: 40,
      },
    ]

    if (query.dateFrom && query.dateTo) {
      searchObject.dateCompleted = {
        $gte: startOfDay(parseISO(query.dateFrom)),
        $lte: endOfDay(parseISO(query.dateTo)),
      }
    } else if (query.dateFrom && !query.dateTo) {
      searchObject.dateCompleted = {
        $gte: startOfDay(parseISO(query.dateFrom)),
      }
    } else if (!query.dateFrom && query.dateTo) {
      searchObject.dateCompleted = {
        $lte: endOfDay(parseISO(query.dateTo)),
      }
    }

    query.techName
      ? (searchObject.techName = {
          $regex: `.*${query.techName}.*`,
          $options: 'i',
        })
      : undefined

    if (query.taskID) {
      searchObject.taskID = query.taskID
    } else {
      if (query.taskName) {
        searchObject.taskName = {
          $regex: `.*${query.taskName}.*`,
          $options: 'i',
        }
      }
      if (query.category) {
        let tasksIncludingCategory = await baseTask.byTenant(tenantId).find({
          category: {
            $in: query.category.split(','),
          },
        })
        searchObject.task_id = {
          $in: tasksIncludingCategory.map((task) => task._id),
        }
      }
    }

    const completionsList = await Prev.find(searchObject)

    if (query.tableData) return completionsList

    excelCreation.push({
      filename: `Task completion export ${format(new Date(), 'yyyy-MM-dd')}${tenantId ? '-' + tenantId : ''}`,
      worksheetTitle: 'Previous Completions',
      columns: prevOutputColumnsForExcel,
      data: completionsList,
    })

    if (query.includeNotes) {
      const Task = baseTask.byTenant(tenantId)
      let justIdList = completionsList.map((item) => item.task_id)

      const noteSheetColumns = [
        {
          key: 'taskID',
          header: 'ID',
          width: 10,
        },
        {
          key: 'techName',
          header: 'Tech Name',
          width: 20,
        },
        {
          key: 'note',
          header: 'Note',
          width: 40,
        },
        {
          key: 'date',
          type: 'date',
          header: 'Date',
          width: 15,
        },
      ]

      const noteList = await Task.find({
        _id: {
          $in: justIdList,
        },
      })

      // take the tasks object and drop them to just the notes then flatten that
      let excelNoteList = noteList
        .map((task) => {
          return {
            taskID: task.customTaskID ? task.customTaskID : task.taskID,
            notes: task.notes,
          }
        })
        .flatMap(({ taskID, notes }) =>
          notes.map((note) => ({
            taskID: taskID,
            techName: note.techName,
            note: note.note,
            date: note.manualDate ? note.manualDate : note.enteredDate ? note.enteredDate : note.createdAt,
          }))
        )

      excelCreation.push({
        filename: `Task Notes export ${format(new Date(), 'yyyy-MM-dd')}${tenantId ? '-' + tenantId : ''}`,
        worksheetTitle: 'Task Notes',
        columns: noteSheetColumns,
        data: excelNoteList,
      })
    }

    if (query.includeForms) {
      let justIdList = completionsList.map((item) => item.task_id)
      justIdList = new Set(justIdList)
      let taskForms = await getFormData([...justIdList], tenantId, query.dateFrom, query.dateTo)
      if (taskForms.length) {
        taskForms.forEach((form) => {
          if (form.submissions && form.submissions.length) {
            form.submissions.forEach((formEntry) => {
              const date = format(formEntry[formEntry.length - 1].dateSubmitted, 'yyyy-MM-dd')
              const sheetTitles = formEntry.slice(0, formEntry.length - 1).map((entry) => entry[0].group)
              const formData = formEntry.slice(0, formEntry.length - 1).map((set) => {
                return {
                  [set[0].group]: {
                    columns: set.map((entry) => {
                      return {
                        header: entry.field,
                        key: entry.field,
                        width: 15,
                      }
                    }),
                    data: [
                      Object.assign(
                        {},
                        ...set.map((entry) => {
                          return {
                            [entry.field]: entry.value,
                          }
                        })
                      ),
                    ],
                  },
                }
              })
              excelCreation.push({
                filename: `Task ${form.taskID} form submitted ${date} by ${formEntry[formEntry.length - 1].techName}`,
                worksheetTitle: sheetTitles,
                sheetDataSets: formData,
              })
            })
          }
        })
      }
    }
    return excelCreation
  } catch (error) {
    logger.error({
      message: 'An error has occurred trying to get a completions list',
      error: error,
    })
    return false
  }
}

async function getFormData(list, db, dateFrom, dateTo) {
  const Task = baseTask.byTenant(db)
  let submissionList = await Task.find({
    _id: { $in: list },
    'requiresAdditionalInfoSubmissions.formSubmissions.1': { $exists: true },
  })
  submissionList = submissionList.map((task) => {
    return {
      taskID: task.taskID,
      submissions: task.requiresAdditionalInfoSubmissions.formSubmissions.map((submission) => {
        if (submission[submission.length - 1].dateSubmitted || submission[submission.length - 1].createdAt) {
          let compareDate = submission[submission.length - 1].dateSubmitted
            ? submission[submission.length - 1].dateSubmitted
            : submission[submission.length - 1].createdAt
          if (isDate(new Date(compareDate))) {
            compareDate = new Date(compareDate)
          } else {
            compareDate = parseISO(compareDate)
          }
          submission[submission.length - 1] = {
            dateSubmitted: compareDate,
            techName: submission[submission.length - 1].techName,
          }
          return submission
        }
      }),
    }
  })
  if (dateTo || dateFrom) {
    submissionList = submissionList.map((task) => {
      if (task.submissions.length) {
        task.submissions = task.submissions.filter((submission) => {
          if (submission[submission.length - 1].dateSubmitted instanceof Date) {
            if (dateFrom && !dateTo) {
              return isAfter(endOfDay(submission[submission.length - 1].dateSubmitted), startOfDay(parseISO(dateFrom)))
            } else if (!dateFrom && dateTo) {
              return isBefore(startOfDay(submission[submission.length - 1].dateSubmitted), endOfDay(parseISO(dateTo)))
            } else if (dateFrom && dateTo) {
              return (
                isAfter(endOfDay(submission[submission.length - 1].dateSubmitted), startOfDay(parseISO(dateFrom))) &&
                isBefore(startOfDay(submission[submission.length - 1].dateSubmitted), endOfDay(parseISO(dateTo)))
              )
            } else {
              return true
            }
          }
        })
      }
      return task
    })
  }
  return submissionList
}

export async function getTasksRequiringEmails() {
  try {
    return await baseTask.find({
      earlyNotification: { $eq: true },
      'emailTo.0': { $exists: true },
      emailAlreadySent: { $eq: false },
      active: { $eq: true },
    })
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
      })
    )
    return false
  }
}

export async function updateTaskAutoEmailVar(_id, newVal) {
  try {
    await baseTask.findOneAndUpdate({ _id: _id }, { emailAlreadySent: newVal })
    return true
  } catch (error) {
    logger.error(
      JSON.stringify({
        details: error,
      })
    )
    return false
  }
}
