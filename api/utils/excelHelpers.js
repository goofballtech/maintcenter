const fs = require('fs-extra')
const excel = require('exceljs')
const path = require('path')

import { base64_encode, makeZip } from '../utils/helpers.js'
import { parseISO, format } from 'date-fns/esm'

const baseDir = path.join(process.cwd(), 'supportDocs')
const templateStorage = path.join(process.cwd(), 'static', 'download')

// Excel import object examples objects at bottom of file
//? https://github.com/exceljs/exceljs#create-a-workbook

export async function makeWorksheet(workbook, sheetData) {
  // prettier-ignore
  const alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  let dateColumns = []
  let listColumns = []

  let sheet = workbook.addWorksheet(sheetData.worksheetTitle || 'Maint Export')
  sheet.columns = sheetData.columns
  for (let i = 0; i < sheetData.columns.length; i++) {
    // right now more than 26 columns will cause the overflow headers to not be outlined
    sheet.getCell(`${alpha[i]}1`).border = {
      top: { style: 'medium' },
      left: { style: 'medium' },
      bottom: { style: 'medium' },
      right: { style: 'medium' },
    }
    if (sheetData.columns[i].type) {
      switch (sheetData.columns[i].type) {
        case 'date':
          dateColumns.push(sheetData.columns[i].key)
          break
        case 'list':
          listColumns.push(sheetData.columns[i].key)
          break
        default:
          break
      }
    }
  }
  sheetData.data.forEach(item => {
    dateColumns.forEach(dateVal => {
      if (item[dateVal] instanceof Date) {
        item[dateVal] = format(item[dateVal], 'yyyy-MM-dd')
      } else if (item[dateVal] && parseISO(item[dateVal]) !== 'Invalid Date') {
        item[dateVal] = format(parseISO(item[dateVal]), 'yyyy-MM-dd')
      }
    })

    listColumns.forEach(listVal => {
      if (item[listVal] && Array.isArray(item[listVal])) {
        item[listVal] = item[listVal].join(', ')
      }
    })

    sheet.addRow(item)
  })

  return sheet
}

export async function makeExcel(tenantId, sheetData, base64Encode = false) {
  return new Promise(async (resolve, reject) => {
    try {
      const exportDir = path.join(baseDir, tenantId, 'excel')
      const options = {
        mode: 0o2775, // file/folder creation mode
      }

      await fs.ensureDir(exportDir, options)

      let workbook = new excel.Workbook()
      workbook.properties.date1904 = true

      if (Array.isArray(sheetData.worksheetTitle)) {
        sheetData.worksheetTitle.forEach((sheet, i) => {
          makeWorksheet(workbook, {
            worksheetTitle: sheet,
            columns: sheetData.sheetDataSets[i][sheet].columns,
            data: sheetData.sheetDataSets[i][sheet].data,
          })
        })
      } else {
        makeWorksheet(workbook, sheetData)
      }

     resolve(await makeFile(workbook, exportDir, sheetData.filename, base64Encode))
    } catch (error) {
      reject(error)
    }
  })
}

export async function makePRExcel(tenantId, sheetData, base64Encode = false) {
  return new Promise(async (resolve, reject) => {
    try {
      const exportDir = path.join(baseDir, tenantId, 'excel')
      const options = {
        mode: 0o2775, // file/folder creation mode
      }

      await fs.ensureDir(exportDir, options)
      
      //? Build sheet(s) out
      if (sheetData.data.length > 0){
        //? There is only one sheet worth of items
        if(sheetData.data.length<27){
          //? Make the sheet and return it here to add line items
          let PRTemplate = await fillHeadersOnPR(sheetData)
          for(let i=0;i<sheetData.data.length;i++){
              let row = PRTemplate.sheet.getRow(22+i)
              row.getCell(PRTemplate.columns.qtyCol).value = sheetData.data[i].quantity?sheetData.data[i].quantity:1
              row.getCell(PRTemplate.columns.unitCol).value = sheetData.data[i].uom?sheetData.data[i].uom:'ea'
              row.getCell(PRTemplate.columns.partNumberCol).value = sheetData.data[i].partNumber?sheetData.data[i].partNumber:''
              row.getCell(PRTemplate.columns.descriptionCol).value = sheetData.data[i].description?sheetData.data[i].partNumber:''
              row.getCell(PRTemplate.columns.unitCostCol).value = sheetData.data[i].cost?sheetData.data[i].partNumber:0
            }
            resolve(await makeFile(PRTemplate.workbook, exportDir, sheetData.filename, base64Encode))
        } else {
          //? We need more than one PR to be filled out, work on them then zip em up to send back
          const numberOfSheets = Math.ceil(sheetData.data.length/27)
          let filledSheets = []
          for(let i=0; i<numberOfSheets; i++){
            //? cut some items from array, 27*0 and 27*(0+1)-1 end up being slice(0, 26) for the first 27 items, second one ends up starting at 27 and moving on from there
            let dataSubset = sheetData.data.slice(27*i, 27*(i+1))
            filledSheets.push(await fillHeadersOnPR(sheetData))
            for(let j=0;j<dataSubset.length;j++){
              let row = filledSheets[i].sheet.getRow(22+j)
              row.getCell(filledSheets[i].columns.qtyCol).value = dataSubset[j].quantity?dataSubset[j].quantity:1
              row.getCell(filledSheets[i].columns.unitCol).value = dataSubset[j].uom?dataSubset[j].uom:'ea'
              row.getCell(filledSheets[i].columns.partNumberCol).value = dataSubset[j].partNumber?dataSubset[j].partNumber:''
              row.getCell(filledSheets[i].columns.descriptionCol).value = dataSubset[j].description?dataSubset[j].partNumber:''
              row.getCell(filledSheets[i].columns.unitCostCol).value = dataSubset[j].cost?dataSubset[j].partNumber:0
            }
            await makeFile(filledSheets[i].workbook, exportDir, `${sheetData.filename}_${i+1}`, base64Encode)
          }
          let zip = await makeZip(path.join(baseDir, tenantId), 'excel', `PRSet_${sheetData.vendorName}`)
          resolve({
            success: true,
            parentDir: zip.path,
            file: zip.filename,
          })
        }
      }
    } catch (error) {
      reject(error)
    }
  })
}

async function fillHeadersOnPR(sheetData){
   return new Promise(async (resolve, reject) => {
    try{
      //? Make the workbook and sheet 1
      let workbook = new excel.Workbook()
      await workbook.xlsx.readFile(path.join(templateStorage,'PRTemplate.xlsx'))
      let sheet = workbook.getWorksheet('Req Form')

      //? set up cells by name for easy access then write the proper data to them
      sheet.getCell('F2').value = new Date()
      sheet.getCell('K2').value = sheetData.requestDate
      sheet.getCell('F4').value = sheetData.jobNumber
      sheet.getCell('F6').value = sheetData.vendorName

      //? set up columns for part number section of sheet
      const columns = {
        qtyCol: 'D',
        unitCol: 'E',
        partNumberCol: 'F',
        descriptionCol: 'H',
        unitCostCol: 'K',
      }

      resolve({workbook, sheet, columns}) 
    } catch (error) {
      reject(error)
    }
  })
}

export async function makeEncompassImport(tenantId, sheetData, base64Encode = false) {
  return new Promise(async (resolve, reject) => {
    try {
      const exportDir = path.join(baseDir, tenantId, 'excel')
      const options = {
        mode: 0o2775, // file/folder creation mode
      }

      await fs.ensureDir(exportDir, options)

      // Make the workbook and sheet 1
      let workbook = new excel.Workbook()
      let sheet = workbook.addWorksheet('Sheet1')
      workbook.properties.date1904 = true

      // Build all the columns in th right order and shrink the ones we don't need
      sheet.columns = [
        { header: 'RequestDate', key: 'RequestDate', width: 25 },
        { header: 'JobNumber', key: 'JobNumber', width: 25 },
        { header: 'CostType', key: 'CostType', width: 0 },
        { header: 'Task', key: 'Task', width: 0 },
        { header: 'Subledger', key: 'Subledger', width: 0 },
        { header: 'SubledgerType', key: 'SubledgerType', width: 0 },
        { header: 'Quantity', key: 'Quantity', width: 25 },
        { header: 'UOM', key: 'UOM', width: 0 },
        { header: 'PartNumber', key: 'PartNumber', width: 25 },
        { header: 'Description', key: 'Description', width: 0 },
        { header: 'UnitPrice', key: 'UnitPrice', width: 0 },
      ]

      // Add data
      sheetData.data.forEach(row => {
        sheet.addRow({ 
          RequestDate: sheetData.requestDate, 
          JobNumber: sheetData.jobNumber, 
          PartNumber: row.partNumber, 
          Quantity: row.quantity 
        })
      })

      //? make file and encode if needed
      resolve(await makeFile(workbook, exportDir, sheetData.filename, base64Encode))
      
    } catch (error) {
      reject(error)
    }
  })
}

export async function makeFile(workbook, exportDir, fileName, base64Encode = false) {
  return new Promise(async (resolve, reject) => {
    try {
      // build file and base64 encode if needed, increment file name if already existing
      let base64
      let inc = 2
      let tmpName = fileName

      if (fs.existsSync(path.join(exportDir, `${tmpName}.xlsx`))) {
        while (fs.existsSync(path.join(exportDir, `${tmpName}.xlsx`))) {
          if (tmpName.search(/\(\d*\)/g) !== -1) {
            tmpName = tmpName.replace(/\(\d*\)/g, `(${inc})`)
          } else {
            tmpName = `${tmpName}(${inc})`
          }
          inc++
        }
        inc = 2
      }
      await workbook.xlsx.writeFile(path.join(exportDir, `${tmpName}.xlsx`))

      if (base64Encode) {
        base64 = base64_encode(path.join(exportDir, `${tmpName}.xlsx`))
      }

      setTimeout(function () {
        fs.unlink(path.join(exportDir, `${tmpName}.xlsx`))
      }, 1000 * 60 * 15) // Remove the excel sheet in 15 minutes to clean up the excel folder

      resolve({
        success: true,
        parentDir: exportDir,
        file: `${tmpName}.xlsx`,
        base64: base64Encode ? base64 : undefined,
      })
    } catch (error) {
      reject(error)
    }
  })
}






// Example object expected to arrive via req.body for a single sheet excel file.
// const sheetData = {
//   filename: 'testFile2918',
//   worksheetTitle: 'a page',
//   columns: [
//     { header: 'Task ID', key: 'taskID', width: 10 },
//     { header: 'Task Name', key: 'taskName', width: 25 },
//     { header: 'Description', key: 'detailedDescription', width: 50 },
//     { header: 'Next Due', key: 'nextDue', type: 'date', width: 15 }, // will format date to YYYY-MM-DD based on type assuming date is legit ISO, will remain unformatted if not
//     { header: 'Categories', key: 'category', type: 'list', width: 15 }, // will convert array to comma separated list based on type
//   ],
//   data: [
//     {
//       taskID: '23',
//       taskName: 'mickey J',
//       detailedDescription: 'some data stuffs'
//       nextDue: '2019-12-20'
//       category: ['ROV', 'Cage', 'Others']
//     },
//     {
//       taskID: '50',
//       taskName: 'fity fity',
//       detailedDescription: 'sskfndksnewmckcjxjsbwk'
//     }
//   ]
// }

// Example object expected to arrive via req.body for a multi sheet excel file.
// const sheetData = {
//   filename: 'testFile2918',
//   worksheetTitle: ['a page', 'a second page'],
//   sheetDataSets: {
//     'a page': {
//       columns: [
//         { header: 'Task ID', key: 'taskID', width: 10 },
//         { header: 'Task Name', key: 'taskName', width: 25 },
//       ],
//       data: [
//         {
//           taskID: '23',
//           taskName: 'mickey J',
//         },
//         {
//           taskID: '50',
//           detailedDescription: 'sskfndksnewmckcjxjsbwk'
//         }
//       ]
//     },
//     'a second page': {
//       columns: [
//         { header: 'Task ID', key: 'taskID', width: 10 },
//         { header: 'Task Name', key: 'taskName', width: 25 },
//       ],
//       data: [
//         {
//           taskID: '23',
//           taskName: 'mickey J',
//         },
//         {
//           taskID: '50',
//           detailedDescription: 'sskfndksnewmckcjxjsbwk'
//         }
//       ]
//     },
//   }
// }

